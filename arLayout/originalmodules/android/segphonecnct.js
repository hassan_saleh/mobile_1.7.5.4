function initializesegphonecnct() {
    Copycontainer0a6274027dfa349 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "Copycontainer0a6274027dfa349",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    Copycontainer0a6274027dfa349.setDefaultUnit(kony.flex.DP);
    var lblContact = new kony.ui.Label({
        "id": "lblContact",
        "isVisible": true,
        "left": "5%",
        "skin": "skn",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblphonenum = new kony.ui.Label({
        "height": "0dp",
        "id": "lblphonenum",
        "isVisible": false,
        "left": "5%",
        "top": 32,
        "width": "0dp"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblemail = new kony.ui.Label({
        "height": "0dp",
        "id": "lblemail",
        "isVisible": false,
        "left": "5%",
        "top": "70dp",
        "width": "0dp"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    Copycontainer0a6274027dfa349.add(lblContact, lblphonenum, lblemail, contactListDivider);
}