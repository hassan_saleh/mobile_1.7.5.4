function initializetempNUOClassificationTypesKA() {
    OuterFlexContainerMainContentsKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "OuterFlexContainerMainContentsKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "skncontainerFAFAFAKA"
    }, {}, {});
    OuterFlexContainerMainContentsKA.setDefaultUnit(kony.flex.DP);
    var InnerFlexContainerRoundCornerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "focusSkin": "sknFocusRoundedFlexf0f0f0KA",
        "height": "80.20%",
        "id": "InnerFlexContainerRoundCornerKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknRoundedFlexNormalfafafaKA",
        "width": "87.50%",
        "zIndex": 1
    }, {}, {});
    InnerFlexContainerRoundCornerKA.setDefaultUnit(kony.flex.DP);
    var lblNUOClassificationTypeKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblNUOClassificationTypeKA",
        "isVisible": true,
        "skin": "sknLatoNUO646464KA",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "98%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    InnerFlexContainerRoundCornerKA.add(lblNUOClassificationTypeKA);
    OuterFlexContainerMainContentsKA.add(InnerFlexContainerRoundCornerKA);
}