function initializetmpSegAccountsTran() {
    flxTrans = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "flxTrans",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxsegBg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTrans.setDefaultUnit(kony.flex.DP);
    var lblTransName = new kony.ui.Label({
        "id": "lblTransName",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblSegName",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "43%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransDate = new kony.ui.Label({
        "id": "lblTransDate",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblsegtextsmall",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11.83%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransAmount = new kony.ui.Label({
        "id": "lblTransAmount",
        "isVisible": true,
        "right": "6%",
        "skin": "lblSegAmount",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransId = new kony.ui.Label({
        "id": "lblTransId",
        "isVisible": false,
        "right": 20,
        "skin": "blsegtextrightsmall",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "31dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Button0hf394843f2a54f = new kony.ui.Button({
        "centerY": "50%",
        "height": "50dp",
        "id": "Button0hf394843f2a54f",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknbtnarow",
        "text": "o",
        "width": "55dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNoData = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNoData",
        "isVisible": false,
        "left": "15dp",
        "skin": "lblSegName",
        "text": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTrans.add(lblTransName, lblTransDate, lblTransAmount, lblTransId, Button0hf394843f2a54f, lblNoData);
}