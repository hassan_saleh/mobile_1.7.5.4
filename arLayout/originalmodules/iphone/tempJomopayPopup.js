function initializetempJomopayPopup() {
    flxJPPopup = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxJPPopup",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxJPPopup.setDefaultUnit(kony.flex.DP);
    var lblJPTransfer = new kony.ui.Label({
        "height": "100%",
        "id": "lblJPTransfer",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferTypeDrop",
        "top": "0%",
        "width": "100%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [5, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblJomopayType = new kony.ui.Label({
        "height": "1%",
        "id": "lblJomopayType",
        "isVisible": true,
        "left": "0%",
        "skin": "sknJPType",
        "text": "Label",
        "top": "0%",
        "width": "1%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxJPPopup.add(lblJPTransfer, lblJomopayType);
}