function initializetmpAccountDetailsScreen() {
    flxAccountDetailsScreenTemplate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxAccountDetailsScreenTemplate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknflxtmpAccountDetailsScreen"
    }, {}, {});
    flxAccountDetailsScreenTemplate.setDefaultUnit(kony.flex.DP);
    var flxContents = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxContents",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "20%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxContents.setDefaultUnit(kony.flex.DP);
    var lblAccountName = new kony.ui.Label({
        "height": "45%",
        "id": "lblAccountName",
        "isVisible": true,
        "left": "1%",
        "maxNumberOfLines": 1,
        "skin": "CopylblSegName0f5b732ec6b8142",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "5%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccountNumber = new kony.ui.Label({
        "height": "50%",
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "1%",
        "maxNumberOfLines": 1,
        "skin": "lblsegtextsmall0b5a3b38d4be646",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "50%",
        "width": "48%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAmount = new kony.ui.Label({
        "height": "45%",
        "id": "lblAmount",
        "isVisible": true,
        "right": "1%",
        "skin": "Copylblsegtextsmall0a16789105f8b49",
        "top": "45%",
        "width": "48%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lbl3 = new kony.ui.Label({
        "height": "45%",
        "id": "lbl3",
        "isVisible": false,
        "right": "5%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Balance",
        "top": "5%",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var Symbol = new kony.ui.Label({
        "height": "100%",
        "id": "Symbol",
        "isVisible": false,
        "left": "0%",
        "skin": "CopylblSegName0i602998c2fe94f",
        "text": "B",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxContents.add(lblAccountName, lblAccountNumber, lblAmount, lbl3, Symbol);
    var imgIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "50%",
        "id": "imgIcon",
        "isVisible": true,
        "left": "5%",
        "skin": "slImage",
        "src": "tran.png",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAccountDetailsScreenTemplate.add(flxContents, imgIcon);
}