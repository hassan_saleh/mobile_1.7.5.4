function initializesegInformation() {
    flxInformation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxInformation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxBgBlueGradientRound8"
    }, {}, {});
    flxInformation.setDefaultUnit(kony.flex.DP);
    var flxBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "slFbox",
        "top": 0,
        "width": "84%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var lblHeader = new kony.ui.Label({
        "id": "lblHeader",
        "isVisible": true,
        "left": "6%",
        "maxNumberOfLines": 2,
        "skin": "sknLblWhike125",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 2, 0, 2],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDescription = new kony.ui.Label({
        "id": "lblDescription",
        "isVisible": true,
        "left": "6%",
        "maxNumberOfLines": 3,
        "skin": "CopysknLblWhike0f7876795165448",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 2, 0, 2],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBody.add(lblHeader, lblDescription);
    var lblSymbol = new kony.ui.Label({
        "id": "lblSymbol",
        "isVisible": true,
        "left": "84%",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.baclinfo"),
        "top": "0dp",
        "width": "16%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 8, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgNews = new kony.ui.Image2({
        "id": "imgNews",
        "isVisible": false,
        "right": "30dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxInformation.add(flxBody, lblSymbol, imgNews);
}