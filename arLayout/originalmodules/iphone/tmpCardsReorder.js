function initializetmpCardsReorder() {
    flxCardsReOrderSettingsTmp = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "flxCardsReOrderSettingsTmp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxCardsReOrderSettingsTmp.setDefaultUnit(kony.flex.DP);
    var lblCardsType = new kony.ui.Label({
        "id": "lblCardsType",
        "isVisible": true,
        "left": "8%",
        "skin": "sknlblTouchIdsmall",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCardNum = new kony.ui.Label({
        "id": "lblCardNum",
        "isVisible": true,
        "left": "8%",
        "skin": "sknlblTouchIdsmall",
        "top": "42dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 4],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCardCode = new kony.ui.Label({
        "id": "lblCardCode",
        "isVisible": false,
        "left": "500dp",
        "skin": "slLabel",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblN = new kony.ui.Label({
        "height": "0px",
        "id": "lblN",
        "isVisible": true,
        "left": "0dp",
        "skin": "slLabel",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblSH = new kony.ui.Label({
        "height": "0px",
        "id": "lblSH",
        "isVisible": true,
        "left": "0dp",
        "skin": "slLabel",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblPref = new kony.ui.Label({
        "height": "0px",
        "id": "lblPref",
        "isVisible": true,
        "left": "0dp",
        "skin": "slLabel",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var ArrowR = new kony.ui.Label({
        "centerY": "50%",
        "id": "ArrowR",
        "isVisible": false,
        "right": "4%",
        "skin": "sknBackIconDisabled",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblIncommingRing = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblIncommingRing",
        "isVisible": false,
        "right": "2%",
        "skin": "sknBOJttfwhitee",
        "text": "W",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCardsReOrderSettingsTmp.add(lblCardsType, lblCardNum, lblCardCode, lblN, lblSH, lblPref, ArrowR, lblIncommingRing);
}