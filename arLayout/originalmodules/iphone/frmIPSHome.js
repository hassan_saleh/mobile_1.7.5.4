function addWidgetsfrmIPSHome() {
    frmIPSHome.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_c430c0c45097446ea58d9481d5244385,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblBack0h2fe661f120d45 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblBack0h2fe661f120d45",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBack.add(lblBackIcon, CopylblBack0h2fe661f120d45);
    var lblIPSRegistrationTitle = new kony.ui.Label({
        "height": "90%",
        "id": "lblIPSRegistrationTitle",
        "isVisible": true,
        "left": "20%",
        "minHeight": "90%",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.IPS.registerforIPS"),
        "top": "0%",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxHeader.add(flxBack, lblIPSRegistrationTitle);
    var flxBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var imgBeneficiary = new kony.ui.Image2({
        "centerX": "50%",
        "height": "25%",
        "id": "imgBeneficiary",
        "isVisible": true,
        "skin": "slImage",
        "src": "icon_beneficiary.png",
        "top": "10%",
        "width": "60%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnRegister = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "90%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "9%",
        "id": "btnRegister",
        "isVisible": true,
        "onClick": AS_Button_f5026d816c8d415ea26770578b08d5ab,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.IPS.registerforIPS"),
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var lblNoteIPS = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblNoteIPS",
        "isVisible": true,
        "left": "141dp",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.NoBeneficiary"),
        "top": "287dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblMainIPSHome = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblMainIPSHome",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblMsg",
        "text": kony.i18n.getLocalizedString("i18n.externalAccount.Sendmoneytobeneficiary"),
        "top": "237dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBody.add(imgBeneficiary, btnRegister, lblNoteIPS, lblMainIPSHome);
    frmIPSHome.add(flxHeader, flxBody);
};

function frmIPSHomeGlobals() {
    frmIPSHome = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmIPSHome,
        "enabledForIdleTimeout": true,
        "id": "frmIPSHome",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "sknBackground"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};