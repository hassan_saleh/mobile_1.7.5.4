function initializetmpAccountTypes() {
    flxFrequency = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxFrequency",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "CopyslFbox0h5adacabf4a940"
    }, {}, {});
    flxFrequency.setDefaultUnit(kony.flex.DP);
    var lblfrequency = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblfrequency",
        "isVisible": true,
        "left": "22%",
        "skin": "sknLblWhike125",
        "width": "74%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblKey = new kony.ui.Label({
        "centerX": "60%",
        "centerY": "60%",
        "id": "lblKey",
        "isVisible": false,
        "left": "5%",
        "skin": "sknLblWhike125",
        "text": "Label",
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "50%",
        "id": "imgIcon",
        "isVisible": true,
        "left": "5%",
        "skin": "slImage",
        "src": "imagedrag.png",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxFrequency.add(lblfrequency, lblKey, imgIcon);
}