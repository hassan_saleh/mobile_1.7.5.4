function initializetmpSegRequestStatus() {
    flxtmpSegRequestStatus = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxtmpSegRequestStatus",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFbox"
    }, {}, {});
    flxtmpSegRequestStatus.setDefaultUnit(kony.flex.DP);
    var lblJoMoPay = new kony.ui.Label({
        "id": "lblJoMoPay",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblAmountCurrency",
        "top": "20dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStatus = new kony.ui.Label({
        "id": "lblStatus",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblAccountStaticText",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStatusDesc = new kony.ui.Label({
        "id": "lblStatusDesc",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblWhike125",
        "top": "-5dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRequestDate = new kony.ui.Label({
        "id": "lblRequestDate",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblAccountStaticText",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRequestDateVale = new kony.ui.Label({
        "id": "lblRequestDateVale",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblWhike125",
        "top": "-5dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDelDate = new kony.ui.Label({
        "id": "lblDelDate",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblAccountStaticText",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDelDateValue = new kony.ui.Label({
        "id": "lblDelDateValue",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblWhike125",
        "top": "-5dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknflxLinewhiteOp",
        "top": "12dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    flxtmpSegRequestStatus.add(lblJoMoPay, lblStatus, lblStatusDesc, lblRequestDate, lblRequestDateVale, lblDelDate, lblDelDateValue, flxLine);
}