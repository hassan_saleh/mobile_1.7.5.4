//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmConfirmPrePayBillAr() {
frmConfirmPrePayBill.setDefaultUnit(kony.flex.DP);
var titleBarWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "titleBarWrapper",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgheader",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarWrapper.setDefaultUnit(kony.flex.DP);
var androidTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "androidTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgheader",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
androidTitleBar.setDefaultUnit(kony.flex.DP);
var androidTitleLabel = new kony.ui.Label({
"centerY": "50%",
"id": "androidTitleLabel",
"isVisible": true,
"right": "55dp",
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.payBill.ConfirmPayBill"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
androidTitleBar.add(androidTitleLabel);
var backButton = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "skntitleBarTextButtonFocus",
"height": "50dp",
"id": "backButton",
"isVisible": true,
"right": "0dp",
"minWidth": "50dp",
"onClick": AS_Button_fd967fc4ab5d4a118ee1f706a1532be5,
"skin": "skntitleBarTextButton",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"top": "0dp",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,2, 0],
"paddingInPixel": false
}, {});
titleBarWrapper.add(androidTitleBar, backButton);
var flxMainKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "flxMainKA",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMainKA.setDefaultUnit(kony.flex.DP);
var FlexContainer042b0725667e643 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "115dp",
"id": "FlexContainer042b0725667e643",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer042b0725667e643.setDefaultUnit(kony.flex.DP);
var FlexContainer0f615b714fea843 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0f615b714fea843",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0f615b714fea843.setDefaultUnit(kony.flex.DP);
var transactionAmount = new kony.ui.Label({
"centerX": "50%",
"id": "transactionAmount",
"isVisible": true,
"skin": "skndetailPageNumber",
"text": "$ 5006.00",
"top": "10dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionId = new kony.ui.Label({
"centerX": "50%",
"id": "transactionId",
"isVisible": false,
"skin": "skndetailPageNumber",
"top": "30dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionType = new kony.ui.Label({
"centerX": "50%",
"id": "transactionType",
"isVisible": false,
"skin": "skndetailPageNumber",
"text": "$ 5006.00",
"top": "30dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var fromAccNumberKA = new kony.ui.Label({
"centerX": "50%",
"id": "fromAccNumberKA",
"isVisible": false,
"skin": "skndetailPageNumber",
"text": "$ 5006.00",
"top": "30dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var fromAccountNameKA = new kony.ui.Label({
"centerX": "50%",
"id": "fromAccountNameKA",
"isVisible": false,
"skin": "skndetailPageNumber",
"text": "$ 5006.00",
"top": "30dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var toPayeeName = new kony.ui.Label({
"centerX": "50%",
"id": "toPayeeName",
"isVisible": false,
"skin": "skndetailPageNumber",
"text": "$ 5006.00",
"top": "30dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var MapedAmountLabel = new kony.ui.Label({
"centerX": "50%",
"id": "MapedAmountLabel",
"isVisible": false,
"skin": "skndetailPageNumber",
"text": "$ 5006.00",
"top": "30dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionDate = new kony.ui.Label({
"centerX": "50%",
"id": "transactionDate",
"isVisible": true,
"skin": "skndetailPageDate",
"text": kony.i18n.getLocalizedString("i18n.transfers.TransferTo"),
"top": "3dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionName = new kony.ui.Label({
"centerX": "50%",
"id": "transactionName",
"isVisible": true,
"skin": "sknNumber",
"text": "Father's Savings Account",
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var toAccountNumberKA = new kony.ui.Label({
"centerX": "50%",
"id": "toAccountNumberKA",
"isVisible": false,
"skin": "sknNumber",
"text": "Father's Savings Account",
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0017b9d4dcba145 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0017b9d4dcba145",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": 10,
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0017b9d4dcba145.setDefaultUnit(kony.flex.DP);
Copydivider0017b9d4dcba145.add();
FlexContainer0f615b714fea843.add(transactionAmount, transactionId, transactionType, fromAccNumberKA, fromAccountNameKA, toPayeeName, MapedAmountLabel, transactionDate, transactionName, toAccountNumberKA, Copydivider0017b9d4dcba145);
FlexContainer042b0725667e643.add(FlexContainer0f615b714fea843);
var additionalDetails1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "additionalDetails1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
additionalDetails1.setDefaultUnit(kony.flex.DP);
var Label0e7a26666ebf141 = new kony.ui.Label({
"id": "Label0e7a26666ebf141",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.common.fromc"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionFrom = new kony.ui.Label({
"id": "transactionFrom",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Savings 2453",
"top": "40dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var fromAccountNumberKA = new kony.ui.Label({
"id": "fromAccountNumberKA",
"isVisible": false,
"left": "15%",
"skin": "sknNumber",
"text": "Savings 2453",
"top": "50dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var divider2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "divider2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
divider2.setDefaultUnit(kony.flex.DP);
divider2.add();
additionalDetails1.add(Label0e7a26666ebf141, transactionFrom, fromAccountNumberKA, divider2);
var flxSchedule = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxSchedule",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSchedule.setDefaultUnit(kony.flex.DP);
var CopyLabel05d0720c3bd5d45 = new kony.ui.Label({
"id": "CopyLabel05d0720c3bd5d45",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": "Scheduled For:",
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblScheduledDate = new kony.ui.Label({
"id": "lblScheduledDate",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "05-20-2016",
"top": "40dp",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var mapedDateKA = new kony.ui.Label({
"id": "mapedDateKA",
"isVisible": false,
"right": "15%",
"skin": "sknNumber",
"text": "05-20-2016",
"top": "50dp",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider058b92c2528494d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider058b92c2528494d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider058b92c2528494d.setDefaultUnit(kony.flex.DP);
Copydivider058b92c2528494d.add();
flxSchedule.add(CopyLabel05d0720c3bd5d45, lblScheduledDate, mapedDateKA, Copydivider058b92c2528494d);
var FlexContainer002f7e6e2ce6b49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"clipBounds": true,
"height": "130dp",
"id": "FlexContainer002f7e6e2ce6b49",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer002f7e6e2ce6b49.setDefaultUnit(kony.flex.DP);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_ff8c57ffb68644be9dbeffa30e456030,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.transfers.CONFIRM"),
"top": "10%",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnEditTranscation = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "btnEditTranscation",
"isVisible": true,
"onClick": AS_Button_a1b6eadfdf394ee09ea1e5d23caabbbb,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.common.edit"),
"top": "5dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlexContainer002f7e6e2ce6b49.add(btnConfirm, btnEditTranscation);
flxMainKA.add(FlexContainer042b0725667e643, additionalDetails1, flxSchedule, FlexContainer002f7e6e2ce6b49);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "skncontainerBkg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_ge2bc7fecd0d48c18d14d8ad02b9013d,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var CopylblHeaderBackIcon0e57343f2e2274c = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblHeaderBackIcon0e57343f2e2274c",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblHeaderBack0j7f57eb046594b = new kony.ui.Label({
"centerY": "50.00%",
"id": "CopylblHeaderBack0j7f57eb046594b",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(CopylblHeaderBackIcon0e57343f2e2274c, CopylblHeaderBack0j7f57eb046594b);
var CopylblHeaderTitle0de254f31c2b44e = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "CopylblHeaderTitle0de254f31c2b44e",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblClose = new kony.ui.Label({
"centerX": "94%",
"height": "100%",
"id": "lblClose",
"isVisible": true,
"onTouchEnd": AS_Label_b7f1f608484c4e0bb7c855314003e590,
"skin": "sknCloseConfirm",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeader.add(flxBack, CopylblHeaderTitle0de254f31c2b44e, lblClose);
var flxMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": false,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxImpDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxImpDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxImpDetails.setDefaultUnit(kony.flex.DP);
var CopyflxIcon0d4c897823fde4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "CopyflxIcon0d4c897823fde4f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxToIcon",
"top": "5%",
"width": "50dp",
"zIndex": 1
}, {}, {});
CopyflxIcon0d4c897823fde4f.setDefaultUnit(kony.flex.DP);
var initialsCategory = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "80%",
"id": "initialsCategory",
"isVisible": true,
"skin": "sknLblFromIcon",
"text": "AI",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxIcon0d4c897823fde4f.add(initialsCategory);
var lblBillerCategory = new kony.ui.Label({
"centerX": "50%",
"id": "lblBillerCategory",
"isVisible": true,
"skin": "sknBeneTitle",
"text": "Airtel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBeneAccNum0d0906faa372149 = new kony.ui.Label({
"centerX": "50%",
"id": "CopylblBeneAccNum0d0906faa372149",
"isVisible": false,
"right": "0dp",
"skin": "sknLblNextDisabled",
"text": "9845654568",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxImpDetails.add(CopyflxIcon0d4c897823fde4f, lblBillerCategory, CopylblBeneAccNum0d0906faa372149);
var flxBillC = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxBillC",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBillC.setDefaultUnit(kony.flex.DP);
var CopylblBillerCategoryStatic0f8d6d48ed2e746 = new kony.ui.Label({
"height": "50%",
"id": "CopylblBillerCategoryStatic0f8d6d48ed2e746",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBillerCategory0b5ee3e311abf42 = new kony.ui.Label({
"height": "50%",
"id": "CopylblBillerCategory0b5ee3e311abf42",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Mobile",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBillC.add(CopylblBillerCategoryStatic0f8d6d48ed2e746, CopylblBillerCategory0b5ee3e311abf42);
var flxBillN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxBillN",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-4%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBillN.setDefaultUnit(kony.flex.DP);
var CopylblConfirmBNTitle0b7f4f5e29e9346 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmBNTitle0b7f4f5e29e9346",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.billerName"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBillerName = new kony.ui.Label({
"height": "50%",
"id": "lblBillerName",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Airtel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-10%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBillN.add(CopylblConfirmBNTitle0b7f4f5e29e9346, lblBillerName);
var flxServiceType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxServiceType",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxServiceType.setDefaultUnit(kony.flex.DP);
var CopylblConfirmBankNameTitle0gb348fcf67e64f = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmBankNameTitle0gb348fcf67e64f",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblServiceType = new kony.ui.Label({
"height": "50%",
"id": "lblServiceType",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Balance",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-10%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxServiceType.add(CopylblConfirmBankNameTitle0gb348fcf67e64f, lblServiceType);
var flxDenomination = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxDenomination",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDenomination.setDefaultUnit(kony.flex.DP);
var CopylblConfirmCountryTitle0fcd86eed51d946 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmCountryTitle0fcd86eed51d946",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.prepaid.denomination"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblConfirmDenomination = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmDenomination",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Denomination Example 1",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-10%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDenomination.add(CopylblConfirmCountryTitle0fcd86eed51d946, lblConfirmDenomination);
var flxBillerNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxBillerNumber",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBillerNumber.setDefaultUnit(kony.flex.DP);
var CopylblConfirmCountryTitle0a095687a3dab49 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmCountryTitle0a095687a3dab49",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.BillerNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBillerNumber = new kony.ui.Label({
"height": "50%",
"id": "lblBillerNumber",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "9000719069",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-10%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBillerNumber.add(CopylblConfirmCountryTitle0a095687a3dab49, lblBillerNumber);
var flxDueAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxDueAmount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDueAmount.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0ab5637c5290a49 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0ab5637c5290a49",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.bills.DueAmount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblConfirmDueAmount = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmDueAmount",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "500 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-10%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDueAmount.add(CopylblConfirmEmailTitle0ab5637c5290a49, lblConfirmDueAmount);
var flxMinAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxMinAmount",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMinAmount.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0g298b3d87e7b4a = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0g298b3d87e7b4a",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Minimum Amount",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblConfirmEmail0cc5acaa015bb4b = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmail0cc5acaa015bb4b",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "50 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMinAmount.add(CopylblConfirmEmailTitle0g298b3d87e7b4a, CopylblConfirmEmail0cc5acaa015bb4b);
var flxMaxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxMaxAmount",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMaxAmount.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0g13c26a5c63d49 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0g13c26a5c63d49",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Maximum Amount",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblConfirmEmail0f16b55f2efce4b = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmail0f16b55f2efce4b",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "1000 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMaxAmount.add(CopylblConfirmEmailTitle0g13c26a5c63d49, CopylblConfirmEmail0f16b55f2efce4b);
var flxFeeAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxFeeAmount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFeeAmount.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0b5d4b7a7d9c44b = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0b5d4b7a7d9c44b",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.bills.FeeAmount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFeeAmount = new kony.ui.Label({
"height": "50%",
"id": "lblFeeAmount",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "1 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-10%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFeeAmount.add(CopylblConfirmEmailTitle0b5d4b7a7d9c44b, lblFeeAmount);
var flxTotalAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxTotalAmount",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTotalAmount.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0i67980d8019c44 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0i67980d8019c44",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.bills.TotalAmount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTotalAmount = new kony.ui.Label({
"height": "50%",
"id": "lblTotalAmount",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "501 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTotalAmount.add(CopylblConfirmEmailTitle0i67980d8019c44, lblTotalAmount);
var flxLCAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxLCAmount",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLCAmount.setDefaultUnit(kony.flex.DP);
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "4%",
"skin": "slFbox",
"top": "2%",
"width": "45%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0ad38e28e71194e = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0ad38e28e71194e",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.bills.PaidAmount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPaidAmount = new kony.ui.Label({
"height": "50%",
"id": "lblPaidAmount",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "500 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAmount.add(CopylblConfirmEmailTitle0ad38e28e71194e, lblPaidAmount);
flxLCAmount.add(flxAmount);
var flxLocalAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxLocalAmount",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "4%",
"skin": "slFbox",
"top": "0%",
"width": "45%",
"zIndex": 1
}, {}, {});
flxLocalAmount.setDefaultUnit(kony.flex.DP);
var lblConfirmLocalAmount = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmLocalAmount",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Transfers.LocalAmount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblVal = new kony.ui.Label({
"id": "lblVal",
"isVisible": true,
"right": "9%",
"skin": "sknLblBack",
"text": "0.000 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxLocalAmount.add(lblConfirmLocalAmount, lblVal);
var flxSourceofFunding = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxSourceofFunding",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSourceofFunding.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0da6b74d1c8ba47 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0da6b74d1c8ba47",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.bills.SourceOfFund"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblSOFAccount = new kony.ui.Label({
"height": "50%",
"id": "lblSOFAccount",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Savings Account",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-10%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSourceofFunding.add(CopylblConfirmEmailTitle0da6b74d1c8ba47, lblSOFAccount);
var flxSourceofFundAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxSourceofFundAccount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSourceofFundAccount.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0a2677ece199249 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0a2677ece199249",
"isVisible": false,
"right": "7.97%",
"skin": "sknLblNextDisabled",
"text": "Source of Fund",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblSOFAccNum = new kony.ui.Label({
"height": "50%",
"id": "lblSOFAccNum",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "****6789",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSourceofFundAccount.add(CopylblConfirmEmailTitle0a2677ece199249, lblSOFAccNum);
var flxIssueDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxIssueDate",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIssueDate.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0b4776406211e4f = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0b4776406211e4f",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Issue Date",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblConfirmEmail0dd9e3fe04d1d46 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmail0dd9e3fe04d1d46",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "1 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxIssueDate.add(CopylblConfirmEmailTitle0b4776406211e4f, CopylblConfirmEmail0dd9e3fe04d1d46);
var flxDueDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxDueDate",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDueDate.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0i743c8b6472c4d = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0i743c8b6472c4d",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Due Date",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblConfirmEmail0id331a6e748544 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmail0id331a6e748544",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "1 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDueDate.add(CopylblConfirmEmailTitle0i743c8b6472c4d, CopylblConfirmEmail0id331a6e748544);
var flxConfirmBtn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxConfirmBtn",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxConfirmBtn.setDefaultUnit(kony.flex.DP);
var CopybtnConfirm0afe340a6460347 = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "100%",
"id": "CopybtnConfirm0afe340a6460347",
"isVisible": true,
"onClick": AS_Button_fd659751407c4d6884f57ac2693fc98e,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
"top": "0%",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnEditTranscation0cdd2ff91db6b45 = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "CopybtnEditTranscation0cdd2ff91db6b45",
"isVisible": false,
"skin": "sknsecondaryAction",
"text": "Edit",
"top": "5dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxConfirmBtn.add(CopybtnConfirm0afe340a6460347, CopybtnEditTranscation0cdd2ff91db6b45);
var CopyflxAmount0h42ee0066fa841 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "0%",
"id": "CopyflxAmount0h42ee0066fa841",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxAmount0h42ee0066fa841.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0e56c42a5adca42 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0e56c42a5adca42",
"isVisible": false,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Issue Date",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblConfirmEmail0caa6a6f404754d = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmail0caa6a6f404754d",
"isVisible": false,
"right": "8%",
"skin": "sknLblBack",
"text": "1 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxAmount0h42ee0066fa841.add(CopylblConfirmEmailTitle0e56c42a5adca42, CopylblConfirmEmail0caa6a6f404754d);
flxMain.add(flxImpDetails, flxBillC, flxBillN, flxServiceType, flxDenomination, flxBillerNumber, flxDueAmount, flxMinAmount, flxMaxAmount, flxFeeAmount, flxTotalAmount, flxLCAmount, flxLocalAmount, flxSourceofFunding, flxSourceofFundAccount, flxIssueDate, flxDueDate, flxConfirmBtn, CopyflxAmount0h42ee0066fa841);
var flxService = new kony.ui.FlexContainer({
"clipBounds": true,
"height": "220dp",
"id": "flxService",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "227dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxService.setDefaultUnit(kony.flex.DP);
var lblBillerCode = new kony.ui.Label({
"id": "lblBillerCode",
"isVisible": false,
"right": "186dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCurrencyType = new kony.ui.Label({
"id": "lblCurrencyType",
"isVisible": false,
"right": "196dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCustID = new kony.ui.Label({
"id": "lblCustID",
"isVisible": false,
"right": "186dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLang = new kony.ui.Label({
"id": "lblLang",
"isVisible": false,
"right": "186dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountNumber = new kony.ui.Label({
"id": "lblAccountNumber",
"isVisible": false,
"right": "186dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccBr = new kony.ui.Label({
"id": "lblAccBr",
"isVisible": false,
"right": "186dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblSvcType = new kony.ui.Label({
"id": "lblSvcType",
"isVisible": false,
"right": "186dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBillingNumber = new kony.ui.Label({
"id": "lblBillingNumber",
"isVisible": false,
"right": "196dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPaidAmnt = new kony.ui.Label({
"id": "lblPaidAmnt",
"isVisible": false,
"right": "206dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDueAmnt = new kony.ui.Label({
"id": "lblDueAmnt",
"isVisible": false,
"right": "216dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "30dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccessChannel = new kony.ui.Label({
"id": "lblAccessChannel",
"isVisible": false,
"right": "216dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "30dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPaymentMethod = new kony.ui.Label({
"id": "lblPaymentMethod",
"isVisible": false,
"right": "226dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblSendSMSflag = new kony.ui.Label({
"id": "lblSendSMSflag",
"isVisible": false,
"right": "236dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "50dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCustInfoFlag = new kony.ui.Label({
"id": "lblCustInfoFlag",
"isVisible": false,
"right": "246dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "60dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPaymentStatus = new kony.ui.Label({
"id": "lblPaymentStatus",
"isVisible": false,
"right": "246dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "60dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFeeAmnt = new kony.ui.Label({
"id": "lblFeeAmnt",
"isVisible": false,
"right": "246dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "60dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFeeOnBiller = new kony.ui.Label({
"id": "lblFeeOnBiller",
"isVisible": false,
"right": "246dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "60dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBillNo = new kony.ui.Label({
"id": "lblBillNo",
"isVisible": false,
"right": "246dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "60dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTransferFlag = new kony.ui.Label({
"id": "lblTransferFlag",
"isVisible": false,
"right": "246dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "60dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblfcbdrefNo = new kony.ui.Label({
"id": "lblfcbdrefNo",
"isVisible": false,
"right": "246dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "60dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPID = new kony.ui.Label({
"id": "lblPID",
"isVisible": false,
"right": "256dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "70dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMobileNumber = new kony.ui.Label({
"id": "lblMobileNumber",
"isVisible": false,
"right": "256dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "70dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var pidType = new kony.ui.Label({
"id": "pidType",
"isVisible": false,
"right": "256dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "70dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var pNation = new kony.ui.Label({
"id": "pNation",
"isVisible": false,
"right": "266dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "80dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var prBillerCode = new kony.ui.Label({
"id": "prBillerCode",
"isVisible": false,
"right": "276dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "90dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var prServiceCode = new kony.ui.Label({
"id": "prServiceCode",
"isVisible": false,
"right": "286dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "100dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var prBillingNo = new kony.ui.Label({
"id": "prBillingNo",
"isVisible": false,
"right": "296dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "110dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var prdenoFlag = new kony.ui.Label({
"id": "prdenoFlag",
"isVisible": false,
"right": "306dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "120dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var prValidationCode = new kony.ui.Label({
"id": "prValidationCode",
"isVisible": false,
"right": "316dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "130dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var prDueAmount = new kony.ui.Label({
"id": "prDueAmount",
"isVisible": false,
"right": "326dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "140dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var prMobileNo = new kony.ui.Label({
"id": "prMobileNo",
"isVisible": false,
"right": "336dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "150dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var prPaymentType = new kony.ui.Label({
"id": "prPaymentType",
"isVisible": false,
"right": "326dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "140dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var prAccorCCNo = new kony.ui.Label({
"id": "prAccorCCNo",
"isVisible": false,
"right": "316dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "130dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var prAccBr = new kony.ui.Label({
"id": "prAccBr",
"isVisible": false,
"right": "326dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "140dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxService.add(lblBillerCode, lblCurrencyType, lblCustID, lblLang, lblAccountNumber, lblAccBr, lblSvcType, lblBillingNumber, lblPaidAmnt, lblDueAmnt, lblAccessChannel, lblPaymentMethod, lblSendSMSflag, lblCustInfoFlag, lblPaymentStatus, lblFeeAmnt, lblFeeOnBiller, lblBillNo, lblTransferFlag, lblfcbdrefNo, lblPID, lblMobileNumber, pidType, pNation, prBillerCode, prServiceCode, prBillingNo, prdenoFlag, prValidationCode, prDueAmount, prMobileNo, prPaymentType, prAccorCCNo, prAccBr);
frmConfirmPrePayBill.add(titleBarWrapper, flxMainKA, flxHeader, flxMain, flxService);
};
function frmConfirmPrePayBillGlobalsAr() {
frmConfirmPrePayBillAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmConfirmPrePayBillAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmConfirmPrePayBill",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"preShow": AS_Form_ibedf2155eba41efa97a5d909a4d5a5e,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_f8305eed1afd4ee09d3616587b1d8c64,
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
