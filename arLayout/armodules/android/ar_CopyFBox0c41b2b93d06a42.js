//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:34 EET 2020
function initializeCopyFBox0c41b2b93d06a42Ar() {
    CopyFBox0c41b2b93d06a42Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyFBox0c41b2b93d06a42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox0c41b2b93d06a42Ar.setDefaultUnit(kony.flex.DP);
    var contactListLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "contactListLabel",
        "isVisible": true,
        "right": "5%",
        "text": "Label",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "49dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "containerWeight": 100
    }, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    CopyFBox0c41b2b93d06a42Ar.add(contactListLabel, contactListDivider);
}
