//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:02 EET 2020
function initializeFBox02fdfea8b923649Ar() {
    FBox02fdfea8b923649Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "FBox02fdfea8b923649",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    FBox02fdfea8b923649Ar.setDefaultUnit(kony.flex.DP);
    var informationListLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "informationListLabel",
        "isVisible": true,
        "right": "15%",
        "text": "Label",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var informationListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "informationListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "49dp",
        "width": "85%",
        "zIndex": 1
    }, {
        "containerWeight": 100
    }, {});
    informationListDivider.setDefaultUnit(kony.flex.DP);
    informationListDivider.add();
    FBox02fdfea8b923649Ar.add(informationListLabel, informationListDivider);
}
