//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function initializetmpAccountSMSEnabledAr() {
    flxSMSNotificationEnabledAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxSMSNotificationEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSMSNotificationEnabledAr.setDefaultUnit(kony.flex.DP);
    var flxSMSSwitchDIsabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSMSSwitchDIsabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_d21c6ccb47a04204823befbf63db9b1e,
        "left": "5%",
        "skin": "sknflxWhiteOpacity60",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 3
    }, {}, {});
    flxSMSSwitchDIsabled.setDefaultUnit(kony.flex.DP);
    var flxInnerDisabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxInnerDisabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    flxInnerDisabled.setDefaultUnit(kony.flex.DP);
    flxInnerDisabled.add();
    var flxILineDisabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "14dp",
        "id": "flxILineDisabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 10,
        "skin": "sknLineDarkBlueOpacity60",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxILineDisabled.setDefaultUnit(kony.flex.DP);
    flxILineDisabled.add();
    flxSMSSwitchDIsabled.add(flxInnerDisabled, flxILineDisabled);
    var flxSMSSwitchEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSMSSwitchEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_c1834d9307e744d6adee27f5b68f4e87,
        "left": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 3
    }, {}, {});
    flxSMSSwitchEnabled.setDefaultUnit(kony.flex.DP);
    var flxInnerEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxInnerEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    flxInnerEnabled.setDefaultUnit(kony.flex.DP);
    flxInnerEnabled.add();
    var flxLineEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "14dp",
        "id": "flxLineEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxLineEnabled.setDefaultUnit(kony.flex.DP);
    flxLineEnabled.add();
    flxSMSSwitchEnabled.add(flxInnerEnabled, flxLineEnabled);
    var flxUnderLineSMS = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderLineSMS",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "flxUnderLine",
        "top": "98%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderLineSMS.setDefaultUnit(kony.flex.DP);
    flxUnderLineSMS.add();
    var lblAccountType = new kony.ui.Label({
        "id": "lblAccountType",
        "isVisible": true,
        "right": "5%",
        "maxNumberOfLines": 1,
        "skin": "lblAccountType",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "23%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "right": "5%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBack",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "45%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxAlertArrow = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAlertArrow",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "onClick": AS_FlexContainer_c1834d9307e744d6adee27f5b68f4e87,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxAlertArrow.setDefaultUnit(kony.flex.DP);
    var lblRightIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblRightIcon",
        "isVisible": true,
        "left": "3%",
        "skin": "sknBOJttfwhitee",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "width": "15%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxAlertArrow.add(lblRightIcon);
    flxSMSNotificationEnabledAr.add(flxSMSSwitchDIsabled, flxSMSSwitchEnabled, flxUnderLineSMS, lblAccountType, lblAccountNumber, flxAlertArrow);
}
