package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_onboardingNav extends JSLibrary {

 
 
	public static final String SMS_URL = "SMS_URL";
 
 
	public static final String MOBILE_FORM_URL = "MOBILE_FORM_URL";
 
 
	public static final String setSEND_IMAGES_URL = "setSEND_IMAGES_URL";
 
 
	public static final String setSendSMSApiHeaders = "setSendSMSApiHeaders";
 
 
	public static final String setSendImagesApiHeaders = "setSendImagesApiHeaders";
 
 
	public static final String setZoomDeviceLicenseKeyIdentifier = "setZoomDeviceLicenseKeyIdentifier";
 
 
	public static final String setZoomProductionKeyText = "setZoomProductionKeyText";
 
 
	public static final String setPublicFaceMapEncryptionKey = "setPublicFaceMapEncryptionKey";
 
 
	public static final String setZoomServerBaseURL = "setZoomServerBaseURL";
 
	String[] methods = { SMS_URL, MOBILE_FORM_URL, setSEND_IMAGES_URL, setSendSMSApiHeaders, setSendImagesApiHeaders, setZoomDeviceLicenseKeyIdentifier, setZoomProductionKeyText, setPublicFaceMapEncryptionKey, setZoomServerBaseURL };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_onboardingNav(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String URL0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 URL0 = (java.lang.String)params[0];
 }
 ret = this.SMS_URL( URL0 );
 
 			break;
 		case 1:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String form_url1 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 form_url1 = (java.lang.String)params[0];
 }
 ret = this.MOBILE_FORM_URL( form_url1 );
 
 			break;
 		case 2:
 if (paramLen != 2){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String BaseURL2 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 BaseURL2 = (java.lang.String)params[0];
 }
 java.lang.String BodyURL2 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 BodyURL2 = (java.lang.String)params[1];
 }
 ret = this.setSEND_IMAGES_URL( BaseURL2, BodyURL2 );
 
 			break;
 		case 3:
 if (paramLen != 2){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String SMS_ARGS_U3 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 SMS_ARGS_U3 = (java.lang.String)params[0];
 }
 java.lang.String SMS_ARGS_P3 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 SMS_ARGS_P3 = (java.lang.String)params[1];
 }
 ret = this.setSendSMSApiHeaders( SMS_ARGS_U3, SMS_ARGS_P3 );
 
 			break;
 		case 4:
 if (paramLen != 2){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String IMAGE_ARGS_U4 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 IMAGE_ARGS_U4 = (java.lang.String)params[0];
 }
 java.lang.String IMAGE_ARGS_P4 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 IMAGE_ARGS_P4 = (java.lang.String)params[1];
 }
 ret = this.setSendImagesApiHeaders( IMAGE_ARGS_U4, IMAGE_ARGS_P4 );
 
 			break;
 		case 5:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String DeviceKeyIdentifier5 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 DeviceKeyIdentifier5 = (java.lang.String)params[0];
 }
 ret = this.setZoomDeviceLicenseKeyIdentifier( DeviceKeyIdentifier5 );
 
 			break;
 		case 6:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String ProductionKeyText6 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 ProductionKeyText6 = (java.lang.String)params[0];
 }
 ret = this.setZoomProductionKeyText( ProductionKeyText6 );
 
 			break;
 		case 7:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String PublicFaceScanEncryptionKey7 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 PublicFaceScanEncryptionKey7 = (java.lang.String)params[0];
 }
 ret = this.setPublicFaceMapEncryptionKey( PublicFaceScanEncryptionKey7 );
 
 			break;
 		case 8:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String BaseURLzoom8 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 BaseURLzoom8 = (java.lang.String)params[0];
 }
 ret = this.setZoomServerBaseURL( BaseURLzoom8 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "onboardingNav";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] SMS_URL( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setSMS_URL( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] MOBILE_FORM_URL( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setMOBILE_FORM_URL( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setSEND_IMAGES_URL( java.lang.String inputKey0, java.lang.String inputKey1 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setSEND_IMAGES_URL( inputKey0
 , inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setSendSMSApiHeaders( java.lang.String inputKey0, java.lang.String inputKey1 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setSendSMSApiHeaders( inputKey0
 , inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setSendImagesApiHeaders( java.lang.String inputKey0, java.lang.String inputKey1 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setSendImagesApiHeaders( inputKey0
 , inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setZoomDeviceLicenseKeyIdentifier( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setZoomDeviceLicenseKeyIdentifier( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setZoomProductionKeyText( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setZoomProductionKeyText( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setPublicFaceMapEncryptionKey( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setPublicFaceMapEncryptionKey( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setZoomServerBaseURL( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setZoomServerBaseURL( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
};
