var data = [];
var prePaidData = [];
var selectedPrePaidPayee = {};
kony.boj.deletePrePaid = 0;


kony.boj.onClickNextAddPreBiller = function(){
  frmAddNewPrePayeeKA.flxDenominationConf.setVisibility(false);
  frmAddNewPrePayeeKA.flxAmountConf.setVisibility(false);
  frmAddNewPrePayeeKA.lblLanguage.text = kony.boj.lang;
  frmAddNewPrePayeeKA.lblType.text = "A";
  //1967 fix
  var nicknm=frmAddNewPrePayeeKA.tbxNickName.text;
  if(nicknm !== undefined && nicknm !== "" && nicknm !== null && nicknm !="0" )
    {
      frmAddNewPrePayeeKA.lblBillerNamePopup.text = nicknm;
      frmAddNewPrePayeeKA.lblInitial.text = nicknm.substring(0,2).toUpperCase();
    }
  else
    {
      frmAddNewPrePayeeKA.lblBillerNamePopup.text = frmAddNewPrePayeeKA.lblBillerName.text;
      frmAddNewPrePayeeKA.lblInitial.text = frmAddNewPrePayeeKA.lblBillerName.text.substring(0,2).toUpperCase();
    }
  //end
 // frmAddNewPrePayeeKA.lblBillerNamePopup.text = frmAddNewPrePayeeKA.lblBillerName.text;
 // frmAddNewPrePayeeKA.lblInitial.text = frmAddNewPrePayeeKA.lblBillerName.text.substring(0, 2).toUpperCase();
  frmAddNewPrePayeeKA.flxIcon1.backgroundColor = kony.boj.getBackGroundColour(frmAddNewPrePayeeKA.lblInitial.text);
  frmAddNewPrePayeeKA.lblBillerNumPopup.text = frmAddNewPrePayeeKA.tbxBillerNumber.text;
  frmAddNewPrePayeeKA.flxBillerCategoryPopup.isVisible = true;
  frmAddNewPrePayeeKA.lblBillerNamePopup.isVisible = true;
  frmAddNewPrePayeeKA.lblConfirmBillerCategory.text = frmAddNewPrePayeeKA.lblBillerCategory.text;
  frmAddNewPrePayeeKA.lblConfirmServiceType.text = frmAddNewPrePayeeKA.lblServiceType.text;
  frmAddNewPrePayeeKA.lblConfirmIDType.text = "";//frmAddNewPayeeKA.lblIDType.text;
  frmAddNewPrePayeeKA.lblConfirmIDNumber.text = "";//frmAddNewPayeeKA.tbxBillerID.text;
  frmAddNewPrePayeeKA.lblConfirmNickName.text = frmAddNewPrePayeeKA.tbxNickName.text;
  frmAddNewPrePayeeKA.lblConfirmAddress.text = "";//frmAddNewPayeeKA.tbxAddress.text;
  frmAddNewPrePayeeKA.lblConfirmNationality.text = "";//frmAddNewPayeeKA.lblNationality.text;
  frmAddNewPrePayeeKA.tbxPhoneNo.text = "";
  if(frmAddNewPrePayeeKA.flxDenomination.isVisible === true){
    frmAddNewPrePayeeKA.lblDenominationConf.text = frmAddNewPrePayeeKA.lblDenomination.text;
    frmAddNewPrePayeeKA.lblAmountConf.text = "";
  	frmAddNewPrePayeeKA.flxDenominationConf.setVisibility(true);
  }else{
    frmAddNewPrePayeeKA.lblDenominationConf.text = "";
    frmAddNewPrePayeeKA.lblAmountConf.text = frmAddNewPrePayeeKA.tbxAmount.text;
  	frmAddNewPrePayeeKA.flxAmountConf.setVisibility(true);
  }
  
  frmAddNewPrePayeeKA.flxConfirmPopUp.isVisible = true;
  frmAddNewPrePayeeKA.flxFormMain.isVisible  =false;
};//Type your code here

kony.boj.isNextEnabledPreBiller = function(){
  if((frmAddNewPrePayeeKA.tbxBillerCategory.text === "" || frmAddNewPrePayeeKA.tbxBillerName.text === "" ||
     frmAddNewPrePayeeKA.tbxServiceType.text === "" || frmAddNewPrePayeeKA.tbxBillerNumber.text === "" ||
      frmAddNewPrePayeeKA.tbxDenomination.text === "" ||
     //frmAddNewPayeeKA.tbxBillerID.text === "" || frmAddNewPayeeKA.tbxAddress.text === "" || 
     //frmAddNewPayeeKA.tbxIDType.text === "" || frmAddNewPayeeKA.tbxNationality.text === "" || 
     frmAddNewPrePayeeKA.tbxNickName.text === ""))
    return false;
  return true;
};




kony.boj.resetDropdownPreBiller = function(name){
  var tbx = "tbx" + name;
  var lbl = "lbl" + name;
  var line = "flxUnderline" + name;

  frmAddNewPrePayeeKA[tbx].text = "";
  frmAddNewPrePayeeKA[lbl].skin = "sknLblNextDisabled";
  frmAddNewPrePayeeKA[line].skin = "sknFlxGreyLine";

  switch(name){
    case "BillerCategory":frmAddNewPrePayeeKA[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory");
      					  frmAddNewPrePayeeKA.flxBillerCategory.isVisible = true;
      break;
    case "BillerName":frmAddNewPrePayeeKA[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.billerName");
      				  frmAddNewPrePayeeKA.flxBillerName.isVisible = true;
      break;
    case "ServiceType":frmAddNewPrePayeeKA[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.ServiceType");
      frmAddNewPrePayeeKA.lblserviceTypeHint.isVisible = false;
      break;
    case "Denomination":frmAddNewPrePayeeKA[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.Denomination")
      frmAddNewPrePayeeKA.lblserviceTypeHint.isVisible = true;
      break;
    case "IDType":frmAddNewPrePayeeKA[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.IDType");
      break;
    case "Nationality":frmAddNewPrePayeeKA[lbl].text = kony.i18n.getLocalizedString("i18n.billsPay.Nationality");
      break;
  }
};



kony.boj.resetPreBiller = function(){
  kony.boj.partialResetPreBiller();
  
  kony.boj.resetDropdownPreBiller("BillerCategory");

  kony.boj.resetDropdownPreBiller("BillerName");

  kony.boj.resetDropdownPreBiller("ServiceType");
  
  kony.boj.resetDropdownPreBiller("Denomination");

  kony.boj.resetDropdownPreBiller("IDType");

  kony.boj.resetDropdownPreBiller("Nationality");

//   if(kony.store.getItem("langPrefObj") === "en")
//     kony.boj.lang = "eng";
//   else
//     kony.boj.lang = "ara";
};



kony.boj.partialResetPreBiller = function(){
  frmAddNewPrePayeeKA.tbxBillerNumber.text = "";
  animateLabel("DOWN", "lblBillerNumber", "");
  frmAddNewPrePayeeKA.flxUnderlineBillerNumber.skin = "sknFlxGreyLine";
  
  frmAddNewPrePayeeKA.tbxBillerID.text = "";
  animateLabel("DOWN", "lblBillerID", "");
  frmAddNewPrePayeeKA.flxUnderlineBillerID.skin = "sknFlxGreyLine";
  
  frmAddNewPrePayeeKA.tbxAddress.text = "";
  animateLabel("DOWN", "lblAddress", "");
  frmAddNewPrePayeeKA.flxUnderlineAddress.skin = "sknFlxGreyLine";

  frmAddNewPrePayeeKA.tbxPhoneNo.text = "";
  animateLabel("DOWN", "lblPhoneNo", "");
  frmAddNewPrePayeeKA.flxUnderlinePhoneNo.skin = "sknFlxGreyLine";

  frmAddNewPrePayeeKA.tbxNickName.text = "";
  animateLabel("DOWN", "lblNickName", "");
  frmAddNewPrePayeeKA.flxUnderlineNickName.skin = "sknFlxGreyLine";

  frmAddNewPrePayeeKA.lblNext.skin = "sknLblNextDisabled";
  
  kony.boj.initialiseStoredDetails();
}





/* hassan 16/05/2019 */
function serv_addNewPrePaidBene() {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
        "access": "online",
        "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Payee", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Payee");
    var d = new Date();
     
    if (gblToOTPFrom === "DeleteBiller"){
        dataObject.addField("p_txn_type", "R");
        dataObject.addField("p_NickName", kony.boj.selectedPayee.NickName);
        dataObject.addField("p_customer_id", custid);
        dataObject.addField("p_BillingNo", kony.boj.selectedPayee.BillingNumber);
        dataObject.addField("p_BillerCode", kony.boj.selectedPayee.p_billercode);
        dataObject.addField("p_BillerDesc", kony.boj.selectedPayee.p_billerdesc);

        dataObject.addField("p_ServiceType", kony.boj.selectedPayee.p_servicetype);
        //dataObject.addField("bene_email", frmAddNewPrePayeeKA.tbxServiceType.text);
      
        
        if (kony.boj.selectedPayee.p_denocode !== null && kony.boj.selectedPayee.p_denocode !== undefined && kony.boj.selectedPayee.p_denocode !== ""){
           dataObject.addField("p_DenoDesc", kony.boj.selectedPayee.p_denodesc);
           dataObject.addField("p_DenoCode", kony.boj.selectedPayee.p_denocode);
        }
        else{
           dataObject.addField("p_DenoCode", "");
           dataObject.addField("p_DueAmount", kony.boj.selectedPayee.p_dueamount);
        }
    }else if (gblToOTPFrom === "UpdateBiller"){ // update prepaid biller Hassan
        dataObject.addField("p_txn_type", "U");
        dataObject.addField("p_NickName", frmEditPayeeKA.txtBillerName.text);
        kony.boj.selectedPayee.NickName=frmEditPayeeKA.txtBillerName.text;
        dataObject.addField("p_customer_id", custid);
        dataObject.addField("p_BillingNo", kony.boj.selectedPayee.BillingNumber);
        dataObject.addField("p_BillerCode", kony.boj.selectedPayee.p_billercode);
        dataObject.addField("p_BillerDesc", kony.boj.selectedPayee.p_billerdesc);

        dataObject.addField("p_ServiceType", kony.boj.selectedPayee.p_servicetype);
        //dataObject.addField("bene_email", frmAddNewPrePayeeKA.tbxServiceType.text);
      
        
        if (kony.boj.selectedPayee.p_denocode !== null && kony.boj.selectedPayee.p_denocode !== undefined && kony.boj.selectedPayee.p_denocode !== ""){
           dataObject.addField("p_DenoDesc", kony.boj.selectedPayee.p_denodesc);
           dataObject.addField("p_DenoCode", kony.boj.selectedPayee.p_denocode);
        }
        else{
           dataObject.addField("p_DenoCode", "");
           dataObject.addField("p_DueAmount", kony.boj.selectedPayee.p_dueamount);
        }
    }else
      {
        dataObject.addField("p_txn_type", "A");
        dataObject.addField("p_NickName", frmAddNewPrePayeeKA.tbxNickName.text);
        dataObject.addField("p_customer_id", custid);
        dataObject.addField("p_BillingNo", frmAddNewPrePayeeKA.tbxBillerNumber.text);
        dataObject.addField("p_BillerCode", frmAddNewPrePayeeKA.tbxBillerName.text);
        dataObject.addField("p_BillerDesc", frmAddNewPrePayeeKA.lblBillerName.text);

        //dataObject.addField("p_ServiceType", frmAddNewPrePayeeKA.lblServiceType.text);
        dataObject.addField("p_ServiceType", frmAddNewPrePayeeKA.tbxServiceType.text);

        if (frmAddNewPrePayeeKA.lblDenomination.text !== geti18nkey("i18n.prepaid.denomination")){
           dataObject.addField("p_DenoDesc", frmAddNewPrePayeeKA.tbxDenomination.text);
           dataObject.addField("p_DenoCode", frmAddNewPrePayeeKA.lblDenomination.text);
        }
        else{
           dataObject.addField("p_DenoCode", "");
           dataObject.addField("p_DueAmount", frmAddNewPrePayeeKA.tbxAmount.text.replace(/,/g,""));
        }
      }
    

  
    //dataObject.addField("bene_address1", frmAddNewPrePayeeKA.lblDenomination.text);

    //	dataObject.addField("p_user_id","BOJMOB");
    //	dataObject.addField("p_channel","MOBILE");
    var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
    };
    kony.print("service input paramerters ::" + JSON.stringify(dataObject));
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("addBenePrePaid", serviceOptions, function(res) {
            kony.print("Success ::" + JSON.stringify(res));
          
            if (gblToOTPFrom === "DeleteBiller"){
              popupCommonAlertDimiss();
              if(res.pkg_host_ref_no === "0" && res.pkg_icbs_ret_code === "00000"){//geti18nkey("i18n.common.ReferenceNumber")+" " + res.p_EfawateercomID
                      var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                      var controller = INSTANCE.getFormController("frmManagePayeeKA");
                      var navObject = new kony.sdk.mvvm.NavigationObject();
                      navObject.setRequestOptions("managepayeesegment",{"headers":{},"queryParams":{"custId": custid,
                                          "P_BENE_TYPE": "PRE",
                                       "lang":(kony.store.getItem("langPrefObj")==="en")?"eng":"ara"}});
                      controller.loadDataAndShowForm(navObject);
              }
              else{
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                var Message = getErrorMessage(res.pkg_icbs_ret_code);
                if(res.pkg_host_ref_no === "00029"){
                    Message = geti18Value("i18n.common.somethingwentwrong");
                   }

               }
            }else{
            	addNewPrePaidBene(res);
            	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            }
        }, function(err) {
            kony.print("failed ::" + JSON.stringify(err));
            /*if(operationName === "InternetMailorder"){
            		customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.ecommerce.failed"), popupCommonAlertDimiss, "");
                }else{*/
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
            //kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"),geti18nkey("i18n.cards.gotoCards"),"Cards","","");
            //}
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        });
    }
}

function addNewPrePaidBene(res) {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      if(res.pkg_host_ref_no === "0" && res.pkg_icbs_ret_code === "00000"){//geti18nkey("i18n.common.ReferenceNumber")+" " + res.p_EfawateercomID
        var msg ="";
        if (gblToOTPFrom === "UpdateBiller"){
           msg = geti18Value("i18n.updatebeneficiary.successmsg");
           kony.boj.populateSuccessScreen("success.png", 
                                         frmEditPayeeKA.txtBillerName.text+" "+msg, 
                                         "", 
                                         geti18nkey("i18n.common.CgtAD"), 
                                         geti18nkey ("i18n.common.AccountDashboard"), geti18Value("i18n.common.CgtBL"), 
                                         "ManagePayee", geti18Value("i18n.common.paynow"), "PayPrePaidBill");
        }
        else{
           msg = geti18Value("i18n.common.benificaryadded").split("name");
           kony.boj.populateSuccessScreen("success.png", 
                                         msg[0]+frmAddNewPrePayeeKA.tbxNickName.text+msg[1], 
                                         "", 
                                         geti18nkey("i18n.common.CgtAD"), 
                                         geti18nkey ("i18n.common.AccountDashboard"), geti18Value("i18n.common.CgtBL"), 
                                         "ManagePayee", geti18Value("i18n.common.paynow"), "PayPrePaidBill");
        }

      }
      else{
        var Message = getErrorMessage(res.pkg_icbs_ret_code);
        if(res.pkg_host_ref_no === "72018" || res.pkg_icbs_ret_code === "72004"){
        	Message = geti18Value("i18n.common.somethingwentwrong");
        }
        customAlertPopup(geti18Value("i18n.common.alert"), Message,popupCommonAlertDimiss,"", geti18nkey("i18n.settings.ok"), "");
       /* frmAddNewPrePayeeKA.flxConfirmPopUp.isVisible = false;
        frmAddNewPrePayeeKA.flxFormMain.isVisible = true;
        frmAddNewPrePayeeKA.show();*/
      }
}



// fill segment all bene
function getAllBene(){


  for(var i in kony.boj.AllPayeeList){
      //        kony.boj.AllPayeeList[i].BillerCode.toLowerCase().indexOf(searchTerm) != -1){
      kony.boj.AllPayeeList[i].initial = kony.boj.AllPayeeList[i].NickName.substring(0,2).toUpperCase();
      data.push(kony.boj.AllPayeeList[i]);
  }

  frmManagePayeeKA.managepayeesegment.setData([]);
	kony.print("search results ::"+JSON.stringify(data));
  frmManagePayeeKA.managepayeesegment.widgetDataMap = {
    payeename: "NickName",
    accountnumber :"BillingNumber",    
    lblBillerType: "BillerCode",
    lblInitial: "initial",
    flxIcon1: "icon",
    btnBillsPayAccounts:"btnSetting"// hassan 
    };
  
  if(data.length === 0){
    frmManagePayeeKA.managepayeesegment.isVisible = false;
    frmManagePayeeKA.lblSearchResult.isVisible = true;
  }
  else{
    frmManagePayeeKA.managepayeesegment.isVisible = true;
    frmManagePayeeKA.lblSearchResult.isVisible = false;
   // frmManagePayeeKA.managepayeesegment.setData(data);
  }
 // serv_getPrePaidBillerData();
  
}


/* fill segment */
function serv_getPrePaidBillerData(response,scopeObj){
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
        "access": "online",
        "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Payee", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Payee");
  
    dataObject.addField("p_cust_id", custid);
    //	dataObject.addField("p_user_id","BOJMOB");
    //	dataObject.addField("p_channel","MOBILE");
    var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
    };
  
    kony.print("service input paramerters ::" + JSON.stringify(dataObject));
  
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("getPrePaidBillers", serviceOptions, function(res) {
            kony.print("Success ::" + JSON.stringify(res));
            getPrePaidBillerData(res,response,scopeObj);
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }, function(err) {
            kony.print("failed ::" + JSON.stringify(err));
            /*if(operationName === "InternetMailorder"){
            		customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.ecommerce.failed"), popupCommonAlertDimiss, "");
                }else{*/
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
            //kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"),geti18nkey("i18n.cards.gotoCards"),"Cards","","");
            //}
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        });
    }
}



function getPrePaidBillerData(res,response,scopeObj){
//  frmManagePayeeKA.managepayeesegment.setVisibility(true);
 // frmManagePayeeKA.manageAllBeneSegment.setVisibility(false);
 // frmManagePayeeKA.manageprepayeesegment.setVisibility(false);
  kony.boj.selectedBillerType = "PrePaid";
  
 /* frmManagePayeeKA.manageprepayeesegment.setData([]);
  frmManagePayeeKA.manageprepayeesegment.widgetDataMap = { 
    lbltmpTitle: "lbltmpTitle",
    accountNumber :"benAcctNo",
    BenificiaryName: "nickName",
    BenificiaryFullName: "bankName",
    dueAmount : "benBranchNo",
    lblInitial: "initial",
    flxIcon1: "icon",
  };*/
  
 /* frmManagePayeeKA.managepayeesegment.widgetDataMap = { 
    payeename: "p_NickName",
    p_BillingNo :"benAcctNo",
    lblBillerType: "p_BillerCode",
    lblInitial: "initial",
    flxIcon1: "icon",
    btnBillsPayAccounts:"btnSetting"// hassan 
  };*/ // hassan 04/08/2019
  
  tempArr = [];
  data = [];
  prePaidData = [];  // hassan 04/08/2019
 // frmManagePayeeKA.managepayeesegment.setData([]); // hassan 04/08/2019
  for (var i in res.BList) {
       // res[i].initial = res[i].code_desc.substring(0, 2).toUpperCase();
        res.BList[i].initial = res.BList[i].p_nickname.substring(0,2).toUpperCase();
        res.BList[i].icon = {
          backgroundColor: kony.boj.getBackGroundColour(res.BList[i].initial)
        };
      /*  tempArr = {"BillerCode":res.records[0].beneList[i].bene_address2,"ServiceType":res.records[0].beneList[i].bene_city,
                   "dueamount":res.records[0].beneList[i].benBranchNo + "JOD",
                   "BillingNumber":res.records[0].beneList[i].benAcctNo,"NickName":res.records[0].beneList[i].NickName,
                   "billerName":res.records[0].beneList[i].bene_address1,"initial":"KO","icon":{"backgroundColor":"167047"}};  */    
    data.push(res.BList[i]);
    prePaidData.push(res.BList[i]);
  }
  
 // hasssaaan  
 
  kony.boj.Biller[kony.boj.selectedBillerType].selectedList = tempArr;
   kony.print("hassan prePaidData ::" + JSON.stringify(prePaidData));
   //frmManagePayeeKA.manageprepayeesegment.setData(data);
   //frmManagePayeeKA.manageprepayeesegment.setData(data);
   kony.print("hassan array ::" + JSON.stringify(data));
  
  
   var formattedResponse = scopeObj.getController().processData(response);
   scopeObj.bindData(formattedResponse);
   //frmManagePayeeKA.managepayeesegment.setData(data); // hassan 04/08/2019
}



/* 
function getPrePaidBillerData(res){
  frmManagePayeeKA.managepayeesegment.setVisibility(false);
  frmManagePayeeKA.manageAllBeneSegment.setVisibility(false);
  frmManagePayeeKA.manageprepayeesegment.setVisibility(true);
  kony.boj.selectedBillerType = "PrePaid";
  
  frmManagePayeeKA.manageprepayeesegment.setData([]);
  frmManagePayeeKA.manageprepayeesegment.widgetDataMap = { 
    lbltmpTitle: "lbltmpTitle",
    accountNumber :"benAcctNo",
    BenificiaryName: "nickName",
    BenificiaryFullName: "bankName",
    dueAmount : "benBranchNo",
    lblInitial: "initial",
    flxIcon1: "icon",
  };

  tempArr = [];
  for (var i in res.records[0].beneList) {
       // res[i].initial = res[i].code_desc.substring(0, 2).toUpperCase();
        res.records[0].beneList[i].initial = res.records[0].beneList[i].nickName.substring(0,2).toUpperCase();
        res.records[0].beneList[i].icon = {
          backgroundColor: kony.boj.getBackGroundColour(res.records[0].beneList[i].initial)
        };
        tempArr = [{"NickName":res.records[0].beneList[i].nickName,"BillingNumber":res.records[0].beneList[i].benAcctNo,
                   "BillerCode":res.records[0].beneList[i].bene_address2}];      
        data.push(tempArr);
  }
 // hasssaaan  
 
  kony.boj.Biller[kony.boj.selectedBillerType].selectedList = tempArr;
  
   //frmManagePayeeKA.manageprepayeesegment.setData(data);
   frmManagePayeeKA.manageprepayeesegment.setData(data);
   kony.print("hassan array ::" + JSON.stringify(data));

}*/

/* prepaidpayeedetails screen flxnewbill*/
function fillDetailsPrePaidData(){
  var inputs = kony.store.getItem("PrepaidInputs");
  frmPrePaidPayeeDetailsKA.flxNewBill.setVisibility(true);
  frmPrePaidPayeeDetailsKA.flxBiller.setVisibility(false);
  frmPrePaidPayeeDetailsKA.btnPayNow.setVisibility(false);
  frmPrePaidPayeeDetailsKA.flxDenomination.setVisibility(false);
  frmPrePaidPayeeDetailsKA.flxAmount.setVisibility(false);
  
  if(frmPrePaidPayeeDetailsKA.txtDenomination.text !== " " && frmPrePaidPayeeDetailsKA.txtDenomination.text !==null){
      frmPrePaidPayeeDetailsKA.flxDenomination.setVisibility(true);
      frmPrePaidPayeeDetailsKA.txtAmount.text = "";
  }
  else if(frmPrePaidPayeeDetailsKA.txtAmount.text !== "" && frmPrePaidPayeeDetailsKA.txtAmount.text !== null){
      frmPrePaidPayeeDetailsKA.flxAmount.setVisibility(true);
      frmPrePaidPayeeDetailsKA.txtDenomination.text = "";
  }
    
 // frmPrePaidPayeeDetailsKA.txtServiceType.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedList[0].beneficiaryName;
 // frmPrePaidPayeeDetailsKA.tbxBillerCategory = frmPrePaidPayeeDetailsKA;
  //frmPrePaidPayeeDetailsKA.tbxBillerName = 
  //frmPrePaidPayeeDetailsKA.tbxServiceType
 /* if (inputs.deno_flag == "true"){
      frmPrePaidPayeeDetailsKA.flxAmount.setVisibility(false);
      frmPrePaidPayeeDetailsKA.flxDenomination.setVisibility(true);
      frmPrePaidPayeeDetailsKA.txtDenomination.text = frmPrePaidPayeeDetailsKA.lblDenoDesc.text;
      frmPrePaidPayeeDetailsKA.txtAmount.text = "";
  }else
    { frmPrePaidPayeeDetailsKA.flxAmount.setVisibility(true);
      frmPrePaidPayeeDetailsKA.flxDenomination.setVisibility(false);
      frmPrePaidPayeeDetailsKA.txtDenomination.text = "";
      frmPrePaidPayeeDetailsKA.txtAmount.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedList[0].benBranchNo;
    }*/

  
}


/* hassan 16/05/2019 */

    kony.boj.preshowPrePaidPayeeDetails = function(selectedData){
      var inputs = kony.store.getItem("PrepaidInputs");
      var deno_desc = kony.store.getItem("deno_desc");
      kony.boj.selectedPayee = selectedData;

      frmPrePaidPayeeDetailsKA.flxNewBill.setVisibility(false);
      frmPrePaidPayeeDetailsKA.flxBiller.setVisibility(true);
      frmPrePaidPayeeDetailsKA.btnPayNow.setVisibility(true);
      
      frmPrePaidPayeeDetailsKA.lblName.text = selectedData.nickName;
      frmPrePaidPayeeDetailsKA.lblType.text = "PREPAID";//kony.boj.selectedPayee.BillerCode;
      //frmPrePaidPayeeDetailsKA.lblBillerCode.text = selectedData.benAcctNo;
      frmPrePaidPayeeDetailsKA.lblBillNumber.text = selectedData.BillingNumber;
      if(selectedData.bankName.length > 2){
        frmPrePaidPayeeDetailsKA.lblDenoDesc.text =  selectedData.p_denodesc;
        frmPrePaidPayeeDetailsKA.lblDenoDesc.setVisibility(true);
        frmPrePaidPayeeDetailsKA.lblDueAmount.setVisibility(false);
        frmPrePaidPayeeDetailsKA.lblDueAmount.text = "";
      }
      else{
        frmPrePaidPayeeDetailsKA.lblDenoDesc.text = "";
        frmPrePaidPayeeDetailsKA.lblDenoDesc.setVisibility(false);
        frmPrePaidPayeeDetailsKA.lblDueAmount.setVisibility(true);
        frmPrePaidPayeeDetailsKA.lblDueAmount.text = geti18Value("i18n.billsPay.Amount") + " " + selectedData.benBranchNo.replace(/,/g,"");
      }
      
      frmPrePaidPayeeDetailsKA.lblServiceType.text = selectedData.p_servicetype;
      if(selectedData.nickName != null && selectedData.nickName != "")
        frmPrePaidPayeeDetailsKA.lblInitial.text = selectedData.nickName.substring(0,2).toUpperCase();
      else{
        frmPrePaidPayeeDetailsKA.lblInitial.text = "";}
      
        frmPrePaidPayeeDetailsKA.lblOperation.text = "R";
        frmPrePaidPayeeDetailsKA.lblDummy.text = "";
      

      	frmPrePaidPayeeDetailsKA.show();
    };



/* get all data for specific beneficiary */
function serv_getPrePaidBeneData(data){

   /* var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
        "access": "online",
        "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("ExternalAccounts", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts");
   
    dataObject.addField("P_BENE_TYPE", "PRE"); 
    dataObject.addField("custId", custid);
    //	dataObject.addField("p_user_id","BOJMOB");
    //	dataObject.addField("p_channel","MOBILE");
    var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
    };
    kony.print("service input paramerters ::" + JSON.stringify(dataObject));
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("get", serviceOptions, function(res) {
            kony.print("Success ::" + JSON.stringify(res));
            getPrePaidBeneData(res);
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }, function(err) {
            kony.print("failed ::" + JSON.stringify(err));
            /*if(operationName === "InternetMailorder"){
            		customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.ecommerce.failed"), popupCommonAlertDimiss, "");
                }else{
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
            //kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"),geti18nkey("i18n.cards.gotoCards"),"Cards","","");
            //}
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        });
    }*/
  getPrePaidBeneData(data);
}


function getPrePaidBeneData(res){
   // var inputs = kony.store.getItem("PrepaidInputs");

    kony.boj.selectedBillerType = "PrePaid";
  //  for (var i in res.records[0].beneList) {
         // res[i].initial = res[i].code_desc.substring(0, 2).toUpperCase();

   // if (res.records[0].beneList[i].benAcctNo === kony.boj.selectedPayee.BillingNumber){
        /*frmPrePaidPayeeDetailsKA.txtBillerName.text = res.records[0].beneList[i].bene_address1;
        frmPrePaidPayeeDetailsKA.txtServiceType.text = res.records[0].beneList[i].bene_city;
        frmPrePaidPayeeDetailsKA.txtDenomination.text = res.records[0].beneList[i].bankName;
        frmPrePaidPayeeDetailsKA.txtBillerCode.text = res.records[0].beneList[i].bene_address2;
        frmPrePaidPayeeDetailsKA.txtServiceCode.text = res.records[0].beneList[i].bene_email;
        frmPrePaidPayeeDetailsKA.txtDenominationCode.text = res.records[0].beneList[i].bene_bank_city;
        frmPrePaidPayeeDetailsKA.txtBillingNumber.text = res.records[0].beneList[i].benAcctNo;*/
  
  		kony.print("hassan res"+JSON.stringify(res)); 
        if(res.NickName !== undefined && res.NickName !==null && res.NickName !=="" && res.NickName != "0"){
          kony.boj.selectedPayee.NickName =res.NickName;
       }else if(res.nickName !== undefined && res.nickName !==null && res.nickName !=="" && res.nickName != "0"){
          kony.boj.selectedPayee.NickName =res.nickName;
       }
       
  
  		frmPrePaidPayeeDetailsKA.flxMain.isVisible = true;
        frmPrePaidPayeeDetailsKA.flxConfirmPopUp.isVisible = false;
  
        frmPrePaidPayeeDetailsKA.txtBillerName.text = res.p_billerdesc;
        frmPrePaidPayeeDetailsKA.txtServiceType.text = res.p_servicetype;      
        frmPrePaidPayeeDetailsKA.txtBillerCode.text = res.p_billercode;
        frmPrePaidPayeeDetailsKA.txtServiceCode.text = res.p_servicetype;
        frmPrePaidPayeeDetailsKA.txtBillingNumber.text = res.BillingNumber;
        frmPrePaidPayeeDetailsKA.tbxNickName.text=res.NickName;// hassan BMA-1968
		frmPrePaidPayeeDetailsKA.flxDenomination.setVisibility(false);
		frmPrePaidPayeeDetailsKA.flxAmount.setVisibility(false);
		
       if (res.p_denocode !== null && res.p_denocode !== undefined && res.p_denocode !== ""){
           frmPrePaidPayeeDetailsKA.txtDenomination.text = res.p_denocode;
           frmPrePaidPayeeDetailsKA.txtDenominationCode.text = res.p_denodesc;
       	  	if(res.p_denocode.length > 2){
              frmPrePaidPayeeDetailsKA.flxDenomination.setVisibility(true);
              frmPrePaidPayeeDetailsKA.txtAmount.text = "";
            }
       }else if(res.p_dueamount !== undefined && res.p_dueamount !== null && res.p_dueamount !== ""){
         frmPrePaidPayeeDetailsKA.flxAmount.setVisibility(true);
         frmPrePaidPayeeDetailsKA.txtDenomination.text = "";
         frmPrePaidPayeeDetailsKA.txtAmount.text = formatAmountwithcomma(res.p_dueamount, 3);
       }
  
        
 //   }

  //  }
    //frmPrePaidPayeeDetailsKA.flxNewBill.setVisibility(true);
    //frmPrePaidPayeeDetailsKA.flxBiller.setVisibility(false);
    //frmPrePaidPayeeDetailsKA.btnPayNow.setVisibility(false);
    frmPrePaidPayeeDetailsKA.lblNext.skin = "sknLblNextDisabled";
    frmPrePaidPayeeDetailsKA.lblPaymentMode.text = geti18Value("i18n.billsPay.Accounts");
	gblTModule = "BillPrePayNewBillKA";
	frmPrePaidPayeeDetailsKA.flxConversionAmt.setVisibility(false);
	check_numberOfAccounts();
    frmPrePaidPayeeDetailsKA.show();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}



/* Called Validation service*/
function getBOJBillDetailsPrepaid()
{
  if(kony.sdk.isNetworkAvailable()){
    ShowLoadingScreen();

    var langSelected = kony.store.getItem("langPrefObj");
    var Language = langSelected.toUpperCase();
    //var billingNumber = frmAddNewPrePayeeKA.tbxBillerNumber.text;
    
    var billingNumber = frmPrePaidPayeeDetailsKA.txtBillingNumber.text;

   // billingNumber = billingNumber.replace(/\s/g,"");
    
    var acc = kony.store.getItem("BillPayfromAcc");
    var inputs = kony.store.getItem("PrepaidInputs");
   // var deno_desc = frmPrePaidPayeeDetailsKA.;
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var prePaidInputs = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Payee", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Payee");

    dataObject.addField("custId",custid);
    dataObject.addField("lang",Language);
    //dataObject.addField("p_biller_code",kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code);
    //dataObject.addField("p_serv_type_code",kony.boj.Biller[kony.boj.selectedBillerType].selectedType.serv_type_code);
    kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller = {"code":frmPrePaidPayeeDetailsKA.txtBillerCode.text,"code_desc":frmPrePaidPayeeDetailsKA.txtBillerCode.text};
    //1967 fix
	var checkNickName=kony.boj.selectedPayee.NickName;
   
        kony.print("kony.boj.selectedPayee.NickName"+kony.boj.selectedPayee.NickName);
        if(checkNickName !== undefined && checkNickName !==null && checkNickName !=="" && checkNickName != "0")
      {
        kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory = {"code_desc":kony.boj.selectedPayee.NickName};
      kony.print("nickname is not empty *"+kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc);
      }
    //end
    //kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory = {"code_desc":frmPrePaidPayeeDetailsKA.txtBillerName.text}; //{"code_desc":"test"};//commented for 1967 fix
    
    dataObject.addField("p_biller_code",frmPrePaidPayeeDetailsKA.txtBillerCode.text);
    dataObject.addField("p_serv_type_code",frmPrePaidPayeeDetailsKA.txtServiceCode.text);
    
    //alert(kony.boj.Biller[kony.boj.selectedBillerType].selectedType.serv_type_code);
    
    if(billingNumber.length > 1)  {
     // inputs.billing_no_flag = "true";
      dataObject.addField("p_billing_no",billingNumber);
    }
    else{
     // inputs.billing_no_flag = "false";
      dataObject.addField("p_billing_no","");
    }
    if(frmPrePaidPayeeDetailsKA.txtDenomination.text.length > 2){
      //dataObject.addField("P_deno",kony.boj.Biller[kony.boj.selectedBillerType].selectedDenoType.deno_code);
    //  inputs.deno_flag = "true";
      dataObject.addField("amount","0");//deno_desc); amount field should not contains charactes hassan
      dataObject.addField("P_deno",frmPrePaidPayeeDetailsKA.txtDenominationCode.text);
      kony.boj.Biller[kony.boj.selectedBillerType].selectedDenoType= {"deno_code":frmPrePaidPayeeDetailsKA.txtDenominationCode.text};
    }
    else{
   //   inputs.deno_flag = "false";
      dataObject.addField("P_deno","");
      dataObject.addField("amount",frmPrePaidPayeeDetailsKA.txtAmount.text.replace(/,/g,""));
      }
  
      dataObject.addField("p_fcdb_ref_no","");
   //  kony.store.setItem("PrepaidInputs",inputs);
    
    if(frmPrePaidPayeeDetailsKA.btnBillsPayAccounts.text === "t"){
      dataObject.addField("p_acc_or_cr",acc.accountID);
      dataObject.addField("p_account_br",acc.branchNumber);
    }
    else{
      dataObject.addField("p_acc_or_cr",acc.card_num);
      dataObject.addField("p_account_br","");
    }

    //dataObject.addField("fees",""); hassan 
    dataObject.addField("p_paymt_type","BillNew");

    var serviceOptions = {"dataObject":dataObject,"headers":headers};
    kony.print("Input params getBillDetailsPostpaid-->"+ serviceOptions );
    kony.print("Input params getBillDetailsPostpaid-->"+ JSON.stringify(serviceOptions) );
    modelObj.customVerb("PrePaidValDebit",serviceOptions, getBOJBillDetailsPrepaidSuccess, getBillDetailsPrepaidError);
  }else{
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}


function getBOJBillDetailsPrepaidSuccess(response)
{
  kony.application.dismissLoadingScreen();
  if(response.ErrorCode === "00000" && response.FeeOutput !== undefined && response.FeeOutput[0].ErrorCode === "00000"){
    //alert(JSON.stringify(response));
    kony.print("Success getBillDetailsPrepaid-->"+ JSON.stringify(response) );
    // alert("Success!!");
    var storeData = isEmpty(response) ? "" : response;
    kony.store.setItem("PrepaidResponse",storeData);
    //     prePaidNewBillDetails(response);
    //serv_BILLSCOMISSIONCHARGE(frmNewBillKA.tbxAmount.text.replace(/,/g,""));
    if (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD" &&
         frmPrePaidPayeeDetailsKA.flxAmount.isVisible === true && frmPrePaidPayeeDetailsKA.btnBillsPayCards.isVisible === false){
    	serv_BILLSCOMISSIONCHARGE(frmPrePaidPayeeDetailsKA.txtAmount.text.replace(/,/g,""));
    }
    gblQuickFlow = "preBene";
    assignDatatoConfirmPrepaidBene();
    QuickFlowprev = "now";
  }
  else{
    kony.print("Success getBillDetailsPrepaid-->"+ JSON.stringify(response) );
    var Message = getServiceErrorMessage(response.FeeOutput[0].ErrorCode,"getBillDetailsPrepaidSuccess");
    customAlertPopup(geti18Value("i18n.Bene.Failed"), Message,popupCommonAlertDimiss,"");
    //kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.transactionFailed"), geti18Value("i18n.Transfer.AnotherTransaction"), "PaymentDashboard",geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard"));
    //frmCongratulations.show();
  }
}


function getBOJBillDetailsPrepaidError(response)
{
  kony.application.dismissLoadingScreen();
  //alert(JSON.stringify(response));
  kony.print("Failure getBillDetailsPostpaid-->"+ JSON.stringify(response) );
  alert("Failure!!");
}




/* to confirm screen */
function assignDatatoConfirmPrepaidBene()
{
  var x = kony.store.getItem("PrepaidResponse");
  //   alert("x--->"+JSON.stringify(x));
  //   kony.print("x--->"+JSON.stringify(x));
  var res = kony.store.getItem("PrepaidResponse");
  var inputs = kony.store.getItem("PrepaidInputs");
  var billingNumber = frmPrePaidPayeeDetailsKA.txtBillingNumber.text.replace(/\s/g,"");
  var acc = kony.store.getItem("BillPayfromAcc");	
	kony.print("hassan inputs ::"+JSON.stringify(inputs));
  kony.print("hassan acc ::"+JSON.stringify(acc));
  if(frmNewBillDetails.btnBillsPayAccounts.text === "t"){
    frmConfirmPrePayBill.lblSOFAccount.text = acc.accountName;
    frmConfirmPrePayBill.lblSOFAccNum.text = acc.accountID;
    frmConfirmPrePayBill.prAccorCCNo.text = acc.accountID;
    frmConfirmPrePayBill.prAccBr.text = acc.branchNumber;
    frmConfirmPrePayBill.lblPaymentMethod.text = "ACTDEB";
  }
  else{
    frmConfirmPrePayBill.lblSOFAccount.text = acc.accountName;// hassan pay from credit card
    frmConfirmPrePayBill.lblSOFAccNum.text = acc.hiddenAccountNumber;// hassan pay from credit card
    frmConfirmPrePayBill.prAccorCCNo.text = acc.card_num;// hassan pay from credit card
    frmConfirmPrePayBill.prAccBr.text = "";
    frmConfirmPrePayBill.lblPaymentMethod.text = "CCARD";
  }
  // hassan pay from credit card
  if(frmPrePaidPayeeDetailsKA.btnBillsPayAccounts.text === "t"){
    frmConfirmPrePayBill.lblSOFAccount.text = acc.accountName;
    frmConfirmPrePayBill.lblSOFAccNum.text = acc.accountID;
    frmConfirmPrePayBill.prAccorCCNo.text = acc.accountID;
    frmConfirmPrePayBill.prAccBr.text = acc.branchNumber;
    frmConfirmPrePayBill.lblPaymentMethod.text = "ACTDEB";
  }
  else{
    frmConfirmPrePayBill.lblSOFAccount.text = acc.accountName;// hassan pay from credit card
    frmConfirmPrePayBill.lblSOFAccNum.text = acc.hiddenAccountNumber;// hassan pay from credit card
    frmConfirmPrePayBill.prAccorCCNo.text = acc.card_num;
    frmConfirmPrePayBill.prAccBr.text = "";
    frmConfirmPrePayBill.lblPaymentMethod.text = "CCARD";
  }
 // hassan pay from credit card
  frmConfirmPrePayBill.prBillerCode.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code;
  frmConfirmPrePayBill.prServiceCode.text = frmPrePaidPayeeDetailsKA.txtServiceCode.text;
  
  if(billingNumber.length > 1)
    frmConfirmPrePayBill.prBillingNo.text = billingNumber;
  else
    frmConfirmPrePayBill.prBillingNo.text = "";
  
  if(frmPrePaidPayeeDetailsKA.txtDenomination.text.length > 2)
    frmConfirmPrePayBill.prdenoFlag.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedDenoType.deno_code;
  else
    frmConfirmPrePayBill.prdenoFlag.text = "";
  
  // hassan BMA-1968
  if (frmPrePaidPayeeDetailsKA.tbxNickName.text === "" || frmPrePaidPayeeDetailsKA.tbxNickName.text === null){
    frmConfirmPrePayBill.lblBillerCategory.text = frmPrePaidPayeeDetailsKA.txtBillerName.text; // hassan BMA-1968
    frmConfirmPrePayBill.initialsCategory.text  = frmPrePaidPayeeDetailsKA.txtBillerName.text.substring(0,2).toUpperCase();// hassan BMA-1968
  }
  else{  
    frmConfirmPrePayBill.lblBillerCategory.text = frmPrePaidPayeeDetailsKA.tbxNickName.text; // hassan BMA-1968
    frmConfirmPrePayBill.initialsCategory.text  = frmPrePaidPayeeDetailsKA.tbxNickName.text.substring(0,2).toUpperCase();// hassan BMA-1968
  }
  // hassan BMA-1968
  
  frmConfirmPrePayBill.prValidationCode.text = x.FeeOutput[0].ValidationCode;
  frmConfirmPrePayBill.prDueAmount.text = x.FeeOutput[0].DueAmount;
  frmConfirmPrePayBill.prMobileNo.text = "0797545415";//customerAccountDetails.profileDetails.smsno;
  frmConfirmPrePayBill.prPaymentType.text = "A";
  frmConfirmPrePayBill.lblTransferFlag.text = "P";

  frmConfirmPrePayBill.lblBillerCode.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code;
  frmConfirmPrePayBill.lblCustID.text = custid;
  frmConfirmPrePayBill.lblLang.text = "ara";
  frmConfirmPrePayBill.lblAccountNumber.text = acc.accountID;
  frmConfirmPrePayBill.lblAccBr.text = acc.branchNumber;
  frmConfirmPrePayBill.lblSvcType.text = frmPrePaidPayeeDetailsKA.txtServiceCode.text;
  frmConfirmPrePayBill.lblBillingNumber.text = frmPrePaidPayeeDetailsKA.txtBillingNumber.text.replace(/\s/g,"");
  frmConfirmPrePayBill.lblPaidAmnt.text = x.p_debit_amt;
  frmConfirmPrePayBill.lblDueAmnt.text = "";
  frmConfirmPrePayBill.lblAccessChannel.text = "MOBILE";
  //Change when we have cards service
  frmConfirmPrePayBill.lblSendSMSflag.text = "N";
  frmConfirmPrePayBill.lblCustInfoFlag.text = "Y";
  frmConfirmPrePayBill.lblPaymentStatus.text = "BillNew";
  frmConfirmPrePayBill.lblFeeAmnt.text = x.FeeOutput[0].fees;
  frmConfirmPrePayBill.lblFeeOnBiller.text = "";
  frmConfirmPrePayBill.lblBillNo.text = "";
  frmConfirmPrePayBill.lblfcbdrefNo.text = "";
  frmConfirmPrePayBill.lblPID.text = "";
  frmConfirmPrePayBill.lblMobileNumber.text = "";
  frmConfirmPrePayBill.pidType.text = "";
  frmConfirmPrePayBill.pNation.text = "";
  //   alert("ValidationCode "+x.FeeOutput[0].ValidationCode);
  //   alert("lblValidationCode "+frmConfirmPayBill.prValidationCode.text);
  loadDetailstoConfirmPrepaidBene();
}


function loadDetailstoConfirmPrepaidBene()
{
  
  var acc = kony.store.getItem("BillPayfromAcc");
  var amntres =  kony.store.getItem("PrepaidResponse");
  var deno = kony.store.getItem("PrepaidInputs");
  var billingNumber = frmPrePaidPayeeDetailsKA.txtBillingNumber.text.replace(/\s/g,"");
  
  frmConfirmPrePayBill.flxImpDetails.setVisibility(true);
  //1967 fix
kony.print(" In loadDetailstoConfirmPrepaidBene"+kony.boj.selectedPayee.NickName);
     var checkNickName=kony.boj.selectedPayee.NickName;
    if(checkNickName !== undefined && checkNickName !==null && checkNickName !=="" && checkNickName != "0")
      {
        kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory = {"code_desc":kony.boj.selectedPayee.NickName};
      kony.print("nickname is not empty *"+kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc);
      }
    else
      {
        kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory = {"code_desc":frmPrePaidPayeeDetailsKA.txtBillerName.text};
      kony.print("nickname is empty *"+kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc);
      }
    //end

  
  //frmConfirmPrePayBill.lblBillerCategory.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc;// hassan BMA-1968
  //frmConfirmPrePayBill.initialsCategory.text = getInitials(kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory.code_desc); // hassan BMA-1968
  //frmConfirmPrePayBill.lblBillerName.text = kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller.code_desc;
  //frmConfirmPrePayBill.lblBillerName.text = frmPrePaidPayeeDetailsKA.txtBillerName.text; // hassan BMA-1968
  
  frmConfirmPrePayBill.lblServiceType.text = frmPrePaidPayeeDetailsKA.txtServiceType.text;
  frmConfirmPrePayBill.lblBillerNumber.text = frmPrePaidPayeeDetailsKA.txtBillingNumber.text.replace(/\s/g,"");

  //frmConfirmPrePayBill.lblConfirmDueAmount.text = setDecimal(frmNewBillKA.tbxAmount.text.replace(/,/g,""), 3) + " JOD";
  frmConfirmPrePayBill.lblConfirmDueAmount.text = setDecimal(frmConfirmPrePayBill.prDueAmount.text.replace(/,/g,""), 3) + " JOD";
  
  frmConfirmPrePayBill.lblFeeAmount.text = setDecimal(amntres.FeeOutput[0].fees, 3) + " JOD";
//   frmConfirmPayBill.lblTotalAmount.text = setDecimal(amntres.FeeOutput[0].DueAmount,3) + " JOD";
  frmConfirmPrePayBill.lblCurrencyType.text = "JOD";
  //frmConfirmPayBill.flxDueAmount.setVisibility(false);
  if(billingNumber.length > 1){

    frmConfirmPrePayBill.flxBillerNumber.setVisibility(true);
    frmConfirmPrePayBill.lblBillerNumber.setVisibility(true);
    frmConfirmPrePayBill.lblBillerNumber.text =frmPrePaidPayeeDetailsKA.txtBillingNumber.text;
  }
  else{
    frmConfirmPrePayBill.flxBillerNumber.setVisibility(false);
  }
   if(frmPrePaidPayeeDetailsKA.txtDenomination.text.length > 2){
    frmConfirmPrePayBill.flxDenomination.setVisibility(true);
    frmConfirmPrePayBill.lblConfirmDenomination.text = frmPrePaidPayeeDetailsKA.txtDenomination.text;
   }
  else{
    frmConfirmPrePayBill.flxDenomination.setVisibility(false);
  }

  frmConfirmPrePayBill.show();
}



function performBillPayPrePaid(){
  if(kony.sdk.isNetworkAvailable()){
    if(frmConfirmPrePayBill.lblPaymentMethod.text === "ACTDEB")// hassan pay from credit card
    {
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var controller = INSTANCE.getFormController("frmConfirmPrePayBill");
      var controllerContextData = controller.getContextData() || new kony.sdk.mvvm.NavigationObject();
      controllerContextData.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
      controller.setContextData(controllerContextData);
      controller.performAction("saveData");
    }
    else
    {
      CallServicePrePaidRegBillPayCC();
    }
  }else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
  }
}



kony.boj.onClickYesBackAddPrePaidBene = function(){
   popupCommonAlertDimiss();
  
   frmPrePaidPayeeDetailsKA.lblPaymentMode.text = geti18Value("i18n.billsPay.Accounts");
   frmPrePaidPayeeDetailsKA.lblNext.skin = "sknLblNextDisabled";
   frmPrePaidPayeeDetailsKA.lblNext.setEnabled(false); 
   if(loadBillerDetails === true){
		if(kony.store.getItem("langPrefObj") === "en")
          kony.boj.lang = "eng";
        else
          kony.boj.lang = "ara";
        gblTModule = "billspay";
        getAllAccounts();
   }else{
	  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var controller = INSTANCE.getFormController("frmManagePayeeKA");
      var navObject = new kony.sdk.mvvm.NavigationObject();
      navObject.setRequestOptions("managepayeesegment",{"headers":{},"queryParams":{"custId": custid,
                                                                                    "P_BENE_TYPE": "PRE",
                                                                                    "lang":(kony.store.getItem("langPrefObj")==="en")?"eng":"ara"}});
      controller.loadDataAndShowForm(navObject);
     
   }
   kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); 
  
 
};

         
kony.boj.onClickYesBackFromAddPrePaidBene = function(){
    popupCommonAlertDimiss();
  
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    show_BILLER_LIST("all");
    frmManagePayeeKA.show();
    
	frmAddNewPrePayeeKA.destroy();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); 
};




function nextFromNewPrepaidBene()
{
  if( frmAddNewPrePayeeKA.lblBillerCategory.text != geti18Value("i18n.billsPay.BillerCategory") && frmAddNewPrePayeeKA.lblBillerName.text != geti18Value("i18n.billsPay.billerName") &&frmAddNewPrePayeeKA.lblServiceType.text!= geti18Value("i18n.billsPay.ServiceType"))
  {
    var w,x,y,z,h;
    if(frmAddNewPrePayeeKA.flxBillerNumber.isVisible === true){
      if(frmAddNewPrePayeeKA.tbxBillerNumber.text === "")
        x=0;
      else
        x=1;
    }
    else{
     // customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
      x=1;
    }

    if(frmAddNewPrePayeeKA.flxDenomination.isVisible === true){
      if(frmAddNewPrePayeeKA.lblDenomination.text === geti18Value("i18n.prepaid.denomination"))
        y=0;
      else
        y=1;
    }
    else{
      y=1;
    }
    
    if(frmAddNewPrePayeeKA.tbxNickName.isVisible === true){
      if(frmAddNewPrePayeeKA.tbxNickName.text === "")
        h=0;
      else
        h=1;
    }
    else{
      h=1;
    }

    if(frmAddNewPrePayeeKA.flxAmount.isVisible === true){
      if(frmAddNewPrePayeeKA.tbxAmount.text === "")
        z=0;
      else
        z=1;
    }
    else{
      z=1;
    }


        w=1;
    	if(frmAddNewPrePayeeKA.flxAmount.isVisible === true){
          if(frmAddNewPrePayeeKA.tbxAmount.text !== "." && parseFloat(frmAddNewPrePayeeKA.tbxAmount.text) > 0){
            w=1;
          }
            else w=0;
          }

	
    if(w&x&y&z&h){ 
      frmAddNewPrePayeeKA.lblNext.skin = "sknLblNextEnabled";
      frmAddNewPrePayeeKA.lblNext.setEnabled(true); 
    }
    else{
      frmAddNewPrePayeeKA.lblNext.skin = "sknLblNextDisabled";
      frmAddNewPrePayeeKA.lblNext.setEnabled(false);
    }

  }
  else{
    frmAddNewPrePayeeKA.lblNext.skin = "sknLblNextDisabled";
    frmAddNewPrePayeeKA.lblNext.setEnabled(false);
  }
}





kony.boj.bojCheckBox = function(eventobject,context){


  var segData = [], isChanged = false;
  segData.push(frmManagePayeeKA.managepayeesegment.selectedRowItems[0]);
  
  if (segData[0].btnSetting.text === "p"){
  	/***segData[0].initial = segData[0].NickName.substring(0, 2).toUpperCase();
    segData[0].lblTick = "";****/
    segData[0].icon.backgroundColor = kony.boj.getBackGroundColour(segData[0].initial);
    segData[0].btnSetting.text = "q";
    segData[0].lblBulkSelection.isVisible = false;
    for(var i in kony.boj.AllPayeeList){
    	if(segData[0].category === "postpaid"){
			if(kony.boj.AllPayeeList[i].BillerCode === segData[0].BillerCode &&
              kony.boj.AllPayeeList[i].ServiceType === segData[0].ServiceType &&
              kony.boj.AllPayeeList[i].BillingNumber === segData[0].BillingNumber){
            	kony.boj.AllPayeeList[i].btnSetting.text = "q";
                handle_SELECTION_BULK_BILL(i);//context.rowIndex
                break;
            }
    	}else{
       		if(kony.boj.AllPayeeList[i].p_billercode === segData[0].p_billercode &&
              kony.boj.AllPayeeList[i].p_servicetype === segData[0].p_servicetype &&
              kony.boj.AllPayeeList[i].p_denocode === segData[0].p_denocode &&
              kony.boj.AllPayeeList[i].BillingNumber === segData[0].BillingNumber){
            	kony.boj.AllPayeeList[i].btnSetting.text = "q";
                handle_SELECTION_BULK_BILL(i);//context.rowIndex
                break;
            }
        }
//     	if(kony.boj.AllPayeeList[i].BillingNumber === segData[0].BillingNumber){
//         	kony.boj.AllPayeeList[i].btnSetting.text = "q";
//         	handle_SELECTION_BULK_BILL(i);//context.rowIndex
//         	break;
//         }
    }
    bill_BULK_PAYMENT_COUNT = parseInt(bill_BULK_PAYMENT_COUNT)-1;
    isChanged = true;
  }else if(parseInt(bill_BULK_PAYMENT_COUNT) < 7){
    /****segData[0].initial = "";
    segData[0].lblTick = "r";****/
    //segData[0].icon.backgroundColor = "10BF00";
    segData[0].btnSetting.text = "p";
    segData[0].lblBulkSelection.isVisible = true;
    for(var i in kony.boj.AllPayeeList){
    	if(segData[0].category === "postpaid"){
			if(kony.boj.AllPayeeList[i].BillerCode === segData[0].BillerCode &&
              kony.boj.AllPayeeList[i].ServiceType === segData[0].ServiceType &&
              kony.boj.AllPayeeList[i].BillingNumber === segData[0].BillingNumber){
            	kony.boj.AllPayeeList[i].btnSetting.text = "p";
                selectedIndex_BULK_PAYMENT.push(i);//context.rowIndex
                break;
            }
    	}else{
       		if(kony.boj.AllPayeeList[i].p_billercode === segData[0].p_billercode &&
              kony.boj.AllPayeeList[i].p_servicetype === segData[0].p_servicetype &&
              kony.boj.AllPayeeList[i].p_denocode === segData[0].p_denocode &&
              kony.boj.AllPayeeList[i].BillingNumber === segData[0].BillingNumber){
            	kony.boj.AllPayeeList[i].btnSetting.text = "p";
                selectedIndex_BULK_PAYMENT.push(i);//context.rowIndex
                break;
            }
        }
//     	if(kony.boj.AllPayeeList[i].BillingNumber === segData[0].BillingNumber){
//         	kony.boj.AllPayeeList[i].btnSetting.text = "p";
//         	selectedIndex_BULK_PAYMENT.push(i);//context.rowIndex
//         	break;
//         }
    }
    bill_BULK_PAYMENT_COUNT = parseInt(bill_BULK_PAYMENT_COUNT)+1;
    isChanged = true;
  }
  if(isChanged){
    validate_BULK_BILL_SELECTION();
    frmManagePayeeKA.managepayeesegment.setDataAt(segData[0], context.rowIndex);
    flip_ANIMATION_SEGMENT_WIDGET(eventobject,context,frmManagePayeeKA.managepayeesegment,"flxIcon1");
  }else if(parseInt(bill_BULK_PAYMENT_COUNT) == 7){
  	customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.bulkpayment.selectionLimitValidationMsg"), popupCommonAlertDimiss, null, "", "");
  }
};


function process_POSTPAID_PREPAID_BILLER_LIST(data){
	try{
      
      kony.print("Hassan prePaidData ::"+JSON.stringify(prePaidData));
   		var temp = [];
    	if(data.BillerList !== undefined && data.BillerList.length > 0){
        	for(var i in data.BillerList){
        		data.BillerList[i].category = "postpaid";
            	temp.push(data.BillerList[i]);
            }
        }
    	/*if(data.beneList !== undefined && data.beneList.length > 0){
        	for(var i in data.beneList){
        		data.beneList[i].category = "prepaid";
            	data.beneList[i].NickName = data.beneList[i].nickName;
            	data.beneList[i].BillingNumber = data.beneList[i].benAcctNo;
            	temp.push(data.beneList[i]);
            }
        }*/ // vadivel
      
        if(prePaidData !== undefined && prePaidData.length > 0){
              for(var i in prePaidData){
                  prePaidData[i].category = "prepaid";
                  prePaidData[i].NickName = prePaidData[i].p_nickname;
                  prePaidData[i].BillingNumber = prePaidData[i].p_billingno;
                  kony.print("temp data11111 ::"+JSON.stringify(prePaidData[i]));
                  temp.push(prePaidData[i]);
              }
          }
    	return temp;
    }catch(e){
    	kony.print("Exceptin_process_POSTPAID_PREPAID_BILLER_LIST ::"+e);
    }
}

function show_BILLER_LIST(flow){
	try{
    	bill_BULK_PAYMENT_COUNT = 0;
    	var temp = [];
    	if(flow === "all"){
        	//temp = kony.boj.AllPayeeList;
          for(var j in kony.boj.AllPayeeList){
            kony.boj.AllPayeeList[j].lblBulkSelection.isVisible = false;
            kony.boj.AllPayeeList[j].btnSetting.text = "q";
            if(kony.boj.AllPayeeList[j].category === "prepaid"){
              //    kony.boj.AllPayeeList[j].btnEditBillerDetails ={"isVisible": false}; // hassan edit prepaid
              //      kony.boj.AllPayeeList[j].verticalDivider ={"isVisible": false};  // hassan edit prepaid
              kony.boj.AllPayeeList[j].btnEditBillerDetails ={"isVisible": true};   // hassan edit prepaid
              kony.boj.AllPayeeList[j].verticalDivider ={"isVisible": true};// hassan edit prepaid
                  }else{
              kony.boj.AllPayeeList[j].btnEditBillerDetails ={"isVisible": true};  
              kony.boj.AllPayeeList[j].verticalDivider ={"isVisible": true};
                  }
            temp.push(kony.boj.AllPayeeList[j]);
          }
          
        }else{
        	for(var i in kony.boj.AllPayeeList){
            	kony.boj.AllPayeeList[i].btnSetting.text = "q";
            	if(kony.boj.AllPayeeList[i].category === flow){
                	kony.boj.AllPayeeList[i].initial = kony.boj.AllPayeeList[i].NickName.substring(0,2).toUpperCase();
        			kony.boj.AllPayeeList[i].lblTick = "";
                  	kony.boj.AllPayeeList[i].lblBulkSelection.isVisible = false;
                  if(kony.boj.AllPayeeList[i].category === "prepaid"){
                    //kony.boj.AllPayeeList[i].btnEditBillerDetails ={"isVisible": false}; // hassan edit prepaid
                    //kony.boj.AllPayeeList[i].verticalDivider ={"isVisible": false}; // hassan edit prepaid
                     kony.boj.AllPayeeList[i].btnEditBillerDetails ={"isVisible": true};// hassan edit prepaid
                     kony.boj.AllPayeeList[i].verticalDivider ={"isVisible": true};// hassan edit prepaid
                  }else{
                   kony.boj.AllPayeeList[i].btnEditBillerDetails ={"isVisible": true};
                    kony.boj.AllPayeeList[i].verticalDivider ={"isVisible": true};
                  }
                	temp.push(kony.boj.AllPayeeList[i]);
                }
            }
        }
    	frmManagePayeeKA.managepayeesegment.removeAll();
    	frmManagePayeeKA.managepayeesegment.widgetDataMap = {
                                payeename: "NickName",
                                accountnumber :"BillingNumber",    
                                lblBillerType: "BillerCode",
                                lblInitial: "initial",
                                flxIcon1: "icon",
        						lblTick:"lblTick",
                                btnEditBillerDetails:"btnEditBillerDetails",
          						verticalDivider : "verticalDivider",
          						lblBulkSelection:"lblBulkSelection"
                                };
    	frmManagePayeeKA.managepayeesegment.setData(temp);
        if(temp.length === 0){
    frmManagePayeeKA.managepayeesegment.isVisible = false;
    frmManagePayeeKA.lblSearchResult.isVisible = true;
  }
  else{
    frmManagePayeeKA.managepayeesegment.isVisible = true;
    frmManagePayeeKA.lblSearchResult.isVisible = false;
  }
    }catch(e){
    	kony.print("Exception_show_BILLER_LIST ::"+e);
    }
}

function navigate_PREPAID_PAYEE_DETAILS_CONFIRM(){
	try{
    	frmPrePaidPayeeDetailsKA.flxDenominationConf.setVisibility(false);
    	frmPrePaidPayeeDetailsKA.flxAmountConf.setVisibility(false);
    	frmPrePaidPayeeDetailsKA.CopyflxIcon0a41947647db445.backgroundColor = kony.boj.getBackGroundColour(frmPrePaidPayeeDetailsKA.txtBillerName.text.substring(0,2));
    	frmPrePaidPayeeDetailsKA.CopylblInitial0df20680ba88e45.text = frmPrePaidPayeeDetailsKA.txtBillerName.text.substring(0,2).toUpperCase();
    	frmPrePaidPayeeDetailsKA.lblBillerNamePopup.text = frmPrePaidPayeeDetailsKA.txtBillerName.text;
    	frmPrePaidPayeeDetailsKA.lblBillerNumPopup.text = frmPrePaidPayeeDetailsKA.txtBillingNumber.text;
    	frmPrePaidPayeeDetailsKA.lblConfirmBillerCategory.text = frmPrePaidPayeeDetailsKA.txtBillerName.text;
    	frmPrePaidPayeeDetailsKA.lblConfirmServiceType.text = frmPrePaidPayeeDetailsKA.txtServiceType.text;
    	frmPrePaidPayeeDetailsKA.lblConfirmNickName.text = kony.boj.selectedPayee.NickName;
    	if(frmPrePaidPayeeDetailsKA.flxDenomination.isVisible === true){
        	frmPrePaidPayeeDetailsKA.lblAmountConf.text = "";
        	frmPrePaidPayeeDetailsKA.lblDenominationConf.text = frmPrePaidPayeeDetailsKA.txtDenomination.text;
        	frmPrePaidPayeeDetailsKA.flxDenominationConf.setVisibility(true);
        }else{
        	frmPrePaidPayeeDetailsKA.lblDenominationConf.text = "";
        	frmPrePaidPayeeDetailsKA.lblAmountConf.text = frmPrePaidPayeeDetailsKA.txtAmount.text;
        	frmPrePaidPayeeDetailsKA.flxAmountConf.setVisibility(true);
        }
    	frmPrePaidPayeeDetailsKA.flxMain.setVisibility(false);
		frmPrePaidPayeeDetailsKA.flxConfirmPopUp.setVisibility(true);
    }catch(e){
    	kony.print("Exception_navigate_PREPAID_PAYEE_DETAILS_CONFIRM ::"+e);
    }
}