kony = kony || {};
kony.rb = kony.rb || {};

/**
 * BusinessController
 * frmNUOSelectAProductKA
 */

kony.rb.selectProductsBusinessController = function(mfObjSvc) {
    this.mfObjectServiceHandler = mfObjSvc;
};

/**
 * function getProductsList
 * get the List of Products
 */

kony.rb.selectProductsBusinessController.prototype.getProductsList = function(presentationSuccessCallback, presentationErrorCallback) {

    this.mfObjectServiceHandler.customPost("getAllProducts", "NewUserProducts", {}, {}, success, error);
    var utlHandler = applicationManager.getUtilityHandler();

    function success(res) {

        var productArray = res.NewUserProducts;

        for (var i = 0; i < productArray.length; i++) {

            productArray[i] = utlHandler.convertJSonToProductsObject(productArray[i]);

        }

        presentationSuccessCallback(productArray);
    }

    function error(err) {
        presentationErrorCallback(error);
    }
};

/**
 * function getProductsList
 * get the List of User Products
 */

kony.rb.selectProductsBusinessController.prototype.getUserProductsList = function(presentationSuccessCallback1, presentationErrorCallback1) {

    this.mfObjectServiceHandler.fetch("NewUserProducts", {}, {}, success, error);
    var utlHandler = applicationManager.getUtilityHandler();

    function success(res) {

        var productArray1 = res.records;

        for (var i = 0; i < productArray1.length; i++) {

            productArray1[i] = utlHandler.convertJSonToProductsObject(productArray1[i]);

        }

        presentationSuccessCallback1(productArray1);
    }

    function error(err) {
        presentationErrorCallback1(error);
    }
};

/**
 * function createSelectedProductsList
 * create the User Products
 */


kony.rb.selectProductsBusinessController.prototype.createSelectedProductsList = function(params,presentationSuccessCallback, presentationErrorCallback)
{
  this.mfObjectServiceHandler.create("NewUserProducts", params, {}, success, error);
    function success(res){
      presentationSuccessCallback(res);
    }
    function error(err)
    {
      presentationErrorCallback(err);
    }
};