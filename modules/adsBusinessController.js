kony = kony || {};
kony.rb = kony.rb || {};
 
kony.rb.adsBusinessController = function(mfObjSvc) {
    this.mfObjectServiceHandler = mfObjSvc;
    this.inFeedAds = null;
    this.nonInFeedAds = null;
};

kony.rb.adsBusinessController.prototype.fetchAndLoadAds = function(successCallback, errorCallback) {
 
    this.mfObjectServiceHandler.fetch("Ads", {}, {}, success, error);
 
    var utlHandler = applicationManager.getUtilityHandler();
 
    function success(res) {
        var adsArray = res.records;
        for (var i = 0; i < adsArray.length; i++) {
            adsArray[i] = utlHandler.convertJSonToAdsObject(adsArray[i]);
        }
         var ads = getAdsByType(adsArray);
               this.nonInFeedAds = ads[0];
               this.inFeedAds = ads[1];
     
        successCallback();
    }
 
    function error(err) {
        errorCallback(err);
    }
  
   function getAdsByType(data)
   {
    	var adsArray = data;
   		var final = [];
    	var infeed = [];
    	var nonInfeed = [];
    	final.push(nonInfeed);
    	final.push(infeed);
 
    	for (var ad in adsArray) {
       		if (adsArray[ad].adType === "infeed") {
            	infeed.push(adsArray[ad]);
        	} else {
            	nonInfeed.push(adsArray[ad]);
        	}
    	}
    	return final;
	}
 
};
 
kony.rb.adsBusinessController.prototype.fetchAdsByType = function(type, presentationSuccessCallback, presentationFailureCallback) {
 
    if (this.inFeedAds === null || this.nonInFeedAds === null) {
        var successCallback = function() {
            if (type == "infeed") {
                presentationSuccessCallback(this.inFeedAds);
            } else if (type == "nonInfeed") {
                presentationSuccessCallback(this.nonInFeedAds);
            }
        };
 
        var failureCallback = function(err) {
            presentationFailureCallback(err);
            kony.print("Failure in fetching the Ads");
        };
 
        this.fetchAndLoadAds(successCallback, failureCallback);
    }
    else {
        if (type == "infeed") {
            presentationSuccessCallback(this.inFeedAds);
        } else if (type == "NonInfeed") {
            presentationSuccessCallback(this.nonInFeedAds);
       }
    }
};
 
