function getDeviceRegStatus(){
  ShowLoadingScreen();
  var scopeObj = this;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options ={    "access": "online",
                "objectName": "RBObjects"
               };
  var headers = {"session_token":kony.retailBanking.globalData.session_token};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("DeviceRegistration",serviceName,options);
  var devID = deviceid;
  var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
  var serviceOptions = {"dataObject":dataObject,
                        "headers":headers,"queryParams":{"deviceId":devID}};
  modelObj.fetch(serviceOptions, deviceSuccess, customErrorCallback);
}

function deviceSuccess(response)
{
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  var deviceRegstatus = response&&response[0]&&response[0].status;
  var adsPC ;
  if (deviceRegstatus == "false"){  
    updateFlags("deviceRegisterFlag",false);
    if(isSettingsFlagEnabled("rememberMeFlag"))
      frmDeviceRegistrationOptionsKA.show();
    else
    {
      if(defaultPage == "frmAccountsLandingKA")
      {
        adsPC = applicationManager.getAdsPresentationController();
        adsPC.ShowPostLoginAdvertisementform(); 
      }
      else
      {
        showFormOrderList();
      }    
    }
  }
  else
  {
    updateFlags("deviceRegisterFlag",true);
    if(isSettingsFlagEnabled("rememberMeFlag"))
    {
      if(isFirstTimeLogin())
      {
        frmDeviceregistrarionSuccessKA.lblDevregistraionSuccess.text=i18n_firstLoginDevRegSucc;
        frmDeviceregistrarionSuccessKA.lblLoginMethod.text=i18n_firstLoginMethod;
        frmDeviceregistrarionSuccessKA.lblRetainlogin.setVisibility(true);
        frmDeviceregistrarionSuccessKA.show();
      }
      else
      {
        if(defaultPage == "frmAccountsLandingKA")
        {
          adsPC = applicationManager.getAdsPresentationController();
          adsPC.ShowPostLoginAdvertisementform(); 
        }
        else
        {
          showFormOrderList();
        }       
      }
    }
    else
    {
      if(defaultPage == "frmAccountsLandingKA")
      {
        adsPC = applicationManager.getAdsPresentationController();
        adsPC.ShowPostLoginAdvertisementform(); 
      }
      else
      {
        showFormOrderList();
      }       
    }
  }  
}


function updateDeviceReg(devId){
  ShowLoadingScreen();
  var scopeObj = this;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options ={    "access": "online",
                "objectName": "RBObjects"
               };
  var headers = {"session_token":kony.retailBanking.globalData.session_token};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("DeviceRegistration",serviceName,options);
  var record = {};
  record["deviceId"] = devId;
  var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration",record);
  var requestOptions = {"dataObject":dataObject, "headers":headers};
  modelObj.create(requestOptions, updatedevRegSuccess, customErrorCallback);
}
function updatedevRegSuccess(res){
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  frmDeviceregistrarionSuccessKA.lblDevregistraionSuccess.text=i18n_LoginDevRegSucc;
  frmDeviceregistrarionSuccessKA.lblLoginMethod.text=i18n_LoginMethod;
  frmDeviceregistrarionSuccessKA.lblRetainlogin.setVisibility(false);
  frmDeviceregistrarionSuccessKA.show();
  updateFlags("deviceRegisterFlag",true);
  kony.print(res);
}

function deviceRegsitrationSuccessPreshow()
{
  if (!(kony.retailBanking.globalData.deviceInfo.isTouchIDSupported())) 
  { 
    frmDeviceregistrarionSuccessKA.EnableTouchIdCheckBox.isVisible=false;
    updateFlags("touchIDEnabledFlag",false);
    isTouchPermitted=0;
  }
  if(kony.retailBanking.globalData.deviceInfo.isIphone())
  {
    frmDeviceregistrarionSuccessKA.enablePinSwitch.selectedIndex=0;
    frmDeviceregistrarionSuccessKA.touchIdSwitch.selectedIndex=0;
  }
  else
  {
    frmDeviceregistrarionSuccessKA.imgEnableTouchId.src="checkbox_selected.png";
    frmDeviceregistrarionSuccessKA.EnablePinImg.src="checkbox_selected.png";
  }


}
