//Array maintains User preference of enabling Authentication modes - Touch Id, Pin, Facial Recognition #Manisha
authModeEnablingStatus ={"touchIdPref":0,
                         "pinLoginPref":0,
                         "facialAuthPref":0
                        };
var authImgArray=[];
function isSettingsFlagEnabled(whichFlag)
{
  var tempSettings=kony.store.getItem("settingsflagsObject");
  if (tempSettings[whichFlag] === true)
    return true;
  else
    return false;
}

function isFirstTimeLogin()
{
  if(kony.store.getItem("firstTimeLogin")=== null || kony.store.getItem("firstTimeLogin") === "")
    return true;
  else
    return false;
}

function gotoLoginFeatures()
{     
  if(kony.retailBanking.globalData.deviceInfo.isIphone())
  {
    if (frmDeviceregistrarionSuccessKA.touchIdSwitch.selectedIndex == 1 || !(kony.retailBanking.globalData.deviceInfo.isTouchIDSupported()))
    {
      authModeEnablingStatus["touchIdPref"]=0;
      updateFlags("touchIDEnabledFlag",false);
    }
    else
    {
      authModeEnablingStatus["touchIdPref"]=1;
      authImgArray.push("touchiconactive.png");
    }
    if (frmDeviceregistrarionSuccessKA.enablePinSwitch.selectedIndex == 0)
    {
      authModeEnablingStatus["pinLoginPref"]=1;
      authImgArray.push("pindeactive.png");
    }
    else
      authModeEnablingStatus["pinLoginPref"]=0;
    if (frmDeviceregistrarionSuccessKA.enableFaceIdSwitch.selectedIndex == 0)
    {
      authModeEnablingStatus["facialAuthPref"]=1;
      authImgArray.push("facedeactive.png");
    }
    else
    {
      authModeEnablingStatus["facialAuthPref"]=0;
      updateFlags("isFacialAuthEnabledFlag",false);
    }
  }
  else
  {
    if(frmDeviceregistrarionSuccessKA.imgEnableTouchId.src == "checkbox_unselected.png" || !(kony.retailBanking.globalData.deviceInfo.isTouchIDSupported()))
    {
      authModeEnablingStatus["touchIdPref"]=0;
      updateFlags("touchIDEnabledFlag",false);
    }
    else
    {
      authModeEnablingStatus["touchIdPref"]=1;
      authImgArray.push("touchiconactive.png");
    }

    if(frmDeviceregistrarionSuccessKA.EnablePinImg.src == "checkbox_selected.png")
    {
      authModeEnablingStatus["pinLoginPref"]=1;
      authImgArray.push("pindeactive.png");
    }
    else
      authModeEnablingStatus["pinLoginPref"]=0;
    if(frmDeviceregistrarionSuccessKA.EnableFaceIdImg.src == "checkbox_selected.png")
    {
      authModeEnablingStatus["facialAuthPref"]=1;
      authImgArray.push("facedeactive.png");
    }
    else
    {
      authModeEnablingStatus["facialAuthPref"]=0;
      updateFlags("isFacialAuthEnabledFlag",false);
    }
  }

 
    kony.print("remoed")();

}
function setAuthModeHeader(frmName)
{
  if (authImgArray.length == 3)
  {
    frmName.imgAuthMode1KA.setVisibility(true);
    frmName.imgAuthMode2KA.setVisibility(true);
    frmName.imgAuthMode3KA.setVisibility(true);
    frmName.flxLine1KA.setVisibility(true);
    frmName.flxLine2KA.setVisibility(true);

    frmName.imgAuthMode1KA.src = authImgArray[0];
    frmName.imgAuthMode2KA.src = authImgArray[1];
    frmName.imgAuthMode3KA.src = authImgArray[2];

    frmName.imgAuthMode1KA.left="0%";
    frmName.flxLine1KA.width="25%";
    frmName.flxLine1KA.left="-3dp";
    frmName.flxLine2KA.width="23%";
  }
  else if(authImgArray.length == 2)
  {
    frmName.imgAuthMode1KA.setVisibility(true);
    frmName.imgAuthMode2KA.setVisibility(true);
    frmName.imgAuthMode3KA.setVisibility(false);
    frmName.flxLine1KA.setVisibility(true);
    frmName.flxLine2KA.setVisibility(false);  
    frmName.imgAuthMode1KA.src = authImgArray[0];
    frmName.imgAuthMode2KA.src = authImgArray[1];

    frmName.imgAuthMode1KA.left="7%";
    frmName.flxLine1KA.width="61%";
    frmName.flxLine1KA.left="-6dp";
  }
  else if(authImgArray.length == 1)
  {
    frmName.imgAuthMode1KA.setVisibility(true);
    frmName.imgAuthMode2KA.setVisibility(false);
    frmName.imgAuthMode3KA.setVisibility(false);
    frmName.flxLine1KA.setVisibility(false);
    frmName.flxLine2KA.setVisibility(false);

    frmName.imgAuthMode1KA.src = authImgArray[0];

    frmName.imgAuthMode1KA.left="42%";
  }

}

function authImgreplace(actualImage,replaceImage)
{
  for( var i=0;i<(authImgArray.length);i++)
  {
    if (authImgArray[i] == actualImage)
      authImgArray[i] = replaceImage;
  }
}

function  navigationOnSetPinMode()
{
  if (kony.retailBanking.globalData.globals.userObj.isPinSet == "true")
  { 
   authImgreplace("pintickwhite.png","pintickoutline.png");
  }
  else
  {
    authImgreplace("pinactive.png","pindeactive.png");
  }
    authImgreplace("facedeactive.png","facebig.png");
    if (authModeEnablingStatus["facialAuthPref"])
    {
    setAuthModeHeader(frmFacialAuthEnable);
    frmFacialAuthEnable.show();
    }
    else
    frmLoginAuthSuccessKA.show();
}
  


function calculateWidth(numWidgets, widgetWidth, marginWidth){
  var width = 0;
  width = (numWidgets*widgetWidth) + (numWidgets-1)*marginWidth;
  return width;
}

/*function showAuthModesflex()
{
  //frmLoginKA.flxLoginFeatures.setVisibility(true);
  var authModesEnabled=0;
  var authModeFeaturesEnabled = [];
  if (isSettingsFlagEnabled("touchIDEnabledFlag"))
  {
    authModesEnabled=authModesEnabled+1;
    authModeFeaturesEnabled.push("touchIdContainer");
  }
  if (isSettingsFlagEnabled("isFacialAuthEnabledFlag"))
  {
    authModesEnabled=authModesEnabled+1;
    authModeFeaturesEnabled.push("flxFacialAuthFeature");
  }
  if (isSettingsFlagEnabled("isPinEnabledFlag"))
  {
    authModesEnabled=authModesEnabled+1;
    authModeFeaturesEnabled.push("flxPinLoginFeature");
  }
  if(authModesEnabled == 1)
    showSingleAuthFeature(authModeFeaturesEnabled);
  else if (authModesEnabled == 2)
    showDualAuthFeature(authModeFeaturesEnabled);
  else if (authModesEnabled == 3)
    showAllAuthModes();
  else
    //frmLoginKA.flxLoginFeatures.setVisibility(false);

}*/

function showAllAuthModes(){
  var width = calculateWidth(3, 70, 20);
  //frmLoginKA.flxLoginFeatures.width = 210 + "dp";
  //frmLoginKA.flxFacialAuthFeature.setVisibility(true);
  //frmLoginKA.flxPinLoginFeature.setVisibility(true);
  //frmLoginKA.touchIdContainer.setVisibility(true);

}

function showSingleAuthFeature(authModeFeaturesEnabled){
  var width = calculateWidth(1, 70, 20);
  //frmLoginKA.flxLoginFeatures.width = 70 + "dp";
  resetFlxLoginFeatures();
  frmLoginKA[authModeFeaturesEnabled[0]].setVisibility(true);
}

function showDualAuthFeature(authModeFeaturesEnabled){
  var width = calculateWidth(2, 70, 20);
  //frmLoginKA.flxLoginFeatures.width = 140 + "dp";
  resetFlxLoginFeatures();
  frmLoginKA[authModeFeaturesEnabled[0]].setVisibility(true);
  frmLoginKA[authModeFeaturesEnabled[1]].setVisibility(true);
}

function resetFlxLoginFeatures()
{
  //frmLoginKA.flxFacialAuthFeature.setVisibility(false);
  //frmLoginKA.touchIdContainer.setVisibility(false);
  //frmLoginKA.flxPinLoginFeature.setVisibility(false);
}

var defaultLoginMtds=[
  {"imgIcon":"password.png","lblLoginMethod":"PASSWORD","imgEnable":""}
];


function defaultLoginPreshow(frmName)
{
  var defaultLoginMtds=[
    {"imgIcon":"password.png","lblLoginMethod":"PASSWORD","imgEnable":'',"tmpAuthMode":"password"}
  ];
  if (isSettingsFlagEnabled("isFacialAuthEnabledFlag"))
    defaultLoginMtds.push({"imgIcon":"face.png","lblLoginMethod":"FACE ID","imgEnable":'',"tmpAuthMode":"faceid"})
    if (isSettingsFlagEnabled("isPinEnabledFlag"))
      defaultLoginMtds.push({"imgIcon":"pin.png","lblLoginMethod":"PIN","imgEnable":'',"tmpAuthMode":"pin"});
  if (isSettingsFlagEnabled("touchIDEnabledFlag"))
    defaultLoginMtds.push({"imgIcon":"touch.png","lblLoginMethod":"TOUCH ID","imgEnable":'',"tmpAuthMode":"touchid"})
    frmName.segLoginMethods.setData(defaultLoginMtds);
}

function onSelectDefaultLogin(frmname)
{
  var authData =  frmname.segLoginMethods.data;
  var selectedauthMode = frmname.segLoginMethods.selectedRowIndex[1];

  for (i = 0; i < authData.length; i++)
  {
    if(selectedauthMode == i)
      authData[i].imgEnable='check_green.png';
    else
      authData[i].imgEnable='';
  }
  frmname.segLoginMethods.setData(authData);
}


function setDefaultLoginMethod(frmname)
{
  var authData =  frmname.segLoginMethods.data;
  for (i = 0; i < authData.length; i++)
  {
    if(authData[i].imgEnable == 'check_green.png')
      updateFlags("defaultLoginMode",authData[i].tmpAuthMode);
  }
  if(frmname.id === "frmLoginAuthSuccessKA")
  {
    if(defaultPage == "frmAccountsLandingKA")
    {
      var adsPC = applicationManager.getAdsPresentationController();
      adsPC.ShowPostLoginAdvertisementform(); 
    }
    else
    {
      showFormOrderList();
    }
  }
}