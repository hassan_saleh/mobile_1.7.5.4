//Type your code here


function navigatetoformOrderChequeBook(){
  kony.print("preshowOrderChequeBook");
   assignCallbacksforfrmOrderandStatements();
  frmOrderCheckBook.lblFre.text = geti18Value("i18n.settings.account");
  frmOrderCheckBook.lblFrequency.setVisibility(false);
  frmOrderCheckBook.lblLine4.skin = "sknFlxGreyLine";

  frmOrderCheckBook.lblBranch.text = geti18Value("i18n.locateus.branch");
  frmOrderCheckBook.lblCurrHead.setVisibility(false);
  frmOrderCheckBook.CopylblLine0ib1721fa6b7140.skin = "sknFlxGreyLine";

  frmOrderCheckBook.lblLeaves.text = geti18Value("i18n.ordercheck.numberofLeaves");
  frmOrderCheckBook.CopylblFrequency0i1313ad52a414f.setVisibility(false);
  frmOrderCheckBook.CopylblLine0fb169aada68946.skin = "sknFlxGreyLine";

  frmOrderCheckBook.lblBook.text = geti18Value("i18n.ordercheck.numberofbook");
  frmOrderCheckBook.lblBookHeader.setVisibility(false);
  frmOrderCheckBook.CopylblLine0a150866e36e549.skin = "sknFlxGreyLine";

  frmOrderCheckBook.lblNext.skin = "sknLblNextDisabled";
//   frmOrderCheckBook.show();
  set_ACCOUNT_ORDERCHECKBOOK();
}



function setDatatoCheckOrderBranchList(selectedItem){
  frmOrderCheckBook.lblBranch.text = selectedItem.BRANCH_NAME;
  frmOrderChequeBookConfirm.lblBranchCode.text = selectedItem.BRANCH_CODE;
  frmOrderCheckBook.CopylblLine0ib1721fa6b7140.skin =  "sknFlxGreenLine";
  frmOrderCheckBook.lblCurrHead.setVisibility(true);
  nextSkinCheckForOrderCheckBook();
  frmOrderCheckBook.show();
}


function nextSkinCheckForOrderCheckBook(){
  if(frmOrderCheckBook.lblFre.text!= geti18Value("i18n.settings.account") && frmOrderCheckBook.lblFre.text!== "" && frmOrderCheckBook.lblBranch.text !== geti18Value("i18n.locateus.branch") && frmOrderCheckBook.lblLeaves.text !== geti18Value("i18n.ordercheck.numberofLeaves")){// && frmOrderCheckBook.lblBook.text !== geti18Value("i18n.ordercheck.numberofbook")
    frmOrderCheckBook.lblNext.skin = "sknLblNextEnabled";
  }else{
    frmOrderCheckBook.lblNext.skin = "sknLblNextDisabled";
  }
}


function nextToConfirmofOrderCheckBook(){
  if(frmOrderCheckBook.lblNext.skin === "sknLblNextEnabled"){
    frmOrderChequeBookConfirm.lblAccount.text = frmOrderCheckBook.lblFre.text;
    frmOrderChequeBookConfirm.lblBranch.text = frmOrderCheckBook.lblBranch.text;
    frmOrderChequeBookConfirm.lblLeaves.text = frmOrderCheckBook.lblLeaves.text;
    frmOrderChequeBookConfirm.lblBook.text = "1";//frmOrderCheckBook.lblBook.text;
    frmOrderChequeBookConfirm.show();
  }
}




function callConfirmOrderCheckBook(){
  kony.print("callConfirmOrderCheckBook::");
  if(kony.sdk.isNetworkAvailable()){
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };


    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Accounts"); 
    dataObject.addField("custId",custid);
    var p_acc_type = frmOrderCheckBook.lblNumber1.text;
    /*if(p_acc_type == geti18Value("i18n.common.savingsAccount")){
      p_acc_type = "305";
    }else{
      p_acc_type = "301";
    }*/

    var leaves = "1";
    if(frmOrderChequeBookConfirm.lblLeaves.text == "10"){
      leaves = "1";
    }else if(frmOrderChequeBookConfirm.lblLeaves.text == "20"){
      leaves = "2";
    }else if(frmOrderChequeBookConfirm.lblLeaves.text == "40"){
      leaves = "4";
    }

    dataObject.addField("p_chq_leave_no",leaves);
    dataObject.addField("p_chq_book_qty","1");
    //dataObject.addField("p_chq_book_qty",frmOrderChequeBookConfirm.lblBook.text);
    dataObject.addField("acctno",frmOrderCheckBook.lblNumber1.text);

    dataObject.addField("p_acc_br",frmOrderChequeBookConfirm.lblBranchCode.text);
    dataObject.addField("p_delv_br",frmOrderChequeBookConfirm.lblBranchCode.text);
    var serviceOptions = {"dataObject":dataObject,"headers":headers};

    kony.print("callConfirmOrderCheckBookInput params-->"+ JSON.stringify(serviceOptions) );
    modelObj.customVerb("cvChqBookOrder",serviceOptions, callConfirmOrderCheckBookSuccessCallback, callConfirmOrderCheckBookErrorCallback);
  }else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}

function callConfirmOrderCheckBookSuccessCallback(response){
  kony.application.dismissLoadingScreen();
  gblsubaccBranchList=false;
  gblTModule = "";
  if(!isEmpty(response) && response.opstatus === 0 && response.ErrorCode === "00000")
  {
    var reference = "";
    if(!isEmpty(response.referenceId))
      reference = geti18nkey("i18n.common.ReferenceNumber")+" " + response.referenceId;

    kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),reference,geti18Value("i18n.Bene.GotoAccDash"),geti18nkey("i18n.common.AccountDashboard"), "", "","","",geti18Value("i18n.ordercheque.successDesc"));


  }else{

    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
    exceptionLogCall("callConfirmOrderCheckBookSuccessCallback","Service ERROR","Service","opstatus other than 0");
    kony.application.dismissLoadingScreen();
  }
}


function callConfirmOrderCheckBookErrorCallback(response){
  gblsubaccBranchList=false;
  gblTModule = "";
  kony.print("callConfirmOrderCheckBookErrorCallback -----"+JSON.stringify(response));
  customAlertPopup(geti18Value("i18n.NUO.Error"),  geti18Value("i18n.common.defaultErrorMsg"),popupCommonAlertDimiss, "");
  kony.application.dismissLoadingScreen();

}


function onClickAccSegToOrderCheckBookScreen(){
  var AccDetailsSegdata = frmAccountDetailsScreen.segFre.data;
  var dataSelected =  frmAccountDetailsScreen.segFre.selectedRowItems[0];
 
  var text;
 
  kony.print("Selected Account Data ::"+JSON.stringify(dataSelected));
  kony.print("gblTModule ::"+gblTModule);
 var accName =(dataSelected.anum.length>16)?(dataSelected.anum.substring(0,16)+"..."):dataSelected.anum;
  frmOrderCheckBook.lblFre.text = accName+ " " +dataSelected.hiddenAccountNumber;
  
   frmOrderCheckBook.lblNumber1.text = dataSelected.accountNumber;
     frmOrderCheckBook.lblFrequency.setVisibility(true);
  frmOrderCheckBook.show();
}





function onClickAccBackToOrderCheckBookScreen(){
 
  frmOrderCheckBook.show();
   gblTModule = "";
}

function show_ACCOUNTS_ORDERCHECKBOOK(){
	try{
    	gblsubacclist="AccountOrder";
//         var fromAccounts = [];
//     	for(var i in kony.retailBanking.globalData.accountsDashboardData.accountsData){
//         	if(kony.retailBanking.globalData.accountsDashboardData.accountsData[i].accountType === "C"){
//             	fromAccounts.push(kony.retailBanking.globalData.accountsDashboardData.accountsData[i]);
//             }
//         }
        gblTModule="AccountOrder";
    	getAllAccounts();
//         frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.my_money.accounts");
//         accountsScreenPreshow(fromAccounts);
//         frmAccountDetailsScreen.show();
    }catch(e){
    	kony.print("Exception_show_ACCOUNTS_ORDERCHECKBOOK ::"+e);
    }
}

function set_ACCOUNT_ORDERCHECKBOOK(){
	try{
    	var dataSelected =  kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)];
        var text;
        kony.print("Selected Account Data ::"+JSON.stringify(dataSelected));
        kony.print("gblTModule ::"+gblTModule);
      	var accName =(dataSelected.accountName.length>16)?(dataSelected.accountName.substring(0,16)+"..."):dataSelected.accountName;
        frmOrderCheckBook.lblFre.text = accName+ " " +dataSelected.accountID;
        frmOrderCheckBook.lblNumber1.text = dataSelected.accountNumber;
        frmOrderCheckBook.lblFrequency.setVisibility(true);
        frmOrderCheckBook.show();
    }catch(e){
    	kony.print("Exception_set_ACCOUNT_ORDERCHECKBOOK ::"+e);
    }
}

function set_ACCOUNTS_REQUESTSTATEMENT(){
	try{
        var dataSelected =  kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)];
        var text;
        kony.print("Selected Account Data ::"+JSON.stringify(dataSelected));
        kony.print("gblTModule ::"+gblTModule);
        var accName =(dataSelected.accountName.length>16)?(dataSelected.accountName.substring(0,16)+"..."):dataSelected.accountName;
        frmRequestStatementAccounts.lblNumber.text = accName+ " " +dataSelected.accountID;
        frmRequestStatementAccounts.lblNumber1.text = dataSelected.accountID;
        frmRequestStatementAccounts.lblAccountNumber.setVisibility(true);
        frmRequestStatementAccounts.show();
    }catch(e){
    	kony.print("Exception_set_ACCOUNTS_REQUESTSTATEMENT ::"+e);
    }
}