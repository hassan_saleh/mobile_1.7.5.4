var frmPayeeDetailsKAConfig = {
    "formid": "frmPayeeDetailsKA",
    "frmPayeeDetailsKA": {
        "entity": "Payee",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        },
    },
//     "lblLanguage": {
//         "fieldprops": {
//             "entity": "Payee",
//             "widgettype": "Label",
//             "field": "p_id_lang"
//         }
//     },
    "lblOperation": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "Label",
            "field": "p_txn_type"
        }
    },
  "lblDummy": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "Label",
            "field": "payeeId"
        }
    },
    "lblBillerCode": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "Label",
            "field": "p_BillerCode"
        }
    },
    "lblName": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "Label",
            "field": "p_ServiceType"
        }
    },
    "lblAccountNumber": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "Label",
            "field": "p_BillingNo"
        }
    },
//     "lblIDType": {
//         "fieldprops": {
//             "entity": "Payee",
//             "widgettype": "Label",
//             "field": "p_IdType"
//         }
//     },
//     "lblID": {
//         "fieldprops": {
//             "entity": "Payee",
//             "widgettype": "Label",
//             "field": "p_ID"
//         }
//     },
//     "lblNationality": {
//         "fieldprops": {
//             "entity": "Payee",
//             "widgettype": "Label",
//             "field": "p_Nation"
//         }
//     },
//     "lblPhoneNo": {
//         "fieldprops": {
//             "entity": "Payee",
//             "widgettype": "Label",
//             "field": "p_Phone"
//         }
//     },
//     "lblAddress": {
//         "fieldprops": {
//             "entity": "Payee",
//             "widgettype": "Label",
//             "field": "p_Address1"
//         }
//     }
};