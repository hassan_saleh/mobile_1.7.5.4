function setDatatoReasonsSegment(data, parameter)
{
  kony.print("@@inside setDatatoReasonsSegment:::"+JSON.stringify(data)+"parameter :: "+parameter);
  frmReasonsListScreen.segList.widgetDataMap = {lblfrequency : parameter};
  frmReasonsListScreen.segList.setData(data);
  frmReasonsListScreen.show();
}


function onClickCancelCardReasons(data){
  gblListFlow = "CancelCardReasons";
  frmReasonsListScreen.lblTitle.text = geti18nkey("i18n.cards.reasonhead1");
  var data = [
    { 
      "Reason" : "Lost",
      "code": "L",
      "Language" : "English"
    },
    {
      "Reason" : "Fraud",
      "code": "RN3",
      "Language" : "English"
    },
    {
      "Reason" : "Stolen",
      "code": "S",
      "Language" : "English"
    }
  ];
  setDatatoReasonsSegment(data, "Reason");
}


function onClickDeliveryBranch(){
  gblListFlow = "DeliveryBranch";
  frmReasonsListScreen.lblTitle.text = geti18nkey("i18n.cards.reasonhead3");
  var data = [
    { 
      "Branch" : "Amman",
      "Language" : "English"
    },
    {
      "Branch" : "Aquaba",
      "Language" : "English"
    },
    {
      "Branch" : "Jerash",
      "Language" : "English"
    },
    {
      "Branch" : "Madaba",
      "Language" : "English"
    },
    {
      "Branch" : "Zarqa",
      "Language" : "English"
    }
  ];
  setDatatoReasonsSegment(data, "Branch");
}


function onClickDeactivateReason(){
  gblListFlow = "DeactivateReason";
  frmReasonsListScreen.lblTitle.text = geti18nkey("i18n.cards.reasonhead2");
  var data = [
    { 
      "Reason" : "Lost",
      "code": "L",
      "Language" : "English"
    },
    {
      "Reason" : "Fraud",
      "code": "RN3",
      "Language" : "English"
    },
    {
      "Reason" : "Stolen",
      "code": "S",
      "Language" : "English"
    }
  ];
  setDatatoReasonsSegment(data, "Reason");
}


function onClickReasonsegment(){
  flow = gblListFlow;
  kony.print("@@@ inside onClickReasonsegment :::"+flow+" segdata:::"+JSON.stringify(frmReasonsListScreen.segList.selectedRowItems[0]));
  switch(flow){
    case "CancelCardReasons" :
      frmCancelCardKA.lblReason.text = frmReasonsListScreen.segList.selectedRowItems[0].Reason;
      frmCancelCardKA.lblReasonCode.text = frmReasonsListScreen.segList.selectedRowItems[0].code;
      frmCancelCardKA.show();
      break;
    case "DeliveryBranch" :
      frmCancelCardKA.lblDeliveryBranch.text = frmReasonsListScreen.segList.selectedRowItems[0].lblfrequency;
      frmCancelCardKA.show();
      break;
    case "DeactivateReason" :
      frmStopCardKA.lblReason.text = frmReasonsListScreen.segList.selectedRowItems[0].lblfrequency;
      frmStopCardKA.show();
      break;
    default :
      alert("Please set global gblListFlow");
  }
}


function backFromReasonsList()
{
  flow = gblListFlow;
  switch(flow){
    case "CancelCardReasons" :
      frmCancelCardKA.show();
      break;
    case "DeliveryBranch" :
      frmCancelCardKA.show();
      break;
    case "DeactivateReason" :
      frmStopCardKA.show();
      break;
    }
}

