kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.navigationManager =  function(){
    this.stack =[];
    this.navTable = new kony.rb.navigationTable();
};

kony.rb.navigationManager.prototype.navigateTo = function(formName){
    //kony.application.showLoadingScreen();
    this.stack.push(formName);
    var formObj = eval(formName);
    formObj.show();
     
};

kony.rb.navigationManager.prototype.mvvmNavigateTo = function(formName,navObject){
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
		var formController = INSTANCE.getFormController(formName);
		if(!navObject || !(navObject instanceof kony.sdk.mvvm.NavigationObject)) {
        	navObject = new kony.sdk.mvvm.NavigationObject();
        }
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(kony.retailBanking.globalData.globals.LOADING_SCREEN_MESSAGE);
        kony.retailBanking.globalData.globals.formStack.push(formName);
		formController.loadDataAndShowForm(navObject);
};

kony.rb.navigationManager.prototype.mvvmGoBack = function(doreload){
       var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
	   var stackLength= kony.retailBanking.globalData.globals.formStack.length;
	   var currentform = kony.retailBanking.globalData.globals.formStack[stackLength-1];
       var controller = INSTANCE.getFormController(currentform);
	  // kony.retailBanking.globalData.globals.formStack.pop();  
	   if(stackLength!==0)
				{
					var form = kony.retailBanking.globalData.globals.formStack[stackLength-1];
					this.showPreviousForm(doreload,form);
				}
};

kony.rb.navigationManager.prototype.showPreviousForm = function(doReload,formName){
    	var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    	var navigateToForm = formName ? formName : kony.application.getPreviousForm().id; // to show particular form
		var prevController = INSTANCE.getFormController(navigateToForm);
		if(doReload){
			kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(kony.retailBanking.globalData.globals.LOADING_SCREEN_MESSAGE);
			prevController.loadDataAndShowForm(prevController.getContextData());
		}else{
		    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(kony.retailBanking.globalData.globals.LOADING_SCREEN_MESSAGE);
			prevController.getFormModel().showView();
			kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
		}
 };

kony.rb.navigationManager.prototype.goBack = function(navDetails){
        var stackLength= this.stack.length;
	    var currentform = this.stack[stackLength-1];
	    this.stack.pop();
		stackLength= this.stack.length;
		if(stackLength!==0)
		{
		  var formId = this.stack[stackLength-1];
          var formObj = eval(formId);
          var existingNavDetails = this.getCustomInfo(formId);
          if(navDetails)
             existingNavDetails.backNavDetails = navDetails;
          this.setCustomInfo(formId,existingNavDetails);
		  formObj.show();
		}
};

kony.rb.navigationManager.prototype.setCustomInfo = function(key, value){
  	   this.navTable.setContextItem(key, value);
};

kony.rb.navigationManager.prototype.getCustomInfo=function(key)
{
      return  this.navTable.getContextItem(key);
};

kony.rb.navigationManager.prototype.clearStack = function(){
      this.stack = [];
};

