
function navigateStopCard(){
  kony.print("@@ Inside navigateStopCard");
  try{
    popupStopCard.show();
  }catch(exceptionObj){
    kony.print("Exception found in navigateStopCard:: "+JSON.stringify(exceptionObj));
  }
}


function closeStopCardPopup(){
  kony.print("@@ Inside closeStopCard");
  try{
    popupCommonAlertDimiss();
  }catch(exceptionObj){
    kony.print("Exception found in closeStopCard:: "+JSON.stringify(exceptionObj));
  }
}


function showCancelCardPopup(){
  kony.print("@@ Inside showCancelCardPopup");
  try{
    PopupCancelAndReplaceCard.show();
  }catch(exceptionObj){
    kony.print("Exception found in showCancelCardPopup:: "+JSON.stringify(exceptionObj));
  }
}

function selectCancelOrReplaceCard(){
  kony.print("@@ Inside selectCancelOrReplaceCard :: ");
  var selRdKey = "";
  try{
    selRdKey = PopupCancelAndReplaceCard.RdBtnCancelCard.selectedKey;
    kony.print("selected Key ::"+selRdKey);
    if(PopupCancelAndReplaceCard != null && PopupCancelAndReplaceCard!= undefined){
      if(PopupCancelAndReplaceCard.RdBtnCancelCard != null && PopupCancelAndReplaceCard.RdBtnCancelCard != undefined){
        if(PopupCancelAndReplaceCard.RdBtnCancelCard.selecteKey == "rbg1"){
          PopupCancelAndReplaceCard.HBxCardDeliveryBranch.isVisible = false;//setVisibility(false);
        }else if(PopupCancelAndReplaceCard.RdBtnCancelCard.selecteKey == "rbg2"){
          PopupCancelAndReplaceCard.HBxCardDeliveryBranch.isVisible = true;//setVisibility(true);
        }
      }
    }
  }catch(exceptionObj){
    kony.print("Exception found in selectCancelOrReplaceCard:: "+JSON.stringify(exceptionObj));
  }
}


function dismissCancelCardPopup(){
  kony.print("@@ Inside dismissCancelCardPopup");
  try{
    PopupCancelAndReplaceCard.dismiss();
  }catch(exceptionObj){
    kony.print("Exception found in dismissCancelCardPopup:: "+JSON.stringify(exceptionObj));
  }
}

function showSettings(){
  kony.print("Inside showSettings...");
  try{
    frmSettingsKA.show();
  }catch(exceptionObj){
    kony.print("Exception found in showSettings :: "+JSON.stringify(exceptionObj));
  }
}
function expandMenuItems(){
    kony.print("Inside expandMenuItems...");
  try{
    
  }catch(exceptionObj){
    kony.print("Exception found in expandMenuItems :: "+JSON.stringify(exceptionObj));
  }
}

function toggleCancelCardSwitch(){
  kony.print("Inside toggleCancelCardSwitch ::");
  try{
    kony.print("inside selected option :: "+PopupCancelAndReplaceCard.RdBtnCancelCard.selectedKey);
    if(PopupCancelAndReplaceCard.RdBtnCancelCard.selectedKey == "rbg1"){
      kony.print("inside first rdbtn option :: "+PopupCancelAndReplaceCard.RdBtnCancelCard.selectedKey);
      kony.print("PopupCancelAndReplaceCard.lstbxCardCancelReason.selectedKey:: "+PopupCancelAndReplaceCard.lstbxCardCancelReason.selectedKey);
      if(PopupCancelAndReplaceCard.lstbxCardCancelReason.selectedKey !== "lb1"){
        frmManageCardsKA["flxcancelSwitchOff"].setVisibility(false);
        frmManageCardsKA["flxcancelSwitchOn"].setVisibility(true);
      }else{
        frmManageCardsKA["flxcancelSwitchOff"].setVisibility(true);
        frmManageCardsKA["flxcancelSwitchOn"].setVisibility(false);
      }
    }/*else if(PopupCancelAndReplaceCard.RdBtnCancelCard.selectedKey == "rbg2"){
      kony.print("inside second rdbtn option :: "+PopupCancelAndReplaceCard.RdBtnCancelCard.selectedKey);
      kony.print("PopupCancelAndReplaceCard.lstbxCardDeliveryBranch:: "+PopupCancelAndReplaceCard.lstbxCardDeliveryBranch);
      if(PopupCancelAndReplaceCard.lstbxCardCancelReason.selectedKey != "lb1" && PopupCancelAndReplaceCard.lstbxCardDeliveryBranch.selectedKey != "lb1"){
        frmManageCardsKA["flxcancelSwitchOff"].setVisibility(false);
        frmManageCardsKA["flxcancelSwitchOn"].setVisibility(true);
      }else{
        frmManageCardsKA["flxcancelSwitchOff"].setVisibility(true);
        frmManageCardsKA["flxcancelSwitchOn"].setVisibility(false);
      }
    }*/
    PopupCancelAndReplaceCard.dismiss();
  }catch(exceptionObj){
    PopupCancelAndReplaceCard.dismiss();
    kony.print("Exception found in toggleCancelCardSwitch :: "+JSON.stringify(exceptionObj));
  }

}

function stopCardSubmit(){
  kony.print("Inside stopCardSubmit...");
  try{
    if(popupStopCard.lstbxCardStoplReason.selectedKey != "lb1"){
     frmManageCardsKA["flxStopCardSwitchOn"].setVisibility(true);
      frmManageCardsKA["flxStopCardSwitchOff"].setVisibility(false);
    }else{
      frmManageCardsKA["flxStopCardSwitchOn"].setVisibility(false);
      frmManageCardsKA["flxStopCardSwitchOff"].setVisibility(true);
    }
    popupStopCard.dismiss();
  }catch(exceptionObject){
    kony.print("Exception found in stopCardSubmit::"+JSON.stringify(exceptionObject));
	exceptionLogCall("stopCardSubmit","UI ERROR","UI",exceptionObject);
    popupStopCard.dismiss();
  }
}

function showPopupStopCard(){
   kony.print("Inside showPopupStopCard...");
  try{
    changePopLabel();
  }catch(exceptionObject){
    kony.print("Exception found in showPopupStopCard::"+JSON.stringify(exceptionObject));
  }
}


function showDeliveryCardReason(){
  kony.print("Inside showDeliveryCardReason...");
  try{
    if(PopupCancelAndReplaceCard.RdBtnCancelCard.selectedKey == "rbg1"){
      PopupCancelAndReplaceCard.HBxCardDeliveryBranch.setVisibility(false);
    }else if(PopupCancelAndReplaceCard.RdBtnCancelCard.selectedKey == "rbg2"){
      PopupCancelAndReplaceCard.HBxCardDeliveryBranch.setVisibility(true);
    }
  }catch(exceptionObject){
    kony.print("Exception found in showDeliveryCardReason::"+JSON.stringify(exceptionObject));
  }
}

function changePopLabel(){
kony.print("@@@inside changePopLabel");
  var isActive = "";
  var ActivateOrDeactivate = "";
  var headerI18n = "";
  var bodyI18n = "";

  try{
    isActive = frmManageCardsKA["flxStopCardSwitchOn"].isVisible;
    kony.print("@@changePopLabel::: isActive--"+isActive);
    if(isActive){
      kony.print("@@changePopLabel::: if--"+isActive);
      ActivateOrDeactivate = geti18nkey("i18n.stopCard.deActivate");
      CARDLIST_DETAILS.operationType = CARD_COSNTANTS.DEACTIVATE;
      headerI18n = "i18n.cards.DeactivateCard";
      bodyI18n = "i18n.stopCard.deActivate";
    }else{
      kony.print("@@changePopLabel::: else--"+isActive);
      ActivateOrDeactivate = geti18nkey("i18n.stopCard.Activate"); 
      CARDLIST_DETAILS.operationType = CARD_COSNTANTS.ACTIVATE;
      headerI18n = "i18n.cards.ActivateCard";
      bodyI18n = "i18n.stopCard.Activate";
    }
    customAlertPopup(kony.i18n.getLocalizedString(headerI18n), kony.i18n.getLocalizedString(bodyI18n),invokeStopCardService, closeStopCardPopup, kony.i18n.getLocalizedString("i18n.common.YES"),kony.i18n.getLocalizedString("i18n.common.NO"));

  }catch(exceptionObj){
    kony.print("Exception found in changePopupLabel ::: "+JSON.stringify(exceptionObj));
  }
}

function invokeStopCardService(){
  var cardType = "", isWearable = null;
  try{
    cardType = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].cardTypeFlag;
    isWearable = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].isWearablePrepaid;
    kony.print("@@ Inside invokeStopCardService... "+CARDLIST_DETAILS.cardNumber+", "+CARDLIST_DETAILS.operationType);
    if(cardType == CARD_COSNTANTS.DEBITCARD || (cardType == CARD_COSNTANTS.WEBCHARGE && isWearable === false)){
    	kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    	onOffSwitch();
    }else if(cardType == CARD_COSNTANTS.CREDITCARD || isWearable === true){
    	stopCardsFunction(CARDLIST_DETAILS.cardNumber, CARDLIST_DETAILS.operationType); 
    }
  }catch(exceptionObject){
    kony.print("Exception found in invokeStopCardService::: "+JSON.stringify(exceptionObject));
  }
}

function hideCancelNReplaceDetails(){
  kony.print("@@@ Inside hideCancelNReplaceDetails...");
  try{
    frmCancelCardKA.btnCardReplace.setVisibility(false);
    frmCancelCardKA.lblOtherCard.setVisibility(false);
    frmCancelCardKA.flxDeliveryBranch.setVisibility(false);
  }catch(exceptionObject){
    kony.print("Exception found in hideCancelNReplaceDetails :::"+JSON.stringify(exceptionObject));
  }
}

function loadStopCardReasons(){
  kony.print("@@@ Inside loadStopCardReason...");
  var cardType = "";
  try{
    cardType = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].cardTypeFlag;
    kony.print("cardType found ::: "+cardType+"user selected language:: "+kony.store.getItem("langPrefObj"));
    hideCancelNReplaceDetails();
    if(cardType == CARD_COSNTANTS.DEBITCARD || cardType == CARD_COSNTANTS.WEBCHARGE){
      if(kony.store.getItem("langPrefObj") == GENERIC_COSNTATNTS.AR){
        fillCardReasonssegment(DEBITCARD_REASON_ARABIC);//fillStopCardDropdown(DEBITCARD_REASON_ARABIC);
      }else if(kony.store.getItem("langPrefObj") == GENERIC_COSNTATNTS.EN){
        fillCardReasonssegment(DEBITCARD_REASON_ENGLISH);//fillStopCardDropdown(DEBITCARD_REASON_ENGLISH);
      }
    }else if(cardType == CARD_COSNTANTS.CREDITCARD){
      if(kony.store.getItem("langPrefObj") == GENERIC_COSNTATNTS.AR){
        fillCardReasonssegment(CREDITCARD_REASON_ARABIC);//fillStopCardDropdown(CREDITCARD_REASON_ARABIC);
      }else if(kony.store.getItem("langPrefObj") == GENERIC_COSNTATNTS.EN){
        fillCardReasonssegment(CREDITCARD_REASON_ENGLISH);//fillStopCardDropdown(CREDITCARD_REASON_ENGLISH);
      }
    }
  }catch(exceptionObj){
    kony.print("Exception found in loadStopCardReason ::: "+JSON.stringify(exceptionObj));
  }
}

function fillStopCardDropdown(sourceArray){
  kony.print("@@@ Inside fillStopCardDropdown... sourceArray:::"+JSON.stringify(sourceArray));
  var tempData = [];
  frmCancelCardKA.lstCardReason.masterData = [];
  try{
    for (var key in sourceArray) {
      if (sourceArray.hasOwnProperty(key)) {
        var val = sourceArray[key];
        var temp = [key,val];
        tempData.push(temp);
      }
    }
    kony.print("List Data: "+JSON.stringify(tempData));
    frmCancelCardKA.lstCardReason.masterData = tempData;
  }catch(exceptionObj){
    kony.print("Exception found in fillStopCardDropdown ::: "+JSON.stringify(exceptionObj));
  }
}
function onOffSwitch(){
  kony.print("@@Inside onOffSwitch...");
  try{
  	var CARD_STATUS = "";
    if(CARDLIST_DETAILS.operationType == CARD_COSNTANTS.ACTIVATE){
//       frmManageCardsKA["flxStopCardSwitchOff"].setVisibility(false);
//       frmManageCardsKA["flxStopCardSwitchOn"].setVisibility(true);
      CARD_STATUS = "A";
    }else if(CARDLIST_DETAILS.operationType == CARD_COSNTANTS.DEACTIVATE){
//       frmManageCardsKA["flxStopCardSwitchOff"].setVisibility(true);
//       frmManageCardsKA["flxStopCardSwitchOn"].setVisibility(false);
	  CARD_STATUS = "D";
    }
    serv_DEBITCARD_STATUS(CARD_STATUS);
    popupCommonAlertDimiss();
	
//     frmManageCardsKA.show();

  }catch(exceptionObject){
    kony.print("##Exception found in onOffSwitch ::: "+JSON.stringify(exceptionObject));
  }
}

/*function stopCardPreshow(){
  kony.print("@@Inside stopCardPreshow...");
  try{
    frmCancelCardKA.lblReason.text = "Reason For Card Cancellation";
  }catch(exceptionObject){
    kony.print("Exception found in stopCardPreshow :::"+JSON.stringify(exceptionObject));
  }
}*/

function fillCardReasonssegment(sourceArray){
  kony.print("@@@ Inside fillCardReasonssegment..." + JSON.stringify(sourceArray));
  var tempData = [];
  try{
    for (var key in sourceArray) {
      if (sourceArray.hasOwnProperty(key)) {
        var val = sourceArray[key];
        var temp = {"Reason":val, "code": key};
        tempData.push(temp);
      }
    }
    gblListFlow = "CancelCardReasons";
    frmReasonsListScreen.lblTitle.text = geti18nkey("i18n.cards.reasonhead1");
    setDatatoReasonsSegment(tempData, "Reason");
  }catch(exceptionObject){
    kony.print("Exception found in fillCardReasonssegment ::: "+JSON.stringify(exceptionObject));
  }
}

function showCancelCard(){
  kony.print("@@@ inside showCancelCard...");
  try{
    frmCancelCardKA.lblReason.text = geti18nkey("i18n.cards.reasonforcancel");
    frmCancelCardKA.show();
  }catch(exceptionObject){
    kony.print("Exception found in showCancelCard:::"+JSON.stringify(exceptionObject));
  }
}


function serv_DEBITCARD_STATUS(status){
	try{
      	var tempData = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
    	var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
          "access": "online",
          "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Cards", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Cards");
        dataObject.addField("custId",custid);
        dataObject.addField("CardNum", tempData.card_num);
        dataObject.addField("Reason", "FromMobApp");
        dataObject.addField("action", status);
    	dataObject.addField("p_channel", "MOBILE");
    	dataObject.addField("p_user_id", "BOJMOB");
        var serviceOptions = {
          "dataObject": dataObject,
          "headers": headers
        };
        kony.print("Data Objects ::"+JSON.stringify(dataObject));
        if (kony.sdk.isNetworkAvailable()) {
          kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
          modelObj.customVerb("DCActivation", serviceOptions, serv_DEBITCARD_ACTIVATEORDEACTIVATE_SUCCESS, function(err){kony.print("Failed ::"+JSON.stringify(err));
                                                                                                                          if(err.message !== ""){
                                                                                                                              customAlertPopup(geti18Value("i18n.Bene.Failed"),message,popupCommonAlertDimiss,"");
                                                                                                                          }
                                                                                                                        });
        }
    }catch(e){
    	kony.print("Exception_serv_DEBITCARD_ACTIVATEORDEACTIVATE ::"+e);
    }
}

function serv_DEBITCARD_ACTIVATEORDEACTIVATE_SUCCESS(response){
	try{
    	var message = "";
    	if(response !== null && response !== ""){
        	if(response.referenceId !== "" && response.referenceId !== null){
            	if(CARDLIST_DETAILS.operationType == CARD_COSNTANTS.ACTIVATE){
                  frmManageCardsKA["flxStopCardSwitchOff"].setVisibility(false);
                  frmManageCardsKA["flxStopCardSwitchOn"].setVisibility(true);
                  frmManageCardsKA.lblAccountNumber.text = geti18Value("i18n.card.active");
                  kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_status = "2";
                }else if(CARDLIST_DETAILS.operationType == CARD_COSNTANTS.DEACTIVATE){
                  frmManageCardsKA["flxStopCardSwitchOff"].setVisibility(true);
                  frmManageCardsKA["flxStopCardSwitchOn"].setVisibility(false);
                  frmManageCardsKA.lblAccountNumber.text = geti18Value("i18n.card.notactive");
                  kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_status = "3";
                  customAlertPopup(geti18Value("i18n.common.success"), geti18nkey("i18n.cards.deactiveDBsuccess"),popupCommonAlertDimiss,"");// hassan message when deactivate card
                }
            	return;
            }else if(response.ErrorCode !== "" && response.ErrorCode !== null){
            	message = getErrorMessage(response.ErrorCode);
            }
        }
    	if(message !== ""){
        	if(message === undefined || message === "undefined"){
            	message = geti18Value("i18n.common.somethingwentwrong");
            }
        	customAlertPopup(geti18Value("i18n.Bene.Failed"),message,popupCommonAlertDimiss,"");
        }
    }catch(e){
    	kony.print("Exception_serv_DEBITCARD_ACTIVATEORDEACTIVATE_SUCCESS ::"+e);
    }
}