////////////////////////////
// Account Detail Form
////////////////////////////

function accountDetailPreShow(){
  userAgent = kony.os.userAgent();
  // Alterations to Android layout due to lack of scrolling events
  if (userAgent !== "iPhone"){
    frmAccountDetailKA.accountBalanceOverview.top = "70dp";
  
  }
}

// Called on yourAccount[x] buttons on accountLanding form
// Passes sample data and color information to frmAccountDetailKA form and displays it



function hideAccountDetails(){
    gblfrmName = "Account Overview";
  	var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmAccountsLandingKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
  	navObject.setRequestOptions("segAccountsKA",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
    controller.loadDataAndShowForm(navObject);
}

// Called on frmAccountDetailKA.accountDetailsScrollContainer onScrolling action
function accountDetailsScroll(){
  if (userAgent === "iPhone"){
  	var outerScrollY = frmAccountDetailKA.accountDetailsScrollContainer.contentOffsetMeasured.y;
  	var outerScrollYAbs=Math.abs(outerScrollY);
  
  	// Display accountLabelScroll element
  	if (outerScrollY > 115){
    	frmAccountDetailKA.accountLabelScroll.isVisible = true;
      	frmAccountDetailKA.resourcesLabel.isVisible=false;
    } else if (outerScrollY > 0 && outerScrollY < 115) {
          frmAccountDetailKA.accountLabelScroll.isVisible = false;
      frmAccountDetailKA.resourcesLabel.isVisible=true;
    }

    // If user scrolls upwards
    if (outerScrollY > 0 && outerScrollY < 80){
      frmAccountDetailKA.accountBalanceOverview.top = (95-(outerScrollYAbs*0.3125) + "dp");
      frmAccountDetailKA.accountInfoButton.opacity = (1-(outerScrollYAbs*0.006));
      frmAccountDetailKA.accountInfoButton.bottom = (10+(outerScrollYAbs*0.05) + "%");
    }
  	
  	if (outerScrollY > 80) {
      frmAccountDetailKA.accountBalanceOverview.top = "70dp";
    }
  
  	if (outerScrollY === 0){
      frmAccountDetailKA.accountInfoButton.bottom = "10%";
      frmAccountDetailKA.accountBalanceOverview.opacity = 1;
      frmAccountDetailKA.accountInfoButton.opacity = 1;
    }

    // if user scrolls downwards  
    if (outerScrollY < 0){
      	frmAccountDetailKA.accountDetailsHeader.top = (15 + (outerScrollYAbs*0.3) + "dp");
      	frmAccountDetailKA.accountDetailsTime.top = (37 + (outerScrollYAbs*0.3) + "dp");
      	frmAccountDetailKA.accountBalanceOverview.top = (95+(outerScrollYAbs*0.4) + "dp");
      	frmAccountDetailKA.accountInfoButton.bottom = (10+(outerScrollYAbs*0.05) + "%");
      	frmAccountDetailKA.gradientOverlay.bottom = (0-(outerScrollYAbs*1) + "dp");
 	}
  }
}
function showMore()
{
  frmAccountDetailKA.lineDivider.height=(((Math.ceil(popupData.length/2))*116))+"dp";
  //frmAccountDetailKA.collapseButton.top=((Math.ceil(popupData.length/2))*100)+"dp";
  animateShow(successback);
  
  
}
function animateShow(successback)
{
 
 
  frmAccountDetailKA.flxSegmentWrapper.animate(kony.ui.createAnimation({
   
                "100": {
                                "top":"100%",
                                "opacity": 1,
                                "stepConfig": {
                                    "timingFunction": easeIn
                                }
                            }
            }), {
                "fillMode": forwards,
                "duration": 0.4,
                "iterationCount": 1,
                "delay": 0
            }, {
                "animationStart": function() {
                 
                },
                "animationEnd": function() {

                }
            }
        );
  if(successback)
    successback.call(this);
}
function successback()
{
 
  frmAccountDetailKA.titleBarAccountDetails.animate(kony.ui.createAnimation({
                "100": {
                                "height":"100%",
                                "opacity": 1,
                                "stepConfig": {
                                    "timingFunction": easeIn
                                }
                            }
            }), {
                "fillMode": forwards,
                "duration": 0.4,
                "iterationCount": 1,
                "delay": 0
            }, {
                "animationStart": function() {
                 
                },
                "animationEnd": function() {

                }
            }
        );
  frmAccountDetailKA.accountDetailsOverview.animate(kony.ui.createAnimation({
                "100": {
                                "height":"100%",
                                "opacity": 1,
                                "stepConfig": {
                                    "timingFunction": easeIn
                                }
                            }
            }), {
                "fillMode": forwards,
                "duration": 0.4,
                "iterationCount": 1,
                "delay": 0
            }, {
                "animationStart": function() {
                 
                },
                "animationEnd": function() {

                }
            }
        );
  
 
 frmAccountDetailKA.flxMore.setVisibility(true);
 frmAccountDetailKA.actionWrapper.setVisibility(false);
}
function collapseMore()
{
  animateCollapse();
}
function animateCollapse()
{
   frmAccountDetailKA.titleBarAccountDetails.animate(kony.ui.createAnimation({
                "100": {
                                "height":"45.5%",
                                "opacity": 1,
                                "stepConfig": {
                                    "timingFunction": easeIn
                                }
                            }
            }), {
                "fillMode": forwards,
                "duration": 0.1,
                "iterationCount": 1,
                "delay": 0
            }, {
                "animationStart": function() {
                 
                },
                "animationEnd": function() {

                }
            }
        );
  frmAccountDetailKA.accountDetailsOverview.animate(kony.ui.createAnimation({
                "100": {
                                "height":"100%",
                                "opacity": 1,
                                "stepConfig": {
                                    "timingFunction": easeIn
                                }
                            }
            }), {
                "fillMode": forwards,
                "duration": 0.1,
                "iterationCount": 1,
                "delay": 0
            }, {
                "animationStart": function() {
                 
                },
                "animationEnd": function() {

                }
            }
        );
 
  frmAccountDetailKA.flxMore.setVisibility(false);
  frmAccountDetailKA.actionWrapper.setVisibility(true);
  frmAccountDetailKA.flxSegmentWrapper.animate(kony.ui.createAnimation({
                "100": {
                                "top":"45.5%",
                                "opacity": 1,
                                "stepConfig": {
                                    "timingFunction": easeIn
                                }
                            }
            }), {
                "fillMode": forwards,
                "duration": 0.1,
                "iterationCount": 1,
                "delay": 0
            }, {
                "animationStart": function() {
                 
                },
                "animationEnd": function() {

                }
            }
        );
  
  
}
function animationReset()
{
  frmAccountDetailKA.titleBarAccountDetails.height=45.5+"%";
  frmAccountDetailKA.accountDetailsOverview.height=100+"%";  
  frmAccountDetailKA.flxSegmentWrapper.top=45.5+"%";
  frmAccountDetailKA.flxMore.setVisibility(false);
  frmAccountDetailKA.actionWrapper.setVisibility(true);
}
function fillOptionsInMore(selectedRecord)
{
 kony.retailBanking.globalData.globals.nav_Object = selectedRecord;
  for(var i=1;i<=8;i++)
     frmAccountDetailKA["flxMoreOption"+i].setVisibility(false);
  var accountType = selectedRecord["accountType"];
  var flagsupportsCashWithdrawal=selectedRecord["supportCardlessCash"];
  switch(accountType)
    {
      case "CreditCard" :
        popupData = [
         i18n_accountDetails,
         i18n_accountStatements,
         i18n_transferMoney,
         i18n_payABill,
         i18n_payAPerson,
        i18n_ManageCard
      ];
         popupDataIcon = [
          "accountdetail.png",
          "statementicon.png",
         "transfer_money_icon.png",
         "pay_bill_icon.png",
         "pay_a_person_icon.png",
         "managecards.png"
      ];
        break;
      case "Savings":
        popupData = [
         i18n_accountDetails,
           i18n_accountStatements,
         i18n_payABill,
         i18n_reorderCheck
      ];
         popupDataIcon = [
          "accountdetail.png",
           "statementicon.png",
          "pay_bill_icon.png",
         "checkreorder.png"
      ];
        break;
      case "Checking":
        popupData = [
          i18n_accountDetails,
           i18n_accountStatements,
         i18n_transferMoney,
         i18n_payAPerson,
         i18n_reorderCheck
      ];
         popupDataIcon = [
          "accountdetail.png",
           "statementicon.png",
         "transfer_money_icon.png",
        "pay_a_person_icon.png",
         "checkreorder.png"
      ]
        break;
        case "Deposit":
        popupData = [];
        popupDataIcon = [];
        break;
        default : acc = 0; 
    }
  if(flagsupportsCashWithdrawal==="1")
    {
      popupData.push(i18n_WithdrawCash);
      popupDataIcon.push("atm_icon_white.png");
    }
  
  for(var i=1;i<=popupData.length;i++)
    {
      frmAccountDetailKA["flxMoreOption"+i].setVisibility(true);
      frmAccountDetailKA["MoreLabel"+i].text=popupData[i-1];
      frmAccountDetailKA["MoreIcon"+i].src=popupDataIcon[i-1];
      frmAccountDetailKA["MoreHiddenKey"+i].text=popupData[i-1];
    }
  
}
