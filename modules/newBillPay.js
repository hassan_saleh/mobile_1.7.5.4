//Type your code here
var payeelength=0;
var BillPayfromForm="NewBillPay";
var globalCount=0;
var segmentData=[];
var billPayShowState="";
var billPayTxnObj={};

function billPaySearch(){
	var payeeNickName;
  	++globalCount;
  	if(globalCount===1){
		segmentData=frmBillPayeeDetailsKA.segPayeeNamesKA.data;    
	}
	var searchText=frmBillPayeeDetailsKA.searchTextField.text;
	var newSegmentData=[];
  	if(segmentData){
		for (var i=0;i<segmentData.length;i++){
      		payeeNickName=segmentData[i].payeeNickName;
      		var lowerCasepayeeNickName=payeeNickName.toLowerCase();
			if(lowerCasepayeeNickName.indexOf(searchText.toLowerCase())!==-1){
				newSegmentData.push(segmentData[i]);
			}
		}
    }
  	if(newSegmentData.length===0){
      frmBillPayeeDetailsKA.flexRecent.setVisibility(true);
    }
  	else{
      frmBillPayeeDetailsKA.flexRecent.setVisibility(false);
    }
	frmBillPayeeDetailsKA.segPayeeNamesKA.setData(newSegmentData);
}

function newBillPayPreShow(){
  var showState = "InitialLanding";
  var BillPayTransactionData=null;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmNewBillKA");
  var controllerContextData= controller.getContextData();
  if(controllerContextData && controllerContextData.getCustomInfo("showState")){
    showState = controllerContextData.getCustomInfo("showState");
  }
  if(controllerContextData && controllerContextData.getCustomInfo("BillPayTransactionData")){
    BillPayTransactionData = controllerContextData.getCustomInfo("BillPayTransactionData");
  }
  
  /* First set data for all the UI elements on the New Bill Pay form */
  setDataUIElementsOnNewBillPayForm(BillPayTransactionData, showState);
  
  var viewModel = controller.getFormModel();
  /* Old code. Sets the bank name retreived from global data */
  viewModel.setViewAttributeByProperty("fromAccountBankNameKA","text",kony.retailBanking.globalData.globals.BankName);
  viewModel.setViewAttributeByProperty("CopyLabel03e39ab4661a845","text",kony.retailBanking.globalData.globals.BankName);
  //viewModel.setViewAttributeByProperty("Label0cecf1132bf8049","text",kony.retailBanking.globalData.globals.BankName); 
  
  /* Calculates the height of all the containers */
  calculateNewBillPayCardHeights();
  
  /* Determine the state of all the containers */
  setFromContainerAttributesBillPay(showState);
  setToContainerAttributesBillPay(showState);

  /* Set the title bar */
  frmNewBillKA.transferPayTitleLabel.text="New Bill Pay";
  if (showState === "EditExistingBillPay")
     frmNewBillKA.transferPayTitleLabel.text=i18n_editPayBill; 
}

function frmBillPayeeDetailsKAPreShow(){
  frmBillPayeeDetailsKA.searchTextField.text="";
}

function frmBillPayeeDetailsKASegmentOnRowClick(){
  	kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(kony.retailBanking.globalData.globals.LOADING_SCREEN_MESSAGE);
	var newTransferTransactionData={};
	newTransferTransactionData.payeeNickName = frmBillPayeeDetailsKA.segPayeeNamesKA.selectedRowItems[0].payeeNickName; 
	newTransferTransactionData.payeeId = frmBillPayeeDetailsKA.segPayeeNamesKA.selectedRowItems[0].payeeId;
	navigateToNewBillPayForm("SpecifiedPayeeTo",newTransferTransactionData);
}

function frmBillPayeeDetailsKAaddPayeeOnClick(){
	var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
	var listController = INSTANCE.getFormController("frmAddNewPayeeKA");
    var navigationObject = new kony.sdk.mvvm.NavigationObject();
	var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
    navigationObject.setDataModel(null,kony.sdk.mvvm.OperationType.ADD, "form");
	navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
    listController.performAction("navigateTo",["frmAddNewPayeeKA",navigationObject]);
}

function frmBillPayeeDetailsKAonBackBtnClick(){
  	navigateToNewBillPayForm("back", null);	
}

function navigateToNewBillPayForm(showState, transactionObject){
   billPayShowState=showState;
	 billPayTxnObj=transactionObject;
  kony.application.showLoadingScreen(null,"", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    /* As we are not allowing editing of the To field only in EditExistingBillPay scenario */
    if(showState === "EditExistingBillPay")
      {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmNewBillKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setCustomInfo("showState",showState);
    if(showState !== "InitialLanding")
        navObject.setCustomInfo("BillPayTransactionData",transactionObject);
    navObject.setRequestOptions("segInternalFromAccountsKA",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
    listController.performAction("navigateTo",["frmNewBillKA",navObject]);
      }
  else{
        fetchPayeesData();
  }
    
}

function setDataUIElementsOnNewBillPayForm(BillPayTransactionData, showState){
   frmNewBillKA.lblCurrencyType.text=  kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount("",kony.retailBanking.globalData.globals.CurrencyCode);
   frmNewBillKA.lblTransactionType.text = kony.retailBanking.globalData.globals.PayBill; 
    /* A new transaction will be initiated. So initialize most of the data */  
    if(showState === "InitialLanding"|| showState === "SpecifiedPayee"){    
        var settingsObj = kony.store.getItem("settingsflagsObject");
		var currForm = kony.application.getCurrentForm().id;
      	var preferedSelAcnt;
      if(currForm == "frmAccountDetailKA"){
        var accNo = kony.retailBanking.globalData.globals.nav_Object["accountID"];
        preferedSelAcnt = kony.retailBanking.globalData.accounts.searchAccountById(accNo);
      }
      else
        preferedSelAcnt = kony.retailBanking.globalData.accounts.searchAccountById(settingsObj.DefaultPaymentAcctNo);
        if(preferedSelAcnt !==""){
            var nickNameData = preferedSelAcnt.nickName;
            //Need To Get This From Service
          	if(nickNameData !=="" && nickNameData !==undefined && nickNameData !==null){
            	if(nickNameData.trim() === "")
                	nickNameData = preferedSelAcnt.accountName;
            }
            setSelectedAccountData(frmNewBillKA, "from", nickNameData,
                    kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(preferedSelAcnt.availableBalance,kony.retailBanking.globalData.globals.CurrencyCode), 
                    preferedSelAcnt.accountID, 
                    getSkinColor(preferedSelAcnt.accountType), "INTERNAL");
        }
        //setPayeeData(frmNewBillKA, "form", null);
        frmNewBillKA.amountTextField.text="";
        frmNewBillKA.tolblAccountNumberKA.text="";
        setCalendarDateToCurrentDate(frmNewBillKA.calDateKA);        
        frmNewBillKA.lblTransactionIdKA.text = "";
      //Additionals for Specified payee
      if(showState=== "SpecifiedPayee")
      {
        frmNewBillKA.toNamePick.text =  BillPayTransactionData.payeeNickName; 
  		frmNewBillKA.tolblAccountNumberKA.text =  BillPayTransactionData.payeeId;
      }
      
    }
  	if(showState==="SpecifiedPayeeTo")
      {
        frmNewBillKA.toNamePick.text =  BillPayTransactionData.payeeNickName; 
  		frmNewBillKA.tolblAccountNumberKA.text =  BillPayTransactionData.payeeId;
      }

    /* An existing transaction is available, fill up data in the UI using the passed in navigation object */
    if (showState === "EditExistingBillPay" || showState === "EditNewBillPay" ||
        showState === "RepeatBillPay"){
        frmNewBillKA.amountTextField.text = BillPayTransactionData.transferAmount;   
       
        setSelectedAccountData(frmNewBillKA, "from", BillPayTransactionData.fromAccountName,
        BillPayTransactionData.fromAccountBalance, BillPayTransactionData.fromAccountNumber, 
        BillPayTransactionData.fromAccountColor, "INTERNAL");
       
        frmNewBillKA.calDateKA.dateComponents = BillPayTransactionData.transferDate;
        
      	frmNewBillKA.toNamePick.text =  BillPayTransactionData.payeeNickName; 
  		frmNewBillKA.tolblAccountNumberKA.text =  BillPayTransactionData.payeeId;
      
        frmNewBillKA.lblTransactionIdKA.text = "";
        if(showState === "EditExistingBillPay")
            frmNewBillKA.lblTransactionIdKA.text = BillPayTransactionData.transferTransactionID;

    }
  
  
}

function fetchPayeesData(){
  var resData = [];
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options ={    "access": "online",
                "objectName": "RBObjects"
               };
  var headers = {"session_token":kony.retailBanking.globalData.session_token};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("Payee",serviceName,options);
  var dataObject = new kony.sdk.dto.DataObject("Payee");
  var serviceOptions = {"dataObject":dataObject, "headers":headers};
  modelObj.customVerb("getRecentPayee",serviceOptions, dataSuccess, customErrorCallback);



  function dataSuccess(response){
    payeedatalength = response.Payee.length;
    kony.print("Payee account length = " + payeedatalength);
    var rowData;
    var contactName;
    frmNewBillKA.segPayeeNamesKA.widgetDataMap={
      lblContact:"payeeNickName",
      lblAccountNumberKA : "payeeId",
      lblRowSeparator: "rowSeparator"
    };
	payeelength=response.Payee.length;
    for(var i=0;i< response.Payee.length;i++)
    {       
      if(response.Payee[i].payeeNickName === "" || response.Payee[i].payeeNickName ==null)
      {
        //response[i].nickName = response[i].beneficiaryName;
        contactName = response.Payee[i].payeeName;
      }
      else /* Added code to set rowSeparator as a part of refactoring */
        contactName = response.Payee[i].payeeNickName;
      rowData = {"payeeNickName": contactName, "payeeId": response.Payee[i].payeeId,
                "rowSeparator":{skin: sknCopyslFbox0a29a14ecfe6442}
                };
      
      resData.push(rowData);
    }
    if(resData.length===0){
		frmBillPayeeDetailsKA.flexRecent.setVisibility(true);      
    }
    else{
      	frmBillPayeeDetailsKA.flexRecent.setVisibility(false);
    }
    frmNewBillKA.segPayeeNamesKA.setData(resData);
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmNewBillKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setCustomInfo("showState",billPayShowState);
    if(billPayShowState !== "InitialLanding")
        navObject.setCustomInfo("BillPayTransactionData",billPayTxnObj);
    navObject.setRequestOptions("segInternalFromAccountsKA",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
    listController.performAction("navigateTo",["frmNewBillKA",navObject]);
  }
}

function fetchPayeesDataDetails(response){
	var resData = [];
	payeedatalength = response.segPayeeNamesKA.length;
    kony.print("Payee account length = " + payeedatalength);
    var rowData;
    var contactName;
    frmBillPayeeDetailsKA.segPayeeNamesKA.widgetDataMap={
      lblContact:"payeeNickName",
      lblAccountNumberKA : "payeeId",
      lblRowSeparator: "rowSeparator"
    };
	for(var i=0;i< response.segPayeeNamesKA.length;i++)
    {       
      if(response.segPayeeNamesKA[i].payeeNickName === "" || response.segPayeeNamesKA[i].payeeNickName ==null)
      {
        //response[i].nickName = response[i].beneficiaryName;
        contactName = response.segPayeeNamesKA[i].payeeName;
      }
      else /* Added code to set rowSeparator as a part of refactoring */
        contactName = response.segPayeeNamesKA[i].payeeNickName;
      rowData = {"payeeNickName": contactName, "payeeId": response.segPayeeNamesKA[i].payeeId,
                "rowSeparator":{skin: sknCopyslFbox0a29a14ecfe6442}
                };
      
      resData.push(rowData);
    }
  	frmBillPayeeDetailsKA.segPayeeNamesKA.setData(resData);
  
}		

function setFromContainerAttributesBillPay(showState){
    if(showState === "InitialLanding"||showState === "SpecifiedPayee"){
        var settingsObj = kony.store.getItem("settingsflagsObject");
		var currForm = kony.application.getCurrentForm().id;
      	var preferedSelAcnt;
      if(currForm == "frmAccountDetailKA"){
        var accNo = kony.retailBanking.globalData.globals.nav_Object["accountID"];
        preferedSelAcnt = kony.retailBanking.globalData.accounts.searchAccountById(accNo);
      }
      else
		preferedSelAcnt = kony.retailBanking.globalData.accounts.searchAccountById(settingsObj.DefaultPaymentAcctNo);  
        if(preferedSelAcnt !==""){
            setContainerState(frmNewBillKA,"from","INTERNAL_SELECTED", defaultCardHeight);
        }     
        else{
            setContainerState(frmNewBillKA,"from","INITIAL_EXPANDED", fromCardHeight); 
        }
    }
    if(showState === "EditExistingBillPay" || showState === "EditNewBillPay" ||
        showState === "RepeatBillPay"){
        // From account data has already been set, so just set container state
        setContainerState(frmNewBillKA,"from","INTERNAL_SELECTED", defaultCardHeight);
    }
}

/* The to container state is determined based on how we arrive on this page */
function setToContainerAttributesBillPay(showState){
    frmNewBillKA.editToCard.setVisibility(true);
    if(showState === "InitialLanding"){
        setContainerState(frmNewBillKA,"to","INITIAL_EXPANDED", toCardHeightPayBill);
       
    }
    if(showState === "EditExistingBillPay" || showState === "EditBillPay" ||
        showState === "RepeatBillPay"||showState === "SpecifiedPayee" || showState === "SpecifiedPayeeTo"){
        // To account data has already been set, so just set container state
        // Also INTERNAL and EXTERNAL SELECTED does have an effect on the state
        setContainerState(frmNewBillKA,"to","INTERNAL_SELECTED", defaultCardHeight);
        if(showState === "EditExistingBillPay"){
            frmNewBillKA.editToCard.setVisibility(false);
        }
    }
}

function setPayeeIntoToCard(){ 
  frmNewBillKA["toNamePick"].text =  frmNewBillKA.segPayeeNamesKA.selectedRowItems[0].payeeNickName; 
  frmNewBillKA["tolblAccountNumberKA"].text =  frmNewBillKA.segPayeeNamesKA.selectedRowItems[0].payeeId;
  
}

function newConfirmBillPay(fromForm,toForm){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController(fromForm);
  var viewModel = controller.getFormModel();
  var amount = viewModel.getViewAttributeByProperty("amountTextField","text");
  var fromAccountBal =   viewModel.getViewAttributeByProperty("fromAmountPick","text");
  fromAccountBal = fromAccountBal.slice(2);
  //var fromIndex = viewModel.getViewAttributeByProperty("segInternalFromAccountsKA", "selectedRowIndex");
  //var toIndex = viewModel.getViewAttributeByProperty("segInternalTOAccountsKA", "selectedRowIndex");
  var fromAccountNumber =  viewModel.getViewAttributeByProperty("fromlblAccountNumberKA","text");
  var toAccountNumber =    viewModel.getViewAttributeByProperty("tolblAccountNumberKA","text");

  if(fromAccountNumber !== "" && toAccountNumber !== ""){

   
      var validated = validateDecimals(amount);
      amount = amount;
      if((amount===null) || (amount==="") || (Number(amount)< 1))
      {
        frmNewBillKA.lblInvalidAmount.setVisibility(true);
      }else if(validated) //if((Number(amount)<=Number(fromAccountBal)))
      {
        var listController = INSTANCE.getFormController(toForm);
        var navigationObject = new kony.sdk.mvvm.NavigationObject();
        var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
        if(frmNewBillKA.lblTransactionIdKA.text == null || frmNewBillKA.lblTransactionIdKA.text == "") // {
          navigationObject.setDataModel(null,kony.sdk.mvvm.OperationType.ADD, "form");
          navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
        //}else  {
          //navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
        //  navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}, "queryParams" : {"transactionId": frmNewBillKA.lblTransactionIdKA.text}});
        //}
        var BillPayTransactionData = constructBillPayTransactionObject();
        navigationObject.setCustomInfo("BillPayTransactionData", BillPayTransactionData);
        listController.performAction("navigateTo",[toForm,navigationObject]);
      }/*else
      {
        kony.ui.Alert({
          "message": "Entered amount is more than the Available Balance",
          "alertType": constants.ALERT_TYPE_INFO,
          "alertTitle":"INFO",
          "yesLabel": "MODIFY",
          "noLabel": null,
          "alertHandler":onModifyClick
        },{});
      }*/
  }  
  else if(fromAccountNumber !=="" && toAccountNumber === ""){
    frmNewBillKA.toCardTitle.skin = "sknlblD0021BLatoMedium";
    frmNewBillKA.scrollToBeginning();
  }else if(fromAccountNumber ==="" && toAccountNumber !== ""){
    frmNewBillKA.fromCardTitle.skin = "sknlblD0021BLatoMedium";
    frmNewBillKA.scrollToBeginning();
  }else{
    toastMsg.showToastMsg("select From and To accounts",3000);
  }
}

function constructBillPayTransactionObject(){
    var newTransferTransactionData = {};
    newTransferTransactionData.transferAmount = frmNewBillKA.amountTextField.text;
    newTransferTransactionData.fromAccountName = frmNewBillKA.fromNamePick.text;
    newTransferTransactionData.fromAccountNumber = frmNewBillKA.fromlblAccountNumberKA.text;
    newTransferTransactionData.fromAccountBalance = frmNewBillKA.fromAmountPick.text;
    newTransferTransactionData.fromAccountColor = frmNewBillKA.fromAccountColorPick.skin;
  	newTransferTransactionData.payeeNickName = frmNewBillKA.toNamePick.text;
    newTransferTransactionData.payeeId = frmNewBillKA.tolblAccountNumberKA.text;
  
    newTransferTransactionData.transferDate = frmNewBillKA.calDateKA.dateComponents;
  
    newTransferTransactionData.transferTransactionType = frmNewBillKA.lblTransactionType.text;
    newTransferTransactionData.transferTransactionID = frmNewBillKA.lblTransactionIdKA.text;
    return newTransferTransactionData;
}

function setDataIntoConfirmBillPayfrom(){
  var BillPayTransactionData;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var formController = INSTANCE.getFormController("frmConfirmPayBill");
  var formModel = formController && formController.getFormModel();
  var formControllerContextData= formController.getContextData();
  
  formModel.clear();
  
  if(formControllerContextData && formControllerContextData.getCustomInfo("BillPayTransactionData")){
    BillPayTransactionData = formControllerContextData.getCustomInfo("BillPayTransactionData");   
    var amount = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(BillPayTransactionData.transferAmount,
                                    kony.retailBanking.globalData.globals.CurrencyCode);                            
                                    
   
        
    // start the bind
        	formModel.setViewAttributeByProperty("transactionAmount","text",amount);
			formModel.setViewAttributeByProperty("transactionName","text",BillPayTransactionData.payeeNickName);
			formModel.setViewAttributeByProperty("transactionFrom","text",BillPayTransactionData.fromAccountName);
              
    	formModel.setViewAttributeByProperty("MapedAmountLabel","text",BillPayTransactionData.transferAmount);
    	formModel.setViewAttributeByProperty("fromAccountNumberKA","text",BillPayTransactionData.payeeId);
    	formModel.setViewAttributeByProperty("fromAccountNameKA","text",BillPayTransactionData.payeeNickName);
    	formModel.setViewAttributeByProperty("fromAccNumberKA","text",BillPayTransactionData.fromAccountNumber);
    	var transferFormattedDate = getFormattedDateFromCalendarDate(BillPayTransactionData.transferDate);  
    	var backendDate=JSON.stringify(kony.retailBanking.util.formatingDate.getDBDateTimeFormat(BillPayTransactionData.transferDate,"00:00"));
    	backendDate=backendDate.replace(/['"]+/g, '');
    	formModel.setViewAttributeByProperty("mapedDateKA","text",backendDate);
        formModel.setViewAttributeByProperty("lblScheduledDate","text",kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(transferFormattedDate));
        
    	formModel.setViewAttributeByProperty("transactionType","text",BillPayTransactionData.transferTransactionType);
    	formModel.setViewAttributeByProperty("transactionId","text",BillPayTransactionData.transferTransactionID);
  }
}

function onConfirmPayBillEditButtonClick(){
    var receivedNavObject = getCustomInfoObject("frmConfirmPayBill", "BillPayTransactionData")
    navigateToNewBillPayForm("EditNewBillPay", receivedNavObject);
}

function detailsBillPayClick(data,state){
  var showState = "RepeatBillPay";
  var BillPayTransactionData={};
  var tempAmount=data.amount.slice(1, data.amount.length);
  tempAmount=""+parseInt(tempAmount.replace(/[^0-9-.]/g, ''));
  BillPayTransactionData.transferAmount = tempAmount;
  BillPayTransactionData.fromAccountName=data.fromNickName;
  BillPayTransactionData.fromAccountNumber=data.fromAccountNumber;
  BillPayTransactionData.fromAccountBalance=data.fromAccountBalance;
  BillPayTransactionData.fromAccountColor=getSkinColor(data.fromAccountType);
  BillPayTransactionData.payeeNickName=data.payeeNickName;
  BillPayTransactionData.payeeId=data.payeeId;
  BillPayTransactionData.transferTransactionID=data.transactionId;
  BillPayTransactionData.transferTransactionType=data.transactionType;
  var date=new Date();
  BillPayTransactionData.transferDate=[date.getDate(),(date.getMonth())+1,date.getFullYear(),0,0,0];
  if(state=="EditExistingTransfer"){
    date = data.scheduledDate;
   	var year = date.slice(0,4);
    var month = date.slice(5,7);
    var day = date.slice(8,10);
  	BillPayTransactionData.transferDate= [parseFloat(day), parseFloat(month), parseFloat(year), 0.0, 0.0, 0.0];
    showState="EditExistingBillPay"
  }
  navigateToNewBillPayForm(showState, BillPayTransactionData);
  
}

function goBackInBetweenBillPay(){
 if(BillPayfromForm==="ManagePayee"){
    showFormManagePayeeList();   
  }
}
function fromManagePayeetoBillPay(){
    BillPayfromForm="ManagePayee";
  	var  newTransferTransactionData={};
   	var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmPayeeDetailsKA");
    var listController = INSTANCE.getFormController("frmPayeeTransactionsKA");
    var controllerContextData= listController.getContextData();
    if( controllerContextData && controllerContextData.getCustomInfo("selectedPayeeObj")){
         var payeeDetails =  controllerContextData.getCustomInfo("selectedPayeeObj");
         newTransferTransactionData.payeeNickName = payeeDetails["payeeNickName"];
    	 newTransferTransactionData.payeeId = payeeDetails["payeeId"];         
        }
  navigateToNewBillPayForm("SpecifiedPayee",newTransferTransactionData);
}

function getFilteredBillPayAccounts(fromAccountsData,data){
   var fromProcessData = data.segInternalFromAccountsKA.segInternalFromAccountsKA.getData();
  var fromData = [];
  for(var i in fromAccountsData)
  {    
    if(fromAccountsData[i]["supportBillPay"]==="1")
    {
      if( fromAccountsData[i]["nickName"]!== null|| fromAccountsData[i]["nickName"]!== "")
        fromProcessData[i]["accountName"] = fromAccountsData[i]["nickName"];

      fromProcessData[i]["availableBalance"] = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(fromAccountsData[i]["availableBalance"],kony.retailBanking.globalData.globals.CurrencyCode);
      fromProcessData[i]["sknColor"]= {skin:getSkinColor(fromAccountsData[i]["accountType"])};
      fromProcessData[i]["sknRowSepColor"] = {skin: sknCopyslFbox0a29a14ecfe6442};
      fromData.push(fromProcessData[i]);
      
    }
  }
      fromdatalength = fromData.length;
	  return fromData;
}

function editedPayeeBillPay(){
      BillPayfromForm="ManagePayee";
   var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var listController = INSTANCE.getFormController("frmEditPayeeKA");
	    var controllerContextData= listController.getContextData();
        if( controllerContextData && controllerContextData.getCustomInfo("payeeObjectEdited")){
         var payeeDetails =  controllerContextData.getCustomInfo("payeeObjectEdited");
          var newTransferTransactionData={};
          newTransferTransactionData.payeeNickName = payeeDetails["payeeNickName"];
    	 newTransferTransactionData.payeeId = payeeDetails["payeeId"];  
          navigateToNewBillPayForm("SpecifiedPayee",newTransferTransactionData); 
        }
}

function newPayeeAddedBillPay(fromForm){
	globalCount=0;
	BillPayfromForm=fromForm;
   	var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAddNewPayeeKA");
    var controllerContextData= listController.getContextData();
	if( controllerContextData && controllerContextData.getCustomInfo("payeeObject")){
    	var payeeDetails =  controllerContextData.getCustomInfo("payeeObject");
        var newTransferTransactionData={};
        newTransferTransactionData.payeeNickName = payeeDetails["payeeNickname"];
    	newTransferTransactionData.payeeId = payeeDetails["payeeId"];  
        navigateToNewBillPayForm("SpecifiedPayee",newTransferTransactionData); 
   }
}