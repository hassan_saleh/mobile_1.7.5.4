/*
 * Controller Extension class for frmConfirmPayBill
 * Developer can edit the existing methods or can add new methods if required
 *
 */

kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};

/**
 * Creates a new Form Controller Extension.
 * @class frmConfirmPayBillControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmConfirmPayBillControllerExtension =  Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.selectedScheduledDate = null;
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmConfirmPayBillControllerExtension#
     */
    fetchData: function() {
        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
	        this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.sdk.mvvm.log.info("success fetching data ", response);
            scopeObj.getController().processData(response);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmConfirmPayBillControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
            var scopeObj = this;
            var processedData = this.$class.$superp.processData.call(this, data);
            this.getController().bindData(processedData);
            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        };
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmConfirmPayBillControllerExtension#
     */
    bindData: function(data) {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.clear();
            //this.$class.$superp.bindData.call(this, data);
            //this.getController().getFormModel().formatUI();
			var navigationObject = this.getController().getContextData();
           /* var tempData = navigationObject.getCustomInfo("payBillObject"); 
			var amount = kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(tempData.amount,kony.retailBanking.globalData.globals.CurrencyCode);
			formmodel.setViewAttributeByProperty("transactionAmount","text",amount);
			formmodel.setViewAttributeByProperty("transactionName","text",tempData.toAccName);
			formmodel.setViewAttributeByProperty("transactionFrom","text",tempData.fromAccName);
              
            formmodel.setWidgetData("transactionType",kony.retailBanking.globalData.globals.PayBill);
			formmodel.setWidgetData("fromAccNumberKA",tempData.fromAccNumber);
			formmodel.setWidgetData("MapedAmountLabel",tempData.amount);
			formmodel.setWidgetData("fromAccountNumberKA",tempData.toAccNumber);
			formmodel.setWidgetData("fromAccountNameKA",tempData.toAccName);
			
			var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
              var controller = INSTANCE.getFormController("frmNewBillKA");
              var controllerContextData = controller.getContextData();
              if(controllerContextData && controllerContextData.getCustomInfo("transactionId")){
                formmodel.setWidgetData("transactionId",controllerContextData.getCustomInfo("transactionId"));
              }else{
                formmodel.setWidgetData("transactionId","");
              }
			
			
			   /*     var payBill = {
          "fromAccNumber" : fromAccNumber,
          "toAccNumber" : toAccNumber,
          "amount" : amount,
          "toAccName": toAccName,    
          "fromAccName":fromAccName,
          "scheduledDate" : scheduledDate
        };*
      this.selectedScheduledDate = tempData.scheduledDate;
                
      var month,month1;
      var day,day1;
      var day = parseInt(this.selectedScheduledDate[0]);
      var  month = parseInt(this.selectedScheduledDate[1]);
      if(month <10){
        month = "0"+month;
      }
      /*else{
           month1 = month;
        }*
      if(day <10){
        day = "0"+day;
      }/*else{
           day1 = day;
         }*
      var formattedDate = this.selectedScheduledDate[2]+"-"+month+"-"+day;
      // var formattedDate =scheduledDate[0]+"-"+scheduledDate[1]+"-"+scheduledDate[2];
      //var tempDate = kony.retailBanking.util.formatingDate.getISODateTimeKA(formattedDate, kony.retailBanking.util.BACKEND_DATE_FORMAT);
      //var scheduledDate2 = (tempDate instanceof Date)? tempDate.format('F d, Y') : "";
	  var scheduledDate2 = kony.retailBanking.util.formatingDate.getApplicationFormattedDateKA(formattedDate);
      formmodel.setViewAttributeByProperty("lblScheduledDate","text",scheduledDate2);
	  var dbDate = kony.retailBanking.util.formatingDate.getDBDateTimeFormat(this.selectedScheduledDate,"00:00");
	  formmodel.setWidgetData("mapedDateKA",dbDate);
	*/		setDataIntoConfirmBillPayfrom();
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            this.getController().showForm();
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

    },
    /** 
     /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmConfirmTransferKAControllerExtension#
     */
    saveData: function() {
//       var onBillPaySecondFactorAuth =function (response){
//       	if(response["status"]==="success"){
         try {
//  	if (isSettingsFlagEnabled("isFaceEnrolled"))
//               {
//                     var BC = applicationManager.getTransactionBusinessController();
//                     BC.faceIDBillPayCall(presentationSuccessCallback, presentationErrorCallback);
//                     var scopeObj = this;
//                     function presentationSuccessCallback(res)
//                     {
//                            if(res.authenticationRequired === "true")
//                            { 

//                                   ShowLoadingScreen();
//                                   intializeFacialAuth();
//                                   FaceAuth_initialize2(); 
//                                   FaceAuth_verify2();
//                                   function FaceAuth_verify2(){
//                                      faceIdService.verify({
//                                         onSuccess : function(){
//                                             successFormPreShow();
//                                             frmSuccessFormKA.show();
//                                             successFormanimationShow();
//                                             scopeObj.$class.$superp.saveData.call(scopeObj, success, error);
//                                         },
//                                         onFailed : function(e){showErrorFaceauth(e);}
//                                         });
//                                   }                                                     
//                            }
//                            else
//                            {
//                                   successFormPreShow();
//                                   frmSuccessFormKA.show();
//                                   successFormanimationShow();
//                                   scopeObj.$class.$superp.saveData.call(scopeObj, success, error);
//                            } 
//                     }
//                     function presentationErrorCallback(err)
//                     {
//                              kony.print("error");
//                     }
//               }     
//               else
//               {
//                   var scopeObj = this;
//                   successFormPreShow();
//                   frmSuccessFormKA.show();
//                   successFormanimationShow();
//                   scopeObj.$class.$superp.saveData.call(scopeObj, success, error);
//               } 
           
       //Audit Logger
           if(frmConfirmPayBill.lblPaymentMethod.text === "CCARD"){
             logObj[0] = mask_CreditCardNumber(frmConfirmPayBill.lblAccountNumber.text); //frmAccount,
           }else{
//              if(kony.boj.selectedBillerType === "PrePaid")
//              	logObj[0] = frmConfirmPayBill.lblAccountNumber.text; //frmAccount,
//              else
           	logObj[0] = frmConfirmPayBill.lblAccountNumber.text; //frmAccount,
           }
           
           logObj[1] = frmConfirmPayBill.lblAccBr.text; //frmAccountBr,
           logObj[2] = frmConfirmPayBill.lblCurrencyType.text; //frmAccountCurr,

           logObj[5] = kony.boj.selectedBillerType; //billtype,
           logObj[6] = frmConfirmPayBill.lblBillingNumber.text; //billerno,
           logObj[7] = frmConfirmPayBill.lblBillerCode.text; //billername,
           logObj[8] = frmConfirmPayBill.lblSvcType.text; //servicetype,
           logObj[12] = frmConfirmPayBill.lblPaidAmnt.text; //transferamt,
// 			if(kony.boj.selectedBillerType === "PrePaid")
//             	logObj[12] = frmNewBillKA.tbxAmount.text.replace(/,/g,""); //transferamt,
           logObj[13] = "BILLPAY"; //transferType,
           
       var scopeObj = this;
       scopeObj.$class.$superp.saveData.call(scopeObj, success, error);

        } 
  		catch (err) {
  			kony.application.dismissLoadingScreen();
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
          alert(exception.toString());
        }
//                                                             }
//         else{alert(response["status"]+": "+response["msg"]);}

      function success(res) {
        kony.application.dismissLoadingScreen();
        kony.sdk.mvvm.log.info("success saving record ", res);
        kony.print("Bill Payment Success Response ::"+JSON.stringify(res));
        if(res.ErrorCode === "00000" || res.ErrorCode === "0" || res.ErrorCode == 0 && res.referenceId !== "" && res.referenceId !== null){
          var x;
          logObj[16] = res.referenceId; // referumber
          logObj[17] = "SUCCESS"; //status
          logObj[18] = kony.boj.selectedBillerType + " Bill Pay Successful";
          loggerCall();
          
          if(gblBillHome == "Biller")
            x = geti18nkey ("i18n.common.gotoBillsScreen");
          else
            x= geti18nkey ("i18n.common.gotobp");
          var data = kony.boj.AllPayeeList;//frmManagePayeeKA.managepayeesegment.data;
          var billerCode = frmConfirmPayBill.lblBillerCode.text;
          var billingNumber = frmConfirmPayBill.lblBillingNumber.text;
          var isRegistered = false;
//           kony.print("Biller data ::"+JSON.stringify(data.length));
          if(data !== undefined && data !== null && data !== []){
            for(var i = 0; i < data.length; i++){
              if((data[i].BillerCode === billerCode) && (data[i].BillingNumber === billingNumber)){
                kony.print("Registered Biller ::"+JSON.stringify(data[i]));
                isRegistered = true;
                break;
              }
            }
          }
          /*if(kony.boj.selectedPayee !== null && kony.boj.selectedPayee !== undefined){
            if((kony.boj.selectedPayee.BillingNumber === billingNumber) && (kony.boj.selectedPayee.BillerCode === billerCode)){
              isRegistered = true;
            }
          }*///|| gblQuickFlow == "pre"
          if(isRegistered === true){
            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.bills.TransactionReferenceNumber")+" "+res.referenceId , geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"),x,"MyBillers","","AddNewBiller");
          }else{
            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.bills.TransactionReferenceNumber")+" "+res.referenceId , geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"),x,"MyBillers",geti18nkey ("i18n.bills.RegisterThisBiller"),"AddNewBiller");
          }
          }
        else{
          var x;
          logObj[16] = ""; // reference number
          logObj[17] = "FAILURE"; //status,
          logObj[18] = res.ErrorCode +  ":" +getErrorMessage(res.ErrorCode); // statuscomments
          loggerCall();
          if(gblBillHome == "Biller")
            x = geti18nkey ("i18n.common.gotoBillsScreen");
          else
            x= geti18nkey ("i18n.common.gotobp");
          var Message = getErrorMessage(res.ErrorCode);
          customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
          kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), Message, geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard") , x , "MyBillers");
          }
          
          
          
          
          
          
          
//                 var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
//       			var listController = INSTANCE.getFormController("frmSuccessFormKA");
//       			var navObject = new kony.sdk.mvvm.NavigationObject();
//       			if(res.referenceId){
//       				 navObject.setCustomInfo("TransactionID",i18n_referenceId+ res.referenceId);
//       			}else{
//       				 navObject.setCustomInfo("TransactionID",i18n_referenceId+ res.transactionId);
//       			}
//      // kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
//                 listController.loadDataAndShowForm(navObject);
				
// 			   var amount = frmNewBillKA.amountTextField.text;
//                KNYMetricsService.sendCustomMetrics("frmSuccessFormKA", {"paymentType":"Bill Pay", "PayBillStatus":"Bill Payment", "payBillAverageAmount": parseInt(amount)});
			
        }

        function error(err) {
            //Handle error case
          kony.application.dismissLoadingScreen();
          
          logObj[16] = err.ErrorCode; // reference number
          logObj[17] = "FAILURE"; //status,
          logObj[18] = getErrorMessage(err.ErrorCode); // statuscomments
          loggerCall();
          kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), getErrorMessage(err.ErrorCode), geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard") , x , "MyBillers");
//             frmSuccessFormKA.successText.text = i18n_transactionFailed;
//             frmSuccessFormKA.successTitle.text =i18n_sorryForinconvenience;
//             frmSuccessFormKA.processing.text = i18n_processingTransaction;
//             errorFormPostShow();
            kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
          //alert("In failure callback"+ JSON.stringify(err));
            //var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            //kony.sdk.mvvm.log.error(exception.toString());
// 			var amount = frmNewBillKA.amountTextField.text;
//             var fromAccNumber = frmConfirmPayBill.fromAccNumberKA.text;
//             var toAccNumber = frmConfirmPayBill.fromAccNumberKA.text;
//             var fromAccName = frmConfirmPayBill.transactionFrom.text;
//             var toAccName = frmConfirmPayBill.fromAccountNumberKA.text;
//             KNYMetricsService.sendCustomMetrics("frmSuccessFormKA", {"paybillamount":amount.toString(),"paybillFromAccName":fromAccName,"paybillFromAccNum":fromAccNumber.toString(),"paybillToAccName":toAccName,"paybillToAccNum":toAccNumber.toString()});
        }
      //}
//       // Extension code
//       var serviceChargesforbillpay = function(resp){
//          if(resp["status"]=="success")
//           secondFactorAuth(this,onBillPaySecondFactorAuth);
//          else 
//             alert(resp["status"]+ ": "+ resp["msg"]);
//        }
//        serviceCharges(this,serviceChargesforbillpay);

   },
    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmConfirmPayBillControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmConfirmPayBillControllerExtension#
     */
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.showView();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    }
    });