/*
 * Controller Extension class for frmCreditCardPayment
 * Developer can edit the existing methods or can add new methods if required
 *
 */

kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};

/**
 * Creates a new Form Controller Extension.
 * @class frmCreditCardPaymentControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmCreditCardPaymentControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
  constructor: function(controllerObj) {
    this.$class.$super.call(this, controllerObj);
  },
  /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmCreditCardPaymentControllerExtension#
     */
  fetchData: function() {
    try {
      var scopeObj = this;
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
      this.$class.$superp.fetchData.call(this, success, error);
    } catch (err) {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
      var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
      kony.sdk.mvvm.log.error(exception.toString());
    }

    function success(response) {
      kony.sdk.mvvm.log.info("success fetching data ", response);
      scopeObj.getController().processData(response);
    }

    function error(err) {
      //Error fetching data
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
      var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
      kony.sdk.mvvm.log.error(exception.toString());
    }
  },
  /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmCreditCardPaymentControllerExtension#
     * @returns {Object} - processed data
     */
  processData: function(data) {
    try {
      var scopeObj = this;
      var processedData = this.$class.$superp.processData.call(this, data);
      this.getController().bindData(processedData);
      return processedData;
    } catch (err) {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
      var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
      kony.sdk.mvvm.log.error(exception.toString());
    };
  },
  /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmCreditCardPaymentControllerExtension#
     */
  bindData: function(data) {
    try {
      var formmodel = this.getController().getFormModel();
      formmodel.clear();
      this.$class.$superp.bindData.call(this, data);
      this.getController().getFormModel().formatUI();
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      this.getController().showForm();
    } catch (err) {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
      var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
      kony.sdk.mvvm.log.error(exception.toString());
    }

  },
  /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmCreditCardPaymentControllerExtension#
     */
  saveData: function() {
    try {
      var scopeObj = this;

      kony.print("FlagD :: "+ frmCreditCardPayment.lblPaymentFlag.text);
      kony.print("Card num D:: "+  frmCreditCardPayment.hiddenCardNumber.text);
      kony.print("account num D:: "+  frmCreditCardPayment.hiddenLblAccountNum.text);
      kony.print("Branch num D:: "+  frmCreditCardPayment.lblBranchNum.text);
      kony.print("AmountD :: "+  frmCreditCardPayment.lblHiddenAmount.text);
      frmCreditCardPayment.lblDum.text = Math.floor(Math.random()*100);
      var formmodel = this.getController().getFormModel();
      formmodel.setViewAttributeByProperty('lblPaymentFlag', "text", frmCreditCardPayment.lblPaymentFlag.text);
      formmodel.setViewAttributeByProperty('hiddenCardNumber', "text", frmCreditCardPayment.hiddenCardNumber.text);
      formmodel.setViewAttributeByProperty('hiddenLblAccountNum', "text", frmCreditCardPayment.hiddenLblAccountNum.text);
      formmodel.setViewAttributeByProperty('lblBranchNum', "text", frmCreditCardPayment.lblBranchNum.text);
      formmodel.setViewAttributeByProperty('lblHiddenAmount', "text", frmCreditCardPayment.lblHiddenAmount.text);


      this.$class.$superp.saveData.call(this, success, error);
    } catch (err) {
      var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
      kony.sdk.mvvm.log.error(exception.toString());
    }


    //Logging Info
    //     var logObj = [
    //     "", //frmAccount,0
    //     "", //frmAccountBr,1
    //     "", //toAccount,3

    //     "", //transferamt,12
    //     "", //transferType,13

    //     "", // referenumber16
    //   	"", //status,17
    //     "", // statuscomments,18
    //     "", //description19

    // ];

    logObj[0] = frmCreditCardPayment.hiddenLblAccountNum.text;
    logObj[1] = frmCreditCardPayment.lblBranchNum.text;
    logObj[2] = frmCreditCardPayment.lblFromCurr.text;
    logObj[3] = mask_CreditCardNumber(frmCreditCardPayment.hiddenCardNumber.text);
    logObj[12] = frmCreditCardPayment.lblHiddenAmount.text;
    logObj[13] = "CARDPAY";

    function success(res) {
      kony.application.dismissLoadingScreen();
      kony.print("FlagC :: "+ frmCreditCardPayment.lblPaymentFlag.text);
      kony.print("Card numC :: "+  frmCreditCardPayment.hiddenCardNumber.text);
      kony.print("account numC :: "+  frmCreditCardPayment.hiddenLblAccountNum.text);
      kony.print("Branch numC :: "+  frmCreditCardPayment.lblBranchNum.text);
      kony.print("AmountC :: "+  frmCreditCardPayment.lblHiddenAmount.text);
      if(!isEmpty(res)){
        kony.print("Success "+res);
        //alert("Success "+JSON.stringify(res));
        kony.application.dismissLoadingScreen();
        kony.print("Response - card payment :: "+JSON.stringify(res));
        if(!isEmpty(res.ErrorCode)){
          if(res.ErrorCode == 0){
            //customAlertPopup("Transaction Reference Id",res.referenceId,popupCommonAlertDimiss,"");
            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.payment.paySucc"), geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard"), geti18nkey("i18n.cards.gotoCards"), "Cards","", "", geti18Value("i18n.common.ReferenceId") + " "  + res.referenceId);
            logObj[16] = res.referenceId; 
            logObj[17] = "SUCCESS"
            logObj[18] = "Payment Successful"  
//             frmCongratulations.show();
//             frmCreditCardPayment.dismiss();
          }
          else{
            var Message = getErrorMessage(res.ErrorCode);
            //alert("Message ::" + Message);
            if(isEmpty(Message)){
              Message = getServiceErrorMessage(res.ErrorCode,"frmCreCarPayContExtSuccSaveData");
            }        
            //alert("Message after ::" + Message);
//             customAlertPopup(geti18Value("i18n.Bene.Failed"), Message,popupCommonAlertDimiss,"");
            kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), Message,geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard"),geti18Value("i18n.cards.gotoCards"), "Cards");
            logObj[16] =""; 
            logObj[17] = "FAILURE";
            logObj[18] =  res.ErrorCode +" " +Message;
//             frmCongratulations.show();
//             frmCreditCardPayment.dismiss();
          }
        }
        else{
          logObj[16] = ""; 
          logObj[17] = "FAILURE";
          logObj[18] = "";
          customAlertPopup(geti18nkey("i18n.NUO.Error"), 
                           geti18Value("i18n.common.somethingwentwrong"),
                           popupCommonAlertDimiss, "");
          kony.application.dismissLoadingScreen();
        }

      }
      else{
        logObj[16] = ""; 
        logObj[17] = "FAILURE";
        logObj[18] = "";
        customAlertPopup(geti18nkey("i18n.NUO.Error"), 
                         geti18Value("i18n.common.somethingwentwrong"),
                         popupCommonAlertDimiss, "");
        kony.application.dismissLoadingScreen();
      }
      gblTModule = "";
      loggerCall();
//       preshowcard();
    }

    function error(err) {
      //Handle error case
      kony.application.dismissLoadingScreen();
      gblTModule = "";
      kony.print("Response - card payment :: "+JSON.stringify(err));
      kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
      kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "",geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard"),geti18Value("i18n.cards.gotoCards"), "Cards");
      var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
      kony.sdk.mvvm.log.error(exception.toString());
      loggerCall();
//       preshowcard();
    }

  },
  /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmCreditCardPaymentControllerExtension#
     */
  deleteData: function() {
    try {
      var scopeObj = this;
      this.$class.$superp.deleteData.call(this, success, error);
    } catch (err) {
      var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
      kony.sdk.mvvm.log.error(exception.toString());
    }

    function success(res) {
      //Successfully deleting record
      kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
    }

    function error(err) {
      //Handle error case
      kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
      var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
      kony.sdk.mvvm.log.error(exception.toString());
    }
  },
  /** 
     * This method shows form.
     * @memberof frmCreditCardPaymentControllerExtension#
     */
  showForm: function() {
    try {
      var formmodel = this.getController().getFormModel();
      formmodel.showView();
    } catch (e) {
      var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
      kony.sdk.mvvm.log.error(exception.toString());
    }
  }
});