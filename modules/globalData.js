kony = kony || {};
kony.retailBanking = kony.retailBanking || {};
kony.retailBanking.globalData = kony.retailBanking.globalData || {};
kony.retailBanking.globalData.session_token ="";
kony.retailBanking.globalData.jmpBeneDetails=null;
kony.retailBanking.globalData.deviceInfo = kony.retailBanking.globalData.deviceInfo || {};
kony.retailBanking.globalData.deviceInfo = {
	    getDeviceInfo : function() {   //returns an object which contains name(defines the OS) and version number of the OS
        	var devInfo = kony.os.deviceInfo();
            var obj = {
            		"name": devInfo.name,
            		"version":devInfo.version,
              		"model":devInfo.model,
                    "deviceID":deviceid
            	};
            	return obj;
        },

	    isIphone:function() { //returns true if its an iphone else false
   		    var isIPhone = false;
    		try {
       			  var deviceName = kony.retailBanking.globalData.deviceInfo.getDeviceInfo().name;
        		  if (deviceName === "iPhone" || deviceName === "iPhone Simulator" || deviceName === "iPad" || deviceName === "iPad Simulator")
        		  	          isIPhone = true;
    			} catch (e) {
        				      isIPhone = false;
    			}
   				return isIPhone;
		},

		isIpad : function() { //returns true if its an ipad else false
    		var isIPad = false;
    		try {
      				var deviceName = kony.retailBanking.globalData.deviceInfo.getDeviceInfo().name;
      				if (deviceName === "iPad" || deviceName === "iPad Simulator") 
      					       isIPad = true;
   				} catch (e) {
       				           isIPad = false;
    			}
   				return isIPad;
		},
	    isTouchIDSupported : function() {  //returns true if device supports touchId else  false
            var status = kony.localAuthentication.getStatusForAuthenticationMode(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID);
            kony.print("Status for the touch/faceid :: "+ status);
	        if(status == 5006 || status == 5008)
		     {
			    return false;
		     }
		    else
		     {
			   return true;
		     }
        }
};  

kony.retailBanking.globalData.applicationProperties = kony.retailBanking.globalData.applicationProperties || {};
kony.retailBanking.globalData.applicationProperties = {
	    appProperties : {newVersionInfo : null,appUpgradeMandatory : null,newVersionLink : null,
	                     minSupportedOSVersion : null,accoutnSummaryEnabled : null,
	                     preLoginSummaryEnabled : null,bannerURL : null,bankID : null,businessDays : null },
	    setApplicationProperties: function(response) {
	    	appProperties.newVersionInfo = response.newVersionInfo;
	    	appProperties.appUpgradeMandatory = response.appUpgradeMandatory;
	    	appProperties.newVersionLink = response.newVersionLink;
	    	appProperties.minSupportedOSVersion = response.minSupportedOSVersion;
	    	appProperties.accoutnSummaryEnabled = response.accoutnSummaryEnabled;
	    	appProperties.preLoginSummaryEnabled = response.preLoginSummaryEnabled;
	    	appProperties.bannerURL = response.bannerURL;
	    	appProperties.bankID = response.bankID;
	    	

	    },
	    getApplicationProperties: function() {
	    	return appProperties;
        }
};

kony.retailBanking.globalData.accounts = kony.retailBanking.globalData.accounts || {};
kony.retailBanking.globalData.accounts = {
	           accounts : [],
            
            setAccountsData : function(response) { //sets the array of accounts
            	accounts = response;
            },
            getAccountsData : function() { //returns an array of accounts.
                 return accounts;
            },
            searchAccount: function(searchProperty,searchValue) {//returns the account object which is required.
              //Input would be the property and the value on which search is performed (for example: searchAccount("bankName","HDFC"))
			  var resultArray =  []; 
			  var index = 0;
              var accounts_array = kony.retailBanking.globalData.accounts.getAccountsData();
            	for (var len=0;len<accounts_array.length;len++){
            		if(accounts_array[len][searchProperty] == searchValue)
            			   resultArray[index++] = accounts_array[len];
            	}
			  return resultArray;
            },
           searchAccountById:function(AccountId)
  			{
              var AccountData = "";
              var allAccounts = kony.retailBanking.globalData.accounts.getAccountsData();  
  			   for (var len=0;len<allAccounts.length;len++){
            		if(allAccounts[len]["accountID"] == AccountId)
            			  {
                            AccountData = allAccounts[len];
                          }
            	}
              return AccountData;
            }
			
 };


kony.retailBanking.globalData.globals = kony.retailBanking.globalData.globals || {};
kony.retailBanking.globalData.globals ={
  							popTransferFlag:null,
  							transferSearch:0,
   							accountDashboardFlowFlag : true,
  							globalCountTransferInternal:0,
							globalCountTransferExternal:0,
                            formStack : [],
                            LOADING_SCREEN_MESSAGE : "Loading Data...",  
                            settings : {"rememberMeFlag":true,
                                        "touchIDEnabledFlag":null,
                                        "accountPreviewEnabledFlag":null,
                                        "isPinEnabledFlag":null,
                                        "isFacialAuthEnabledFlag":null,
                                        "isFaceEnrolled":null,
                                        "defaultLoginMode":"password",
                                        "deviceRegisterFlag":null,
                                        "defaultScreenEnum":"frmAccountsLandingKA",
                                        "DefaultTransferAcctNo":"",
                                        "DefaultDepositAcctNo":"", 
                                        "DefaultPaymentAcctNo":"",
                                        "DefaultCardlessAcctNo":"",
                                        "alerts":null},
  						configPreferences:{
                          "currency":"USD",
                          "newAccount":"ON",
                          "rdc":"ON",
                          "accountInsight":"ON"
                        },
                          userObj : {"lastLoginTime" :null,
                                     "userFirstName" :null,
                                     "userLastName" :null,
                                     "email":null,
                                     "phone":null,
                                     "dateOfBirth":null,
                                     "userName":null,
                                     "ssn":null,
                                     "depositsTCaccepted":null,
                                     "acntStatementTCaccepted":null,
                                     "addressLine1" : null,			
									 "addressLine2" : null,	
									 "city" : null,	
									 "country" : null,	
									 "state" : null,	
									 "zipcode"	: null	,
									 "isPinSet":null ,
                                     "nav_Object" : null
                                    },
                          CurrencyCode : "",
						   German :"de_DE",
							SwitzerlandGerman :"de_CH",
						SwitzerlandFrench :"fr_CH",
				SwitzerlandItaly :"it_CH",
					GermanAustria: "de_AT",
  FrenchBelgium : "fr_BE",
  DutchNetherlands:"nl_NL",
  SpanishSpain:"es_ES",
  GreekGreece: "el_GR",
  RussianRussia: "ru_RU",
  SpanishArgentina : "es_AR",
  French : "fr_FR",
  						  BankName : null,
						  Checking : "C",
                          Savings : "S",
                          CreditCard : "CreditCard",
                          Deposit : "T",
                          Mortgage : "Mortgage",
                          AvailableBalance : i18n_availableBalance,
                          CurrentBalance : i18n_currentBalance,
                          MaturityDate : i18n_maturityDate,
                          PaymentDueDate : i18n_PaymentDueDateg,
                          LastStatementBalance : i18n_lastStatementBalanceg,
                          OustandingBalance : "Outstanding Balance",
                          InterestRate : "Interest Rate",
						  PayBill : "BillPay",
  						  Cardless : "Cardless",
  					      PayPerson : "P2P",
                          TransferMoney : "InternalTransfer",
                          ExternalTransfer:"ExternalTransfer",
						   Daily : "Daily",
  						  Weekly : "Weekly",
  					      BiWeekly : "BiWeekly",
  						  Monthly : "Monthly",
  						  Once : "Once",
						  Failed : "Failed",
                          pageFlag :false,
                          DoReloadEditAccountInfo : true,
                          monthCredit : null,
                          currentmonth : null,
                          monthCash : null,
                          refreshTimeLabel : null,
                          usrName :null,
                          msgType : null,
						  IDLE_TIMEOUT :5,
                          msgDel:null,
						  UNCATEGORISED_CATEGORYID : null,
                          UNCATEGORISED : "Uncategorized",
						  PFM_ACCOUNTS_PRESENT : false,
						  GRAPH_CURRENT_MONTH_LABEL : "",
                          UNCATEGORISED_COUNT : 0,
                          decimalOnly : /^\d*\.?\d{0,2}$/,
  							serviceName : "RBObjects" 
};
//kony.retailBanking.datastore.setSettingsObject(kony.retailBanking.globalData.globals.settings);
kony.retailBanking.globalData.transfers = kony.retailBanking.globalData.transfers || {};
kony.retailBanking.globalData.accountsDashboardData = kony.retailBanking.globalData.accountsDashboardData || {};
kony.retailBanking.globalData.accountsTransactionList = kony.retailBanking.globalData.accountsTransactionList || {};
kony.retailBanking.globalData.accountsTransactionData = kony.retailBanking.globalData.accountsTransactionData || {};
kony.retailBanking.globalData.accountsTransactionDataPreview	=	kony.retailBanking.globalData.accountsTransactionDataPreview || {};
kony.retailBanking.globalData.accountsDashboardDataPreview = kony.retailBanking.globalData.accountsDashboardDataPreview || {};
kony.retailBanking.globalData.acccountsUnfilteredData = kony.retailBanking.globalData.acccountsUnfilteredData || null;
kony.retailBanking.globalData.prDWCardDetails = [];
kony.retailBanking.globalData.prCardLandinList = [];
kony.retailBanking.globalData.isCreditCardAvailable = false;
kony.retailBanking.globalData.isDebitCardAvailable = false;
kony.retailBanking.globalData.isWebchargeCardAvailable = false;
kony.retailBanking.globalData.prCreditCardPayList  = [];

kony.retailBanking.globalData.transfers = {
  transfers : [],
  setTransfersData : function(recentObjects,scheduleObjects) { //sets the array of transactions
    var transactionResponseObject = {};
    transactionResponseObject["recentTransactions"] = recentObjects;
    transactionResponseObject["scheduledTransactions"] = scheduleObjects;
    transfers =  transactionResponseObject; 
  },
  getTransfersData : function(segName) { //returns an transaction.
    return transfers[segName];
  }
};
kony.retailBanking.globalData.deposits = kony.retailBanking.globalData.deposits || {};
kony.retailBanking.globalData.deposits = {
  deposits : [],
  setDepositData : function(recentDeposits,pendingDeposits){
    var depositObject = {};
    depositObject["recentTransactions"] = recentDeposits;
    depositObject["scheduledTransactions"] = pendingDeposits;
    deposits =  depositObject; 
  },
  getDepositData : function(segName) { 
    return deposits[segName];
  }
};
kony.retailBanking.globalData.remitDetails = kony.retailBanking.globalData.remitDetails || null;
isUserSettingsEnable=false;
var REG_JMP_MOB = "";
var inputObj1 = [];
var inputObj2;
var inputObj3;
var flagtransfertype=1;
var flagaccounttype=2;
var flagjomopaytype=3;
var flagtype;
var scopeObjgbl;
var scopeObjgblResponse;
var DealsLoansCurrIndex = 0;
var gblAccountsCurrencyType = "JOD";
var gblDiffAccountsQuickBalOrder = true; 
var gblQuickAccessUser_id = "";
var	gblQuickAccessFlowFlag = "D";
var gblDefaultAccPayment   = "";
var gblDefaultAccTransfer  = "";
var gblQbalShowFlag			= "N";
var gblQtransFlagShowFlag   = "N";
kony.boj.gblAlertHistory   = [];
kony.boj.selectedBenf  = {};
kony.boj.deleteFlagBenf = false;
var gbllblTypeJomoPay = false;
var KSID = "";
var infotype = "";
var gblAPPID = "";
var gblColor=["0xF8E71Cff","0xDD39FFff","0x50E3C2ff","0x65ACFFff","0x2EF12Aff","0xFF566Bff","0xFFAC52ff","0xFFFFFFff"];
var gblColorArray=[];
var gblNoThanksClickSaveDatatoQB = false;
var jpdata="";
var gblFromAccCurrency = "";
var gblCardNum = "";
var gblTnCDataObj = "";
var gblTnCFlow = false;
var gblBeforeUpdateSMSNumber = false;
var gblEN_CNGSMS_MSG_BFR = "Dear Customer, You have request to change the mobile number, kindly confirm to complete the transaction";
var gblAR_CNGSMS_MSG_BFR = "Dear Customer, You have request to change the mobile number, kindly confirm to complete the transaction";
var gblEN_CNGSMS_MSG_AFTR = "Dear Customer, Mobile number has been updated successfully";
var gblAR_CNGSMS_MSG_AFTR = "Dear Customer, Mobile number has been updated successfully";
var gblProfileCustmerName = "";
var gblExchangeTempObject = {
    "CURR_CODE":"",
    "ARABIC_DESC":"",
    "ENGLISH_DESC":"",
    "CURR_ISO": "",
    "DECIMAL_PLACES":"",
    "IMG_FLAG":"" 
  };
var gblCalculateFXLeft = false;
var gblCalculateFXRight = false;
var gblExchangeRatesFlag = false;
var previous_FORM = null;
gblEN_CNGSMS_MSG_BFR = "Dear Customer, You have request to change the mobile number, kindly confirm to complete the transaction";
gblAR_CNGSMS_MSG_BFR = "Dear Customer, You have request to change the mobile number, kindly confirm to complete the transaction";
gblEN_CNGSMS_MSG_AFTR = "Dear Customer, Mobile number has been updated successfully";
gblAR_CNGSMS_MSG_AFTR = "Dear Customer, Mobile number has been updated successfully";
gblProfileCustmerName = "";
var gblUpdateOldMobileNumber = "";
gblFxRateModifiedArray = [];
gblFxRateUniqueCountryList = [];
var gblUpdateOldMobileNumber = "";
var gblFromModule = "";
var gblsubaccBranchList=false;
var gblrequestStatementBranch=false;
var gblRequestNewPinBranchList=false;
var gblRequestStatementCrediCards = false;
var gblPushRegisterTimerFlag = false;
var gblOnceSetBranch =false;
// Test data for Account Dashboard
kony.retailBanking.globalData.accountsDataList = {"segAccountsKA":[{
  "openingDate": "",
  "paymentTerm": "",
  "maturityDate": "",
  "lastStatementBalance": "",
  "creditCardNumber": "",
  "currencyCode" : "JOD",
  "accountName" : "Banks Staff-Personal Loans",
  "swiftCode" : "BJORJOAX",
  "favouriteStatus" : "A",
  "accountID": "0012250050221040",
  "accountType" : "L",
  "bankName": "BOJ",
  "currentBalance": "3327.181",
  "availableBalance":"8000",
  "productcode": "L",
  "outstandingBalance" : "",
  "bsbNum" : "JO77BJOR0010000012250050221040",
  "reference_no": "205409",
  "interestRate" : "",
  "minimumDue": "",
  "principalValue": "",
  "supportBillPay": "",
  "supportTransferFrom": "",
  "supportTransferTo": "",
  "transactionLimit": "",
  "transferLimit": "",
  "errmsg": "",
  "nickName": "",
  "accountHolder" :"",
  "availableCredit" : "",
  "availablePoints" : "",
  "dueDate": ""
  },
  {
  "openingDate": "",
  "paymentTerm": "",
  "maturityDate": "",
  "lastStatementBalance": "",
  "creditCardNumber": "",
  "currencyCode" : "JOD",
  "accountName" : "Term Deposite",
  "swiftCode" : "BJORJOAX",
  "favouriteStatus" : "A",
  "accountID": "0012250050221040",
  "accountType" : "T",
  "bankName": "BOJ",
  "currentBalance": "55555.181",
  "availableBalance":"5000",
  "productcode": "T",
  "outstandingBalance" : "",
  "bsbNum" : "JO77BJOR0010000012250050229999",
  "reference_no": "205409",
  "interestRate" : "",
  "minimumDue": "",
  "principalValue": "",
  "supportBillPay": "",
  "supportTransferFrom": "",
  "supportTransferTo": "",
  "transactionLimit": "",
  "transferLimit": "",
  "errmsg": "",
  "nickName": "",
  "accountHolder" :"",
  "availableCredit" : "",
  "availablePoints" : "",
  "dueDate": ""
  },
  {
  "openingDate": "",
  "paymentTerm": "",
  "maturityDate": "",
  "lastStatementBalance": "",
  "creditCardNumber": "",
  "currencyCode" : "JOD",
  "accountName" : "Car Loan",
  "swiftCode" : "BJORJOAX",
  "favouriteStatus" : "A",
  "accountID": "1112250050221041",
  "accountType" : "L",
  "bankName": "BOJ",
  "currentBalance": "1111.181",
  "availableBalance":"5000",
  "productcode": "L",
  "outstandingBalance" : "",
  "bsbNum" : "JO77BJOR0010000012250050211111",
  "reference_no": "205111",
  "interestRate" : "",
  "minimumDue": "",
  "principalValue": "",
  "supportBillPay": "",
  "supportTransferFrom": "",
  "supportTransferTo": "",
  "transactionLimit": "",
  "transferLimit": "",
  "errmsg": "",
  "nickName": "",
  "accountHolder" :"",
  "availableCredit" : "",
  "availablePoints" : "",
  "dueDate": ""
  },
  {
  "openingDate": "",
  "paymentTerm": "",
  "maturityDate": "",
  "lastStatementBalance": "",
  "creditCardNumber": "",
  "currencyCode" : "JOD",
  "accountName" : "Current Account",
  "swiftCode" : "BJORJOAX",
  "favouriteStatus" : "A",
  "accountID": "0013010050221002",
  "accountType" : "C",
  "bankName": "BOJ",
  "currentBalance": "5424.186",
  "availableBalance":"5424.186",
  "productcode": "C",
  "outstandingBalance" : "",
  "bsbNum" : "JO77BJOR0010000013010050221002",
  "reference_no": "205409",
  "interestRate" : "",
  "minimumDue": "",
  "principalValue": "",
  "supportBillPay": "",
  "supportTransferFrom": "",
  "supportTransferTo": "",
  "transactionLimit": "",
  "transferLimit": "",
  "errmsg": "",
  "nickName": "",
  "accountHolder" :"",
  "availableCredit" : "",
  "availablePoints" : "",
  "dueDate": ""
  },
  {
  "openingDate": "",
  "paymentTerm": "",
  "maturityDate": "",
  "lastStatementBalance": "",
  "creditCardNumber": "",
  "currencyCode" : "JOD",
  "accountName" : "Saving Account",
  "swiftCode" : "BJORJOAX",
  "favouriteStatus" : "A",
  "accountID": "0013050050221010",
  "accountType" : "S",
  "bankName": "BOJ",
  "currentBalance": "711.771",
  "availableBalance":"711.771",
  "productcode": "S",
  "outstandingBalance" : "",
  "bsbNum" : "JO98BJOR0010000013050050221010",
  "reference_no": "205409",
  "interestRate" : "",
  "minimumDue": "",
  "principalValue": "",
  "supportBillPay": "",
  "supportTransferFrom": "",
  "supportTransferTo": "",
  "transactionLimit": "",
  "transferLimit": "",
  "errmsg": "",
  "nickName": "",
  "accountHolder" :"",
  "availableCredit" : "",
  "availablePoints" : "",
  "dueDate": ""
  },
  {
  "openingDate": "",
  "paymentTerm": "",
  "maturityDate": "",
  "lastStatementBalance": "",
  "creditCardNumber": "",
  "currencyCode" : "JOD",
  "accountName" : "Inward Bills for Collection-Customers/Re",
  "swiftCode" : "BJORJOAX",
  "favouriteStatus" : "A",
  "accountID": "0015980050221004",
  "accountType" : "Y",
  "bankName": "BOJ",
  "currentBalance": "-10000",
  "availableBalance":"-10000",
  "productcode": "Y",
  "outstandingBalance" : "",
  "bsbNum" : "JO39BJOR0010000015980050221004",
  "reference_no": "205409",
  "interestRate" : "",
  "minimumDue": "",
  "principalValue": "",
  "supportBillPay": "",
  "supportTransferFrom": "",
  "supportTransferTo": "",
  "transactionLimit": "",
  "transferLimit": "",
  "errmsg": "",
  "nickName": "",
  "accountHolder" :"",
  "availableCredit" : "",
  "availablePoints" : "",
  "dueDate": ""
  }
]};