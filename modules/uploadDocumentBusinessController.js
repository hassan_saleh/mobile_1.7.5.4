/**
* Business Controller for UploadDocument Object
*/
kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.uploadDocumentBusinessController = function(mfObjSvc){
  this.mfObjectServiceHandler = mfObjSvc;
};

/**
* function uploadDocumentForNewUser
* Uploads Documents on success
*/
kony.rb.uploadDocumentBusinessController.prototype.uploadDocumentForNewUser = function(params,presentationSuccessCallback, presentationErrorCallback) 
{  
    this.mfObjectServiceHandler.create("uploadDocuments", params, {}, success, error);
    function success(res){
	  presentationSuccessCallback(res);
	}
	function error(err)
	{
	  presentationErrorCallback(err);
	}
};
