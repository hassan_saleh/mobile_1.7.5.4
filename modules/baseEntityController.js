kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.baseEntityController = function(){
};
  kony.rb.baseEntityController.prototype.getSdkInstance= function(){
    return kony.sdk.getCurrentInstance();
  };
  
  kony.rb.baseEntityController.prototype.getObjService = function(){
    var sdkInstance=this.getSdkInstance();
    return sdkInstance.getObjectService(kony.retailBanking.globalData.globals.serviceName,{"access": "online","objectName": "RBObjects"});
  };
 
  kony.rb.baseEntityController.prototype.fetch= function(entity,queryParams,headers,onSuccess,onError){
    var objService = this.getObjService();
    var options = {"dataObject":new kony.sdk.dto.DataObject(entity),"queryParams":queryParams,"headers":headers};
    objService.fetch(options,fetchSuccess,fetchError);
   
   function fetchSuccess(response) {
        kony.print(response);
        //alert(response);
        onSuccess(response);
  }
  function fetchError(err){
        kony.print(err);
       // alert(err);
        onError(err);
  }
 };
  kony.rb.baseEntityController.prototype.create = function(entity,record,headers,onSuccess,onError){
    var objService = this.getObjService();
    var options = {"dataObject":new kony.sdk.dto.DataObject(entity,record),"headers":headers};
    objService.create(options,createSuccess,createError);
  
  function createSuccess(response) {
    kony.print(response);
    onSuccess(response);
  }
  function createError(err){
    kony.print(err);
    onError(err);
  }
 };
 
  kony.rb.baseEntityController.prototype.partialUpdate = function(entity,record,headers,onSuccess,onError){
    var objService = this.getObjService();
    var options = {"dataObject": new kony.sdk.dto.DataObject(entity,record),"headers":headers};
    objService.partialUpdate(options,partialUpdateSuccess,partialUpdateError);
  
  function partialUpdateSuccess(response) {
    kony.print(response);
    onSuccess(response);
  }
  function partialUpdateError(err){
    kony.print(err);
    onError(err);
  }
 };

 kony.rb.baseEntityController.prototype.update = function(entity,record,headers,onSuccess,onError){
    var objService = this.getObjService();
    var options = {"dataObject": new kony.sdk.dto.DataObject(entity,record),"headers":headers};
    objService.update(options,updateSuccess,updateError);
  
  function updateSuccess(response) {
    kony.print(response);
    onSuccess(response);
  }
  function updateError(err){
    kony.print(err);
    onError(err);
  }
 };

 kony.rb.baseEntityController.prototype.delete = function(entity,primaryKeyValueMap,headers,onSuccess,onError){
    var objService = this.getObjService();
    var options = {"dataObject":new kony.sdk.dto.DataObject(entity,primaryKeyValueMap),"headers":headers};
    objService.deleteRecord(options,deleteSuccess,deleteError);
  
  function deleteSuccess(response) {
    kony.print(response);
    onSuccess(response);
  }
  function deleteError(err){
    kony.print(err);
    onError(err);
  }
 };

  
//CUSTOM VERBS

kony.rb.baseEntityController.prototype.customGet= function(customVerbName,entity,queryParams,headers,onSuccess,onError){
    var objService = this.getObjService();
    var options = {"dataObject":new kony.sdk.dto.DataObject(entity),"queryParams":queryParams,"headers":headers};
    objService.customVerb(customVerbName,options,customGetSuccess,customGetError);
   
   function customGetSuccess(response) {
        kony.print(response);
        //alert(response);
        onSuccess(response);
  }
  function customGetError(err){
        kony.print(err);
       // alert(err);
        onError(err);
  }
 };
  kony.rb.baseEntityController.prototype.customPost = function(customVerbName,entity,record,headers,onSuccess,onError){
    var objService = this.getObjService();
    var options = {"dataObject":new kony.sdk.dto.DataObject(entity,record),"headers":headers};
    objService.customVerb(customVerbName,options,customPostSuccess,customPostError);
  
  function customPostSuccess(response) {
    kony.print(response);
    onSuccess(response);
  }
  function customPostError(err){
    kony.print(err);
    onError(err);
  }
 };
 
 