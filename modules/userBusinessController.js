/**
* Business Controller for User Object
*/
kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.userBusinessController = function(mfObjSvc) {
    this.mfObjectServiceHandler = mfObjSvc;
};

/**
* function customRequestOTP
* requests OTP when resend is triggered
*/
kony.rb.userBusinessController.prototype.customRequestOTP = function(presentationSuccessCallback, presentationErrorCallback) {
    this.mfObjectServiceHandler.customPost("requestOTP", "User", {}, {}, success, error);

    function success(res) {
        presentationSuccessCallback(res);
    }

    function error(err) {
        presentationErrorCallback(err);
    }
};