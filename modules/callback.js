function cleanUp() {
    kony.print("cleaning up ...");
}
function watchReplyCallback(dict, replyObj){
   var taskID = kony.application.beginBackgroundTask("UniqueTaskID", cleanUp);
   var retDict;
   if (dict.request == "fetchAccounts") {
           fetchData(dict);
            /*var dictResponse = {};
            dictResponse.responseData = watchAccounts;
            replyObj.executeWithReply(dictResponse);*/
           kony.application.endBackgroundTask(taskID);
          
   }      
     function fetchData(dict){
         var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
         var options ={    "access": "online",
                           "objectName": "RBObjects"
                               };
         var serviceName = "RBObjects";
         var modelObj = INSTANCE.getModel("Accounts",serviceName,options);
         var dataObject = new kony.sdk.dto.DataObject("Accounts");
         var username =  DecryptValue(kony.store.getItem("userName"));
         var authParamKey = deviceid; 
         var serviceOptions = {"dataObject":dataObject,"queryParams":{"userName": username,"deviceID":authParamKey}};
         modelObj.fetch(serviceOptions,watchSuccess,watchError);
     }
        function watchSuccess(response){
                var dictResponse = {};
                dictResponse.responseData = response;
                dictResponse.currencyCode = kony.retailBanking.util.currency_symbols[kony.retailBanking.globalData.globals.CurrencyCode];
                replyObj.executeWithReply(dictResponse);
                kony.application.endBackgroundTask(taskID);
        }
        function watchError(err){
            kony.print("error");
         }
  
}