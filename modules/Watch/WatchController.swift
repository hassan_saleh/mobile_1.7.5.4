



// MARK: - Splash From lifecycle methods

func initFrmLaunch(form: frmWatLaunchController) {
  
}

func activateSeesion() {
     let phoneCommunicator = PhoneCommunicator.getSharedInstance();
    if phoneCommunicator.isActivate() {
//         phoneCommunicator.requestData(message: param, replyHandler: phoneCommunicationSuccess, errorHandler: phoneCommunicationFailure as? ((Error) -> Void));
    }
    else {
        phoneCommunicator.activate(completionHandler: { (status) in
//             if status {
//                 phoneCommunicator.requestData(message: param, replyHandler: phoneCommunicationSuccess, errorHandler: phoneCommunicationFailure as? ((Error) -> Void));
//             }
        });
    }
  

}

func onFrmSplashAwake(form: frmWatLaunchController, context: AnyObject) {
  activateSeesion();
  
}

func willActiviateFrmLaunch(form: frmWatLaunchController) {
    fetchData(form: form, param: ["service":"isUserLoggedIn" as AnyObject], screen: launchScreen)
}

func bindLaunchScreenData(form: frmWatLaunchController, response: [String : Any]) {
    if let isUserLoggedIn = response["isUserLoggedIn"] as? Bool {
        if isUserLoggedIn {
            moveToMenuScreen()
            return
        }
    }
    showAlert(message: "Please register the device to continue.")
}

// MARK: - Menu form lifecycle methods

func initFrmMenu(form: frmWatMenuController) {
}

func onFrmMenuAwake(form: frmWatMenuController, context: AnyObject) {
}

// MARK: - Menu list lifecycle methods

func onFrmMenuItemListAwake(form: frmWatMenuItemListController, context: AnyObject) {
    if let contextDict = context as? [String: String] {
        let tokenVal = Int(contextDict["token"] ?? "0")
        menuToken = tokenVal!
        if let menu = Menu(rawValue: tokenVal!) {
            DispatchQueue.main.async {
                form.setTitle(menu.title)
            }
            fetchData(form: form, param: ["service": menu.title.lowercased() as AnyObject], screen: listScreen)
        }
    }
    
    form.lblLoading.setHidden(false)
    form.lblSummery.setHidden(true)
    form.lblLoading.setText("Loading")
}

func segListRowClick(form: frmWatMenuItemListController, table: WKInterfaceTable, rowIndex: Int) {
    
    var context = ["":""]
    
    if let menu = Menu(rawValue: menuToken) {
        switch menu {
            
        case .accounts:
            
            if let aObj = accounts[rowIndex] as? [String: Any] {
                let title = String(describing: String(describing: aObj["accountName"] ?? "").split(separator: " ").first ?? "")
                let heading = String(describing: aObj["accountID"] ?? "")
                context = ["title": title, "heading": heading, "index": "\(rowIndex)"]
            }
            form.pushController(withName: "frmWatMenuListItemDetails", context: context);
            
        case .cards:
            
            if let aObj = cards[rowIndex] as? [String: Any] {
                let title = String(describing: aObj["card_type"] ?? "")
                
                let cardSuffix = String(describing: aObj["card_num"] ?? "").suffix(4)
                let cardPrefix = String(describing: aObj["card_num"] ?? "").prefix(6)
                
                let heading = "\(cardPrefix) ** **** \(cardSuffix)"
                context = ["title": title, "heading": heading, "index": "\(rowIndex)"]
            }
            form.pushController(withName: "frmWatMenuListItemDetails", context: context);
            
        case .deposits:
            
            if let aObj = deposits[rowIndex] as? [String: Any] {
                let title = String(describing: aObj["accountName"] ?? "")
                let heading = String(describing: aObj["accountID"] ?? "")
                selectedDepositeCT =  String(describing: aObj["currencyCode"] ?? "")
                selectedDepositeamount =  String(describing: aObj["currentBalance"] ?? "")
                context = ["title": title, "heading": heading, "index": "\(rowIndex)"]
            }
            form.pushController(withName: "frmWatMenuLAndDDetails", context: context);
            
        case .loans:
            
            if let aObj = loans[rowIndex] as? [String: Any] {
                let title = String(describing: aObj["accountName"] ?? "")
                let heading = String(describing: aObj["accountID"] ?? "")
                selectedLoanCT = String(describing: aObj["currencyCode"] ?? "")
                selectedLoanamount = String(describing: aObj["availableBalance"] ?? "")
                context = ["title": title, "heading": heading, "index": "\(rowIndex)"]
            }
        }
        form.pushController(withName: "frmWatMenuLAndDDetails", context: context);
        
    }
}

// MARK: - Menu list details Lifecycle

func onFrmMenuListItemDetailsAwake(form: frmWatMenuListItemDetailsController, context: AnyObject) {
    
    form.lblLoading.setHidden(false)
    form.lblLoading.setText("Loading")
    
    var rowIndex = 0;
    if let response = context as? [String: Any] {
        if let title = response["title"] as? String, let heading = response["heading"] as? String, let index = response["index"] as? String {
            rowIndex = Int(index)!
            DispatchQueue.main.async {
                form.setTitle(title)
                form.lblSummery.setText(" \(heading)")
            }
        }
    }
    
    if let menu = Menu(rawValue: menuToken) {
        switch menu {
        case .accounts:
            
            var accountDict = [String: AnyObject]()
            accountDict["service"] = "transactions" as AnyObject
            
            if let aObj = accounts[rowIndex] as? [String: Any] {
                //let accountID = String(describing: aObj["accountID"] ?? "") as AnyObject
                accountDict["selectedAcc"] = aObj as AnyObject
            }
            
            fetchData(form: form, param: accountDict, screen: listDetailsScreen)
        case .cards:
            
            var cardDict = [String: AnyObject]()
            cardDict["service"] = "cardTransactions" as AnyObject
            
            if let aObj = cards[rowIndex] as? [String: Any] {
                let cardNo = String(describing: aObj["card_num"] ?? "")
                let cardFlag = String(describing: aObj["cardTypeFlag"] ?? "")
                let branch = String(describing: aObj["card_acc_branch"] ?? "")
                let accountNo = String(describing: aObj["card_acc_num"] ?? "")
                
                cardDict["cardNo"] = cardNo as AnyObject
                cardDict["cardFlag"] = cardFlag as AnyObject
                cardDict["branch"] = branch as AnyObject
                cardDict["accountNo"] = accountNo as AnyObject
            }
            
            fetchData(form: form, param: cardDict, screen: listDetailsScreen)
        default: break
        }
    }
}

// MARK: -  Loans And Depostis details LifeCycle

func onFrmMenuLAndDDetailsAwake(form: frmWatMenuLAndDDetailsController, context: AnyObject) {
    
    form.lblLoading.setText("Loading")
    form.lblLoading.setHidden(false)
    //    form.lblSummery.setHidden(true)
    
    var rowIndex = 0
    
    if let response = context as? [String: Any] {
        if let title = response["title"] as? String, let heading = response["heading"] as? String, let index = response["index"] as? String {
            rowIndex = Int(index)!
            
            DispatchQueue.main.async {
                form.setTitle(title)
                form.lblSummery.setText(" \(heading)")
            }
        }
    }
    
    if let menu = Menu(rawValue: menuToken) {
        if menu == .deposits {
            
            var accountDict = [String: AnyObject]()
            accountDict["service"] = "depositInfo" as AnyObject
            if let aObj = deposits[rowIndex] as? [String: Any] {
                accountDict["selectedInfo"] = aObj as AnyObject
            }
            
            fetchData(form: form, param: accountDict, screen: loanAndDepositDetailsScreen)
        }
            
        else if menu == .loans {
            var accountDict = [String: AnyObject]()
            accountDict["service"] = "loanInfo" as AnyObject
            if let aObj = loans[rowIndex] as? [String: Any] {
                accountDict["selectedInfo"] = aObj as AnyObject
            }
            
            fetchData(form: form, param: accountDict, screen: loanAndDepositDetailsScreen)
        }
    }
    
}
