function onclicknextbasicinfo()
{
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmEnrolluserLandingKA");
  var viewModel = controller.getFormModel();

  if(kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxaccountype").getData()))
    {
     frmEnrolluserLandingKA.lblaccountype.skin="sknsectionHeaderLabel";
	 frmEnrolluserLandingKA.CopyFlexContainer06af0074ae94f40.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblaccountype.skin="sknD0021BLatoSemiBold";
	 frmEnrolluserLandingKA.CopyFlexContainer06af0074ae94f40.skin="sknFlxBGe2e2e2B1pxD0021B";    
    }
  if(kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getViewAttributeByProperty("tbxaccntnumber","text")))
    {
     frmEnrolluserLandingKA.lblacntnumber.skin="sknsectionHeaderLabel";
	 frmEnrolluserLandingKA.CopyFlexContainer09fbd523e9a6c43.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblacntnumber.skin="sknD0021BLatoSemiBold";
	 frmEnrolluserLandingKA.CopyFlexContainer09fbd523e9a6c43.skin="sknFlxBGe2e2e2B1pxD0021B";    
    }
   if(kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getViewAttributeByProperty("tbxssnnumber","text")) && validateSSN(viewModel.getViewAttributeByProperty("tbxssnnumber","text")))
    {
     frmEnrolluserLandingKA.lblssnumber.skin="sknsectionHeaderLabel";
	 frmEnrolluserLandingKA.FlexContainer0da5c3167759a45.skin="skntextFieldDivider";
     frmEnrolluserLandingKA.lblssnumbeErr.setVisibility(false);
    }
  else
    {
     frmEnrolluserLandingKA.lblssnumbeErr.setVisibility(false);
     frmEnrolluserLandingKA.lblssnumber.skin="sknD0021BLatoSemiBold";
	 frmEnrolluserLandingKA.FlexContainer0da5c3167759a45.skin="sknFlxBGe2e2e2B1pxD0021B";   
     if(frmEnrolluserLandingKA.tbxssnnumber.text !== null && frmEnrolluserLandingKA.tbxssnnumber.text !== "")
      {
      frmEnrolluserLandingKA.lblssnumbeErr.setVisibility(true);
      }
    }
  var calDate = kony.retailBanking.util.formatingDate.getDateObjFromKonyCalendarArray(viewModel.getViewAttributeByProperty("calDob","dateComponents"));
  if(kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxaccountype").getData()) &&
     kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getViewAttributeByProperty("tbxaccntnumber","text")) &&
     validateSSN(viewModel.getViewAttributeByProperty("tbxssnnumber","text")) && kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getViewAttributeByProperty("tbxssnnumber","text")) &&
     compareDate(calDate))
    {
     userdetailsTabSelected();
    }
}

function compareDate(dt)
{
  var currentDate = new Date();
  if (currentDate >= dt)
    return true;
  return false;
}

function onclicknextuserdetails()
{
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmEnrolluserLandingKA");
  var viewModel = controller.getFormModel();

  if(kony.retailBanking.util.validation.isValidUsername(viewModel && viewModel.getViewAttributeByProperty("tbxUsernameKA","text")))
    {
      alert("Username correct");
      frmEnrolluserLandingKA.CopyLabel0c929b1e27d9b44.skin="sknsectionHeaderLabel";
      frmEnrolluserLandingKA.CopyLabel03b3c824e8bd443ontainer0d38e2928c7fe4e.skin="skntextFieldDivider";
      frmEnrolluserLandingKA.lblUsernameErr.setVisibility(false);
    }
  else
    {
      alert("Username incorrect");
      frmEnrolluserLandingKA.lblUsernameErr.setVisibility(false);
      frmEnrolluserLandingKA.CopyLabel0c929b1e27d9b44.skin="sknD0021BLatoSemiBold";
      frmEnrolluserLandingKA.CopyLabel03b3c824e8bd443ontainer0d38e2928c7fe4e.skin="sknFlxBGe2e2e2B1pxD0021B"; 
      if(frmEnrolluserLandingKA.tbxUsernameKA.text !== null && frmEnrolluserLandingKA.tbxUsernameKA.text !== "")
        {
          frmEnrolluserLandingKA.lblUsernameErr.setVisibility(true);
        }
    }
    if(kony.retailBanking.util.validation.isValidPassword(viewModel && viewModel.getViewAttributeByProperty("tbxPasswordKA","text")))
    {
      alert("Password correct");
      frmEnrolluserLandingKA.CopyLabel038c9dce3f8b243.skin="sknsectionHeaderLabel";
      frmEnrolluserLandingKA.CopyLabel03bbaee3179c947.skin="skntextFieldDivider";
      frmEnrolluserLandingKA.lblPwdErr.setVisibility(false);
      
       if(validateConfirmPassword(viewModel && viewModel.getViewAttributeByProperty("tbxConfrmPwdKA","text"),viewModel && viewModel.getViewAttributeByProperty("tbxPasswordKA","text")))
    	{
      	frmEnrolluserLandingKA.CopyLabel038c9dce3f8b243.skin="sknsectionHeaderLabel";
      	frmEnrolluserLandingKA.CopyLabel0123e1dc1949143.skin="skntextFieldDivider";
          
        frmEnrolluserLandingKA.CopyLabel038c9dce3f8b243.skin="sknsectionHeaderLabel";
      	frmEnrolluserLandingKA.CopyLabel03bbaee3179c947.skin="skntextFieldDivider";
        frmEnrolluserLandingKA.lblPwdErr.setVisibility(false);
    	}
  		else
    	{
      	frmEnrolluserLandingKA.CopyLabel038c9dce3f8b243.skin="sknD0021BLatoSemiBold";
      	frmEnrolluserLandingKA.CopyLabel0123e1dc1949143.skin="sknFlxBGe2e2e2B1pxD0021B"; 
          
        frmEnrolluserLandingKA.CopyLabel038c9dce3f8b243.skin="sknD0021BLatoSemiBold";
      	frmEnrolluserLandingKA.CopyLabel03bbaee3179c947.skin="sknFlxBGe2e2e2B1pxD0021B"; 
        
      	frmEnrolluserLandingKA.lblPwdErr.setVisibility(true);
   		}
    }
  else
    {
      alert("Password incorrect");
        if(frmEnrolluserLandingKA.tbxPasswordKA.text !== null && frmEnrolluserLandingKA.tbxPasswordKA.text !== "")
        frmEnrolluserLandingKA.lblPwdErr.setVisibility(true);

      	frmEnrolluserLandingKA.CopyLabel038c9dce3f8b243.skin="sknD0021BLatoSemiBold";
      	frmEnrolluserLandingKA.CopyLabel0123e1dc1949143.skin="sknFlxBGe2e2e2B1pxD0021B"; 
          
        frmEnrolluserLandingKA.CopyLabel038c9dce3f8b243.skin="sknD0021BLatoSemiBold";
      	frmEnrolluserLandingKA.CopyLabel03bbaee3179c947.skin="sknFlxBGe2e2e2B1pxD0021B";  
    }
    if(kony.retailBanking.util.validation.isValidEmail(viewModel && viewModel.getViewAttributeByProperty("tbxEmailKA","text")))
    {
      frmEnrolluserLandingKA.CopyLabel092d7258cc7d340.skin="sknsectionHeaderLabel";
      frmEnrolluserLandingKA.CopyLabel0cee284b2ad414d.skin="skntextFieldDivider";
      frmEnrolluserLandingKA.lblEmailErr.setVisibility(false);
    }
  else
    {
      frmEnrolluserLandingKA.lblEmailErr.setVisibility(false);
      frmEnrolluserLandingKA.CopyLabel092d7258cc7d340.skin="sknD0021BLatoSemiBold";
      frmEnrolluserLandingKA.CopyLabel0cee284b2ad414d.skin="sknFlxBGe2e2e2B1pxD0021B"; 
      if(frmEnrolluserLandingKA.tbxEmailKA.text !== null && frmEnrolluserLandingKA.tbxEmailKA.text !== "")
        {
         frmEnrolluserLandingKA.lblEmailErr.setVisibility(true);
        }
    }
   if(kony.retailBanking.util.validation.isValidNumber(viewModel && viewModel.getViewAttributeByProperty("tbxPhoneNumberKA","text")))
    {
      frmEnrolluserLandingKA.CopyLabel0b7e45def03a04a.skin="sknsectionHeaderLabel";
      frmEnrolluserLandingKA.lblLineKA.skin="skntextFieldDivider";
      frmEnrolluserLandingKA.lblPhErr.setVisibility(false);
    }
  else
    {
      frmEnrolluserLandingKA.CopyLabel0b7e45def03a04a.skin="sknD0021BLatoSemiBold";
      frmEnrolluserLandingKA.lblLineKA.skin="sknFlxBGe2e2e2B1pxD0021B";
      frmEnrolluserLandingKA.lblPhErr.setVisibility(false);
      if(frmEnrolluserLandingKA.tbxPhoneNumberKA.text !== null && frmEnrolluserLandingKA.tbxPhoneNumberKA.text !== "")
        {
         frmEnrolluserLandingKA.lblPhErr.setVisibility(true);
        }
    }
  if(kony.retailBanking.util.validation.isValidUsername(viewModel && viewModel.getViewAttributeByProperty("tbxUsernameKA","text")) &&
    kony.retailBanking.util.validation.isValidPassword(viewModel && viewModel.getViewAttributeByProperty("tbxPasswordKA","text")) &&
    validateConfirmPassword(viewModel && viewModel.getViewAttributeByProperty("tbxConfrmPwdKA","text"),viewModel && viewModel.getViewAttributeByProperty("tbxPasswordKA","text")) &&
    kony.retailBanking.util.validation.isValidEmail(viewModel && viewModel.getViewAttributeByProperty("tbxEmailKA","text")) &&
    kony.retailBanking.util.validation.isValidNumber(viewModel && viewModel.getViewAttributeByProperty("tbxPhoneNumberKA","text")))
    {
    termsTabSelected(); 
    }
}

function validateConfirmPassword(password1,password2){
  if(password1 !== password2)
    return false;
  else 
    return true;
  
}

function onclickEnroll(){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmEnrolluserLandingKA");
  var viewModel = controller.getFormModel();
  var valid= false;
  var q1,q2,q3,q4,q5;
  var qstn = new Array(6);

  if(kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxquestn1").getData()))
    {
     frmEnrolluserLandingKA.lblquestion1.skin="sknsectionHeaderLabel";
     frmEnrolluserLandingKA.CopylblLineKA0f48b291a4b8541.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblquestion1.skin="sknD0021BLatoSemiBold";
     frmEnrolluserLandingKA.CopylblLineKA0f48b291a4b8541.skin="sknFlxBGe2e2e2B1pxD0021B"; 
    }
   if(kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getWidgetData("tbxAnswer1").getData()))
    {
     frmEnrolluserLandingKA.lblquestion1.skin="sknsectionHeaderLabel";
     frmEnrolluserLandingKA.CopylblLineKA09883b62c62974b.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblquestion1.skin="sknD0021BLatoSemiBold";
     frmEnrolluserLandingKA.CopylblLineKA09883b62c62974b.skin="sknFlxBGe2e2e2B1pxD0021B"; 
    }
  if(kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxquestn2").getData()))
    {
     frmEnrolluserLandingKA.lblquestion2.skin="sknsectionHeaderLabel";
     frmEnrolluserLandingKA.CopylblLineKA077ab14ab13414e.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblquestion2.skin="sknD0021BLatoSemiBold";
     frmEnrolluserLandingKA.CopylblLineKA077ab14ab13414e.skin="sknFlxBGe2e2e2B1pxD0021B"; 
    }
    if(kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getWidgetData("tbxAnswer2").getData()))
    {
     frmEnrolluserLandingKA.lblquestion2.skin="sknsectionHeaderLabel";
     frmEnrolluserLandingKA.CopylblLineKA075b26173b69849.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblquestion2.skin="sknD0021BLatoSemiBold";
     frmEnrolluserLandingKA.CopylblLineKA075b26173b69849.skin="sknFlxBGe2e2e2B1pxD0021B"; 
    }
    if(kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxquestn3").getData()))
    {
     frmEnrolluserLandingKA.lblquestion3.skin="sknsectionHeaderLabel";
     frmEnrolluserLandingKA.CopylblLineKA0094e603337474c.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblquestion3.skin="sknD0021BLatoSemiBold";
     frmEnrolluserLandingKA.CopylblLineKA0094e603337474c.skin="sknFlxBGe2e2e2B1pxD0021B"; 
    }
    if(kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getWidgetData("tbxAnswer3").getData()))
    {
     frmEnrolluserLandingKA.lblquestion3.skin="sknsectionHeaderLabel";
     frmEnrolluserLandingKA.CopylblLineKA0b98c3838f27c40.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblquestion3.skin="sknD0021BLatoSemiBold";
     frmEnrolluserLandingKA.CopylblLineKA0b98c3838f27c40.skin="sknFlxBGe2e2e2B1pxD0021B"; 
    }
    if(kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxquestn4").getData()))
    {
     frmEnrolluserLandingKA.lblquestion4.skin="sknsectionHeaderLabel";
     frmEnrolluserLandingKA.CopylblLineKA081389386ccb44a.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblquestion4.skin="sknD0021BLatoSemiBold";
     frmEnrolluserLandingKA.CopylblLineKA081389386ccb44a.skin="sknFlxBGe2e2e2B1pxD0021B"; 
    }
    if(kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getWidgetData("tbxAnswer4").getData()))
    {
     frmEnrolluserLandingKA.lblquestion4.skin="sknsectionHeaderLabel";
     frmEnrolluserLandingKA.CopylblLineKA0812b64794bc248.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblquestion4.skin="sknD0021BLatoSemiBold";
     frmEnrolluserLandingKA.CopylblLineKA0812b64794bc248.skin="sknFlxBGe2e2e2B1pxD0021B"; 
    }
   if(kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxquestn5").getData()))
    {
     frmEnrolluserLandingKA.lblquestion5.skin="sknsectionHeaderLabel";
     frmEnrolluserLandingKA.CopylblLineKA0d90b2a62526845.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblquestion5.skin="sknD0021BLatoSemiBold";
     frmEnrolluserLandingKA.CopylblLineKA0d90b2a62526845.skin="sknFlxBGe2e2e2B1pxD0021B"; 
    }
    if(kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getWidgetData("tbxAnswer5").getData()))
    {
     frmEnrolluserLandingKA.lblquestion5.skin="sknsectionHeaderLabel";
     frmEnrolluserLandingKA.CopylblLineKA09897f6223ce64a.skin="skntextFieldDivider";
    }
  else
    {
     frmEnrolluserLandingKA.lblquestion5.skin="sknD0021BLatoSemiBold";
     frmEnrolluserLandingKA.CopylblLineKA09897f6223ce64a.skin="sknFlxBGe2e2e2B1pxD0021B"; 
    }
  if(kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxquestn1").getData()) &&
    kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getWidgetData("tbxAnswer1").getData()) &&
    kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxquestn2").getData()) &&
    kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getWidgetData("tbxAnswer2").getData()) &&
    kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxquestn3").getData()) &&
    kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getWidgetData("tbxAnswer3").getData()) &&
    kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxquestn4").getData()) &&
    kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getWidgetData("tbxAnswer4").getData()) &&
    kony.retailBanking.util.validation.validateListBox(viewModel && viewModel.getWidgetData("lstboxquestn5").getData()) &&
    kony.retailBanking.util.validation.validateTextboxOrLabel(viewModel && viewModel.getWidgetData("tbxAnswer5").getData())){
    q1 = viewModel.getWidgetData("lstboxquestn1").getData();
    q2 = viewModel.getWidgetData("lstboxquestn2").getData();
    q3 = viewModel.getWidgetData("lstboxquestn3").getData();
    q4 = viewModel.getWidgetData("lstboxquestn4").getData();
    q5 = viewModel.getWidgetData("lstboxquestn5").getData();
    valid = validateQstns(q1,q2,q3,q4,q5,qstn);
  }
  if(valid){
   controller.performAction("saveData");
  }
}
function validateQstns(q1,q2,q3,q4,q5,qstn){
  qstn[q1] = 1;
  if(qstn[q2]!= 1)
    qstn[q2] =1;
  else 
    return false;
  if(qstn[q3]!= 1)
    qstn[q3]= 1;
  else 
    return false;
  if(qstn[q4]!= 1)
    qstn[q4]= 1;
  else 
    return false;
  if(qstn[q5]!= 1){
     qstn[q5]= 1;
     return true;
  }
  else 
    return false;
}
function onclickbackenrolluserbtn()
{
   if (frmEnrolluserLandingKA.enrollListsContainer.left == "0%"){
		frmLoginKA.show();}
  else if (frmEnrolluserLandingKA.enrollListsContainer.left == "-100%"){
 		 frmEnrolluserLandingKA.btnuserdetails.setEnabled(true);
         basicInfotabSelected();}
   else if (frmEnrolluserLandingKA.enrollListsContainer.left == "-200%"){
   		 frmEnrolluserLandingKA.btnterms.setEnabled(true);
  		 userdetailsTabSelected();}
}

function basicInfotabSelected(){
   frmEnrolluserLandingKA.enrollListsContainer.animate(
        kony.ui.createAnimation({100:
        	{ 
                "left": '0%',
             "stepConfig":{"timingFunction": easeOut}}}),
        {fillMode: forwards ,duration:0.3},
        {animationEnd: function() {} });
  
  frmEnrolluserLandingKA.tabSelectedIndicator.animate(
        kony.ui.createAnimation({100:
        	{ 
             "left": '0%',
             "stepConfig":{"timingFunction": easeOut}}}),
        {fillMode: forwards ,duration:0.3},
        {animationEnd: function() {} }); 
  
  frmEnrolluserLandingKA.btnbasicinfo.skin = skntabSelected;
  frmEnrolluserLandingKA.btnuserdetails.skin = skntabDeselected;
  frmEnrolluserLandingKA.btnterms.skin = skntabDeselected;
  frmEnrolluserLandingKA.btnuserdetails.setEnabled(false);
  frmEnrolluserLandingKA.btnterms.setEnabled(false);
  var d = new Date();
  var curr_date, curr_month, curr_year;
  curr_date = d.getDate();
  curr_month = d.getMonth()+1;
  curr_year = d.getFullYear();
  frmEnrolluserLandingKA.calDob.validEndDate = [curr_date,curr_month,curr_year];
  if(frmEnrolluserLandingKA.calDob.dateComponents === null)
    frmEnrolluserLandingKA.calDob.dateComponents = [curr_date,curr_month,curr_year];
}


function userdetailsTabSelected(){
   frmEnrolluserLandingKA.enrollListsContainer.animate(
        kony.ui.createAnimation({100:
        	{ 
             "left": '-100%',
             "stepConfig":{"timingFunction": easeOut}}}),
        {fillMode: forwards ,duration:0.3},
        {animationEnd: function() {} });
  
    frmEnrolluserLandingKA.tabSelectedIndicator.animate(
        kony.ui.createAnimation({100:
        	{ 
             "left": '33.3%',
             "stepConfig":{"timingFunction": easeOut}}}),
        {fillMode: forwards ,duration:0.3},
        {animationEnd: function() {} });
  frmEnrolluserLandingKA.btnterms.skin = skntabDeselected;
    frmEnrolluserLandingKA.btnuserdetails.skin = skntabSelected;
    frmEnrolluserLandingKA.btnbasicinfo.skin = skntabDeselected;
  frmEnrolluserLandingKA.btnterms.setEnabled(false);
  frmEnrolluserLandingKA.btnbasicinfo.setEnabled(false);
}

function termsTabSelected() {
  frmEnrolluserLandingKA.enrollListsContainer.animate(
        kony.ui.createAnimation({100:
        	{ 
             "left": '-200%',
             "stepConfig":{"timingFunction": easeOut}}}),
        {fillMode: forwards ,duration:0.3},
        {animationEnd: function() {} });
  
    frmEnrolluserLandingKA.tabSelectedIndicator.animate(
        kony.ui.createAnimation({100:
        	{ 
             "left": '66.6%',
             "stepConfig":{"timingFunction": easeOut}}}),
        {fillMode: forwards ,duration:0.3},
        {animationEnd: function() {} });
  
    frmEnrolluserLandingKA.btnterms.skin = skntabSelected;
    frmEnrolluserLandingKA.btnuserdetails.skin = skntabDeselected;
 frmEnrolluserLandingKA.btnbasicinfo.skin = skntabDeselected;
frmEnrolluserLandingKA.btnuserdetails.setEnabled(false);
  frmEnrolluserLandingKA.btnbasicinfo.setEnabled(false);
}

function validateSSN(ssn){
  if(ssn == null || ssn.length!=9)
    return false;
  else 
    return true;
}
