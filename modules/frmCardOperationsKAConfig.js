var frmCardOperationsKAConfig = {
  "formid": "frmCardOperationsKA",
  "frmCardOperationsKA": {
    "entity": "Cards",
    "objectServiceName": "RBObjects",
    "objectServiceOptions" : {"access":"online"},
  },
       "lblCardTypeKA":{
        "fieldprops": {
          "entity":"Cards",
          "constrained":true,
          "widgettype":"Label",
          "field":"cardType"
		}		  
      },
  "lblCardNumberKA":{
        "fieldprops": {
          "entity":"Cards",
          "constrained":true,
          "widgettype":"Label",
          "field":"cardNumber"
		}		  
      },
	  "lblValidThroughKA":{
        "fieldprops": {
          "entity":"Cards",
          "constrained":true,
          "widgettype":"Label",
          "field":"expiryDate"
		}		  
      },
	  "lblCardHolderNameKA":{
        "fieldprops": {
          "entity":"Cards",
          "constrained":true,
          "widgettype":"Label",
          "field":"cardId"
		}		  
      },
	  "txtAreaReasonKA":{
        "fieldprops": {
          "entity":"Cards",
          "constrained":true,
          "widgettype":"Label",
          "field":"Reason"
		}		  
      },
  "lblCardIdKA":{
        "fieldprops": {
          "entity":"Cards",
          "constrained":true,
          "widgettype":"Label",
          "field":"cardId"
		}		  
      },
"lblActionKA":{
        "fieldprops": {
          "entity":"Cards",
          "constrained":true,
          "widgettype":"Label",
          "field":"Action"
		}		  
      }
};