kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.frmSuccessFormQRCodeKAViewController = function() {

};

kony.rb.frmSuccessFormQRCodeKAViewController.prototype.frmSuccessFormQRCodeKApreshow = function() {
    var navManager = applicationManager.getNavManager();
    var acknowledgeScreenInfo = navManager.getCustomInfo("frmSuccessFormQRCodeKA");
    var data = acknowledgeScreenInfo.successDetails;
    var txnId = data.id;
    frmSuccessFormQRCodeKA.successText.text = i18n_referenceId + txnId;
    frmSuccessFormQRCodeKA.successTitle.text = i18n_transactionsHistory;
    frmSuccessFormQRCodeKA.processing.text = i18n_progressWithdraw;
    frmSuccessFormQRCodeKA.successIcon.opacity = 1;
    frmSuccessFormQRCodeKA.successIcon.skin = "sknsuccessIcon";
    frmSuccessFormQRCodeKA.innerSuccessContainer.opacity = 0;
    frmSuccessFormQRCodeKA.innerSuccessContainer.top = "100dp";
    frmSuccessFormQRCodeKA.successIcon2.isVisible = false;
    frmSuccessFormQRCodeKA.successImage2.opacity = 0;
    frmSuccessFormQRCodeKA.successContinue.opacity = 0;
    frmSuccessFormQRCodeKA.successContinue.top = "70dp";
    frmSuccessFormQRCodeKA.flxSignOutKA.isVisible = false;
    frmSuccessFormQRCodeKA.flxSignOutKA.top = "-50dp";
};

kony.rb.frmSuccessFormQRCodeKAViewController.prototype.frmSuccessFormQRCodeKApostshow = function() {
    var navManager = applicationManager.getNavManager();
    var acknowledgeScreenInfo = navManager.getCustomInfo("frmSuccessFormQRCodeKA");
    var data = acknowledgeScreenInfo.successDetails;
    if (data) {
        this.frmSuccessFormQRCodeKASuccesspostshow();
    } else {
        this.frmSuccessFormQRCodeKAErrorpostshow();
    }
};

kony.rb.frmSuccessFormQRCodeKAViewController.prototype.frmSuccessFormQRCodeKASuccesspostshow = function() {
    frmSuccessFormQRCodeKA.successImage2.isVisible = true;
    frmSuccessFormQRCodeKA.successIcon2.skin = "sknsuccessIcon";
    frmSuccessFormQRCodeKA.successImage2.src = "success_large_check.png";
    var transformSuccess1 = kony.ui.makeAffineTransform();
    transformSuccess1.scale(0.6, 0.6);
    var transformSuccess2 = kony.ui.makeAffineTransform();
    transformSuccess2.scale(0.75, 0.75);
    var transformSuccess3 = kony.ui.makeAffineTransform();
    transformSuccess3.scale(1.1, 1.1);
    var transformSuccess4 = kony.ui.makeAffineTransform();
    transformSuccess4.scale(1.0, 1.0);

    frmSuccessFormQRCodeKA.innerSuccessContainer.animate(
        kony.ui.createAnimation({
            "100": {
                "top": "-20dp",
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": duration,
            "delay": 3.1
        }, {
            "animationEnd": function() {}
        }
    );

    frmSuccessFormQRCodeKA.successContinue.animate(
        kony.ui.createAnimation({
            "100": {
                "top": "30dp",
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": duration,
            "delay": 3.3
        }, {
            "animationEnd": function() {}
        }
    );
    frmSuccessFormQRCodeKA.successIcon2.isVisible = true;
    frmSuccessFormQRCodeKA.successIcon2.animate(
        kony.ui.createAnimation({
            "0": {
                "transform": transformSuccess1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "15": {
                "transform": transformSuccess2,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "30": {
                "transform": transformSuccess1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "45": {
                "transform": transformSuccess2,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "60": {
                "transform": transformSuccess1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "80": {
                "transform": transformSuccess3,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "100": {
                "transform": transformSuccess4,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": 4,
            "delay": 0
        }, {
            "animationStart": function() {
                frmSuccessFormQRCodeKA.successIcon.opacity = 0;
            },
            "animationEnd": function() {}
        }
    );
    frmSuccessFormQRCodeKA.successImage2.animate(
        kony.ui.createAnimation({
            "0": {
                "transform": transformSuccess1,
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "60": {
                "transform": transformSuccess3,
                "opacity": 0.8,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "100": {
                "transform": transformSuccess4,
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": 1.0,
            "delay": 1.6
        }, {
            "animationEnd": function() {}
        }
    );

    frmSuccessFormQRCodeKA.processing.animate(
        kony.ui.createAnimation({
            "65": {
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "80": {
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "100": {
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": 2.0
        }, {
            "animationEnd": function() {}
        }
    );
    frmSuccessFormQRCodeKA.successInfoLabel.animate(
        kony.ui.createAnimation({
            "65": {
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "80": {
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "100": {
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": 2.0
        }, {
            "animationEnd": function() {}
        }
    );
    frmSuccessFormQRCodeKA.flxSignOutKA.isVisible = true;
    frmSuccessFormQRCodeKA.flxSignOutKA.animate(
        kony.ui.createAnimation({
            "100": {
                "top": "0dp",
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": duration,
            "delay": 3.1
        }, {
            "animationEnd": function() {}
        }
    );
};

kony.rb.frmSuccessFormQRCodeKAViewController.prototype.successIconAnimation = function() {
    frmSuccessFormQRCodeKA.successIcon.isVisible = true;
    frmSuccessFormQRCodeKA.successIcon.skin = "sknsuccessIcon";
    frmSuccessFormQRCodeKA.successImage2.src = "";
    var transformSuccess1 = kony.ui.makeAffineTransform();
    transformSuccess1.scale(0.6, 0.6);
    var transformSuccess2 = kony.ui.makeAffineTransform();
    transformSuccess2.scale(0.75, 0.75);
    var transformSuccess3 = kony.ui.makeAffineTransform();
    transformSuccess3.scale(0.9, 0.9);

    frmSuccessFormQRCodeKA.successIcon.animate(
        kony.ui.createAnimation({
            "0": {
                "transform": transformSuccess1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "15": {
                "transform": transformSuccess2,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "30": {
                "transform": transformSuccess1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "45": {
                "transform": transformSuccess2,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "60": {
                "transform": transformSuccess1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "80": {
                "transform": transformSuccess3,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "100": {
                "transform": transformSuccess1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": backwards,
            "duration": 4.0,
            "iterationCount": 0,
            "delay": 0
        }, {
            "animationStart": function() {},
            "animationEnd": function() {

            }
        }
    );

};

kony.rb.frmSuccessFormQRCodeKAViewController.prototype.onClickOfContinue = function() {
    var navManager = applicationManager.getNavManager();
    
};

kony.rb.frmSuccessFormQRCodeKAViewController.prototype.frmSuccessFormQRCodeKAErrorpostshow = function() {
    frmSuccessFormQRCodeKA.successImage2.isVisible = true;
    frmSuccessFormQRCodeKA.successIcon2.skin = "sknsuccessIcon";
    frmSuccessFormQRCodeKA.successImage2.src = "error.png";
    var transformSuccess1 = kony.ui.makeAffineTransform();
    transformSuccess1.scale(0.6, 0.6);
    var transformSuccess2 = kony.ui.makeAffineTransform();
    transformSuccess2.scale(0.75, 0.75);
    var transformSuccess3 = kony.ui.makeAffineTransform();
    transformSuccess3.scale(1.1, 1.1);
    var transformSuccess4 = kony.ui.makeAffineTransform();
    transformSuccess4.scale(1.0, 1.0);

    frmSuccessFormQRCodeKA.successText.text = i18n_transactionFailed;
    frmSuccessFormQRCodeKA.successTitle.text = i18n_failedTxnDesc;
    frmSuccessFormQRCodeKA.innerSuccessContainer.animate(
        kony.ui.createAnimation({
            "100": {
                "top": "-20dp",
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": duration,
            "delay": 3.1
        }, {
            "animationEnd": function() {}
        }
    );

    frmSuccessFormQRCodeKA.successContinue.animate(
        kony.ui.createAnimation({
            "100": {
                "top": "30dp",
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": duration,
            "delay": 3.3
        }, {
            "animationEnd": function() {}
        }
    );
    frmSuccessFormQRCodeKA.successIcon2.isVisible = true;
    frmSuccessFormQRCodeKA.successIcon2.animate(
        kony.ui.createAnimation({
            "0": {
                "transform": transformSuccess1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "15": {
                "transform": transformSuccess2,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "30": {
                "transform": transformSuccess1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "45": {
                "transform": transformSuccess2,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "60": {
                "transform": transformSuccess1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "80": {
                "transform": transformSuccess3,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "100": {
                "transform": transformSuccess4,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": 4.0,
            "delay": 0
        }, {
            "animationStart": function() {
                frmSuccessFormQRCodeKA.successIcon.opacity = 0;
            },
            "animationEnd": function() {}
        }
    );

    frmSuccessFormQRCodeKA.successImage2.animate(
        kony.ui.createAnimation({
            "0": {
                "transform": transformSuccess1,
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "60": {
                "transform": transformSuccess3,
                "opacity": 0.8,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "100": {
                "transform": transformSuccess4,
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": 1.0,
            "delay": 2.6
        }, {
            "animationStart": function() {
                frmSuccessFormQRCodeKA.successIcon2.skin = "sknerrorIcon";
            },
            "animationEnd": function() {}
        }
    );

    frmSuccessFormQRCodeKA.processing.animate(
        kony.ui.createAnimation({
            "65": {
                "opacity": 1,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "80": {
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            },
            "100": {
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": 4.0
        }, {
            "animationEnd": function() {}
        }
    );
};