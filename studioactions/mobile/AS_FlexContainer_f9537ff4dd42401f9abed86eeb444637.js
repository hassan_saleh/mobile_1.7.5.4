function AS_FlexContainer_f9537ff4dd42401f9abed86eeb444637(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseESP, onClickNoBackESP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function userResponseESP() {
        //   removeDatafrmTransfersformEPS();
        ResetFormIPSData();
        popupCommonAlertDimiss();
        frmPaymentDashboard.show();
    }

    function onClickNoBackESP() {
        popupCommonAlertDimiss();
    }
}