function AS_FlexContainer_jac4555f1a4a456185978128d6bfdecd(eventobject) {
    if (frmSelectProductKA.imgCollapsed.src == "collapsed.png") {
        frmSelectProductKA.imgCollapsed.src = "expanded.png";
        frmSelectProductKA.segSelectProduct.isVisible = true;
    } else {
        frmSelectProductKA.imgCollapsed.src = "collapsed.png";
        frmSelectProductKA.segSelectProduct.isVisible = false;
    }
}