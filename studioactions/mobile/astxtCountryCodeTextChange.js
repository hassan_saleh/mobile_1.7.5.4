function astxtCountryCodeTextChange(eventobject, changedtext) {
    return AS_TextField_e1f51df4168c47acb1420746e8be8c28(eventobject, changedtext);
}

function AS_TextField_e1f51df4168c47acb1420746e8be8c28(eventobject, changedtext) {
    if (frmRegisterUser.txtCardNum.text !== null && frmRegisterUser.txtPIN.text !== null && frmRegisterUser.txtMobileNumber.text !== null && frmRegisterUser.txtCountryCode.text !== null) {
        var cardNo = getCardNum(frmRegisterUser.txtCardNum.text);
        if (cardNo.length == 16 && frmRegisterUser.txtPIN.text.length == 4) {
            frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
        }
    } else {
        frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
    }
    if (!isEmpty(frmRegisterUser.txtCountryCode.text)) {
        frmRegisterUser.lblCountryCodeTitle.setVisibility(true);
        if (frmRegisterUser.txtCountryCode.text.length == 3) {
            frmRegisterUser.txtMobileNumber.setFocus(true);
        }
    } else {
        frmRegisterUser.lblCountryCodeTitle.setVisibility(false);
    }
}