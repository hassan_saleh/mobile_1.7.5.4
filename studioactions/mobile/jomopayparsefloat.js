function jomopayparsefloat(eventobject, changedtext) {
    return AS_TextField_g5a08435b2d0457194344e8053190c15(eventobject, changedtext);
}

function AS_TextField_g5a08435b2d0457194344e8053190c15(eventobject, changedtext) {
    if (frmJoMoPay.txtFieldAmount.text !== "" && frmJoMoPay.txtFieldAmount.text !== "0" && frmJoMoPay.txtFieldAmount.text !== 0) {
        var amount = Number.parseFloat(frmJoMoPay.txtFieldAmount.text).toFixed(3);
        frmJoMoPay.txtFieldAmount.text = amount;
    }
}