function AS_TextField_onDoneCCAmt(eventobject, changedtext) {
    return AS_TextField_d95f6ac7bcad4cb4986a6cbbea503257(eventobject, changedtext);
}

function AS_TextField_d95f6ac7bcad4cb4986a6cbbea503257(eventobject, changedtext) {
    frmCreditCardPayment.lblHiddenAmount.text = frmCreditCardPayment.txtFieldAmount.text;
    frmCreditCardPayment.txtFieldAmount.maxTextLength = 20;
    if (frmCreditCardPayment.lblCurrency.text === "JOD") {
        kony.print("JOD currency " + frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.text = fixDecimal(frmCreditCardPayment.txtFieldAmount.text, 3);
    } else {
        kony.print("Other currency " + frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.text = fixDecimal(frmCreditCardPayment.txtFieldAmount.text, 2);
    }
    //frmCreditCardPayment.txtFieldAmount.text = formatAmountwithcomma(frmCreditCardPayment.lblHiddenAmount.text,3);
}