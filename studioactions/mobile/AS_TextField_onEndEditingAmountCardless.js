function AS_TextField_onEndEditingAmountCardless(eventobject, changedtext) {
    return AS_TextField_d4a3d52fa79d4be6ac77201e35e01f15(eventobject, changedtext);
}

function AS_TextField_d4a3d52fa79d4be6ac77201e35e01f15(eventobject, changedtext) {
    if (parseInt(frmCardlessTransaction.txtFieldAmount.text) % 5 !== 0) {
        frmCardlessTransaction.flxPlusContainer.skin = "CopyslButtonPaymentDashFocus0fc27e0876b3b4d";
        frmCardlessTransaction.flxPlusContainer.setEnabled(false);
        frmCardlessTransaction.flxMinusContainer.skin = "CopyslButtonPaymentDashFocus0fc27e0876b3b4d";
        frmCardlessTransaction.flxMinusContainer.setEnabled(false);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.Cardless.checkAmount"), popupCommonAlertDimiss, "");
    } else {
        onendeditingCaredless();
        frmCardlessTransaction.txtFieldAmount.maxTextLength = 20;
        frmCardlessTransaction.txtFieldAmount.text = frmCardlessTransaction.lblHiddenAmount.text;
    }
}