function AS_Button_g13829135f5440efac99ff60fa97ec22(eventobject) {
    if (kony.sdk.isNetworkAvailable()) {
        frmJoMoPay.show();
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}