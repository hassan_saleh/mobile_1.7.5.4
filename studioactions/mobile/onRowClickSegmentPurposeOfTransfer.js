function onRowClickSegmentPurposeOfTransfer(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_aeaf69d8d19c42668e4551eef00b8744(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_aeaf69d8d19c42668e4551eef00b8744(eventobject, sectionNumber, rowNumber) {
    //alert("frmPOT.segFre.selectedRowItems[0].lblfrequency " + frmPOT.segFre.selectedRowItems[0].lblfrequency);
    if (frmPOT.lblTitle.text === geti18Value("i18n.transfer.transfersubpurpose")) {
        frmNewTransferKA.lblPOT.text = frmPOT.segFre.selectedRowItems[0].lblfrequency;
        frmConfirmTransferKA.lblRemmit.text = frmPOT.segFre.selectedRowItems[0].lblKey;
        frmConfirmTransferKA.lblPurposeofTransfer.text = frmPOT.segFre.selectedRowItems[0].lblfrequency;
        enableNext();
    } else {
        kony.retailBanking.globalData.globals.popTransferFlag = frmPOT.segFre.selectedRowItems[0].lblKey;
        frmNewTransferKA.lblTransferCategory.text = frmPOT.segFre.selectedRowItems[0].lblfrequency;
        frmNewTransferKA.lblPOT.text = geti18Value("i18n.settings.select");
    }
    frmNewTransferKA.show();
}