function AS_onclickbtnMailRequestStaement(eventobject) {
    return AS_Button_f8e4740106f5460a933d4f839a7c13e7(eventobject);
}

function AS_Button_f8e4740106f5460a933d4f839a7c13e7(eventobject) {
    if (frmRequestStatementAccounts.btnMail.text == "s") {
        frmRequestStatementAccounts.btnMail.text = "t";
        frmRequestStatementAccounts.btnBranch.text = "s";
        frmRequestStatementAccounts.lblBranchValue.text = geti18Value("i18n.Map.Branchname")
        frmRequestStatementAccounts.flxBranch.setVisibility(false);
        frmRequestStatementAccounts.flxAddressMail.setVisibility(true);
        frmRequestStatementAccounts.lbllinee1.skin = "sknFlxGreyLine";
    }
    nextSkinCheckForRequestStaement();
}