function AS_Button_f0c3e1f7fcaf47cda75a43c1e693d7f0(eventobject) {
    // kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    frmEPS.flxDetailsAliasScroll.setVisibility(false);
    frmEPS.flxDetailsMobileScroll.setVisibility(false);
    frmEPS.flxDetailsIBAN.setVisibility(true);
    if (frmEPS.btnIBAN.skin === slButtonBlueFocus) {
        frmEPS.btnIBAN.skin = sknOrangeBGRNDBOJ;
        frmEPS.btnAlias.skin = slButtonBlueFocus;
        frmEPS.btnMob.skin = slButtonBlueFocus;
    } else {
        frmEPS.btnAlias.skin = slButtonBlueFocus;
        frmEPS.btnIBAN.skin = sknOrangeBGRNDBOJ;
    }
    // reset the other flows 
    // Reset Alias Details //
    frmEPS.txtAliasName.text = "";
    frmEPS.txtAmountAlias.text = "";
    frmEPS.lblIBANAliastxt.text = "";
    frmEPS.lbAliasAddresstxt.text = "";
    frmEPS.lblAliasBanktxt.txt = "";
    frmEPS.flxAmountAlias.setVisibility(false);
    frmEPS.flxIBANAliass.setVisibility(false);
    frmEPS.flxAliasAddress.setVisibility(false);
    frmEPS.flxAliasBank.setVisibility(false);
    // Reset Alias Details //
    // Reset Mob Details //
    frmEPS.txtAliasNameMob.text = "";
    frmEPS.txtAmountMob.text = "";
    frmEPS.lblIBANMobtxt.text = "";
    frmEPS.lblAddressMobtxt.text = "";
    frmEPS.lblBankMobAliastxt.text = "";
    frmEPS.flxAmountMob.setVisibility(false);
    frmEPS.flxIbanDetailsMob.setVisibility(false);
    frmEPS.flxAddressMob.setVisibility(false);
    frmEPS.flxBankMob.setVisibility(false);
    // Reset Mob Details //
}