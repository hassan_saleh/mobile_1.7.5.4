function onClickToggleATMPOSUpdateLimit(eventobject) {
    return AS_Button_d03cc2209df14854bc9dbd020e5fd4d7(eventobject);
}

function AS_Button_d03cc2209df14854bc9dbd020e5fd4d7(eventobject) {
    if (eventobject.id === "btnDaily") {
        onSelect_PERIOD_ATMPOSLIMIT("Daily");
    } else if (eventobject.id === "btnWeekly") {
        onSelect_PERIOD_ATMPOSLIMIT("Weekly");
    } else if (eventobject.id === "btnMonthly") {
        onSelect_PERIOD_ATMPOSLIMIT("Monthly");
    }
}