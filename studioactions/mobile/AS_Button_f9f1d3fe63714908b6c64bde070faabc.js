function AS_Button_f9f1d3fe63714908b6c64bde070faabc(eventobject) {
    if (frmNewSubAccountConfirm.lblCheck1.text === "p") {
        callConfirmCreateNewSubAccount();
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.pleaseSelectCheckBox"), popupCommonAlertDimiss, "");
    }
}