function AS_ConfirmPasswordOnTextChange(eventobject, changedtext) {
    return AS_TextField_f958340f0288419582fe0ba29281bd4b(eventobject, changedtext);
}

function AS_TextField_f958340f0288419582fe0ba29281bd4b(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidPassword(frmEnrolluserLandingKA.tbxPasswordKA.text) && validateConfirmPassword(frmEnrolluserLandingKA.tbxPasswordKA.text, frmEnrolluserLandingKA.tbxConfrmPwdKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxGreenLine";
        confrmPasswordFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxOrangeLine";
        confrmPasswordFlag = false;
    }
    frmEnrolluserLandingKA.flxUsernameValidation.isVisible = false;
    frmEnrolluserLandingKA.flxPasswordValidation.isVisible = false;
    if (usernameFlag === true && passwordFlag === true && confrmPasswordFlag === true) {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextEnabled";
        frmEnrolluserLandingKA.flxProgress1.left = "80%";
    } else {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextDisabled";
        frmEnrolluserLandingKA.flxProgress1.left = "65%";
    }
    isClearButtonVisible(frmEnrolluserLandingKA.tbxConfrmPwdKA, frmEnrolluserLandingKA.CopylblClose0dcca487a6bc243);
    set_SHOW_BUTTON_VISIBILITY(frmEnrolluserLandingKA.tbxConfrmPwdKA, frmEnrolluserLandingKA.lblShowConfirmPassword);
}