function RequestToPayNextOnClick(eventobject) {
    return AS_FlexContainer_f9dd2a4fa0424d289d5610b6e2c54657(eventobject);
}

function AS_FlexContainer_f9dd2a4fa0424d289d5610b6e2c54657(eventobject) {
    if (frmIPSRequests.lblNextRP.skin === "sknLblNextEnabled") {
        gblTModule == "IPSReturn";
        // Mai 26/11/2020 if IPSRequest
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        SetupIPSRetunrConfirmationScreen();
        //   GetAliasInfo();
        frmEPS.flxMain.setVisibility(false);
        // frmEPS.flxReturnPayment.setVisibility(false);
        frmEPS.flxConfirmEPS.setVisibility(true);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.jomopay.fillDetails"), popupCommonAlertDimiss, "");
    }
}