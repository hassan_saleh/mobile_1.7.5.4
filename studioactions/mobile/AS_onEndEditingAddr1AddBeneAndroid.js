function AS_onEndEditingAddr1AddBeneAndroid(eventobject, changedtext) {
    return AS_TextField_c7724e7da56a49b6a847c57e95708963(eventobject, changedtext);
}

function AS_TextField_c7724e7da56a49b6a847c57e95708963(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxAddress1.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAddress1.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAddress1.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAddress1", frmAddExternalAccountKA.tbxAddress1.text);
}