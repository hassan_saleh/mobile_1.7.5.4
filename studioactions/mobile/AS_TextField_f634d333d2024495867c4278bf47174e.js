function AS_TextField_f634d333d2024495867c4278bf47174e(eventobject, changedtext) {
    try {
        if (frmUserSettingsSIRILIMIT.lblEmail1.top !== "15%") animateLabel("UP", "lblEmail1", frmUserSettingsSIRILIMIT.txtEmail1.text);
        var currency = getDefaultTransferCurr();
        kony.print("currency" + currency);
        if (kony.string.equalsIgnoreCase(currency, "JOD") || kony.string.equalsIgnoreCase(currency, "BHD")) {
            limit = 3;
        } else {
            limit = 2;
        }
        var value = fixDecimal(frmUserSettingsSIRILIMIT.txtEmail1.text, limit);
        if (value.indexOf(".") == -1 && value.length > 5) {
            frmUserSettingsSIRILIMIT.txtEmail1.text = value.substring(0, 5);
        } else {
            frmUserSettingsSIRILIMIT.txtEmail1.text = value;
        }
        if (frmUserSettingsSIRILIMIT.txtEmail1.text !== "" && frmUserSettingsSIRILIMIT.txtEmail1.text !== null) frmUserSettingsSIRILIMIT.lblClose1.setVisibility(true);
        else frmUserSettingsSIRILIMIT.lblClose1.setVisibility(false);
    } catch (err) {
        kony.print("err:" + err);
    }
}