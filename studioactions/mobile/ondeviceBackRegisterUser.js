function ondeviceBackRegisterUser(eventobject) {
    return AS_Form_g53cc23ca45343a68b0346061ed6ec57(eventobject);
}

function AS_Form_g53cc23ca45343a68b0346061ed6ec57(eventobject) {
    kony.print("gblFromModule::" + gblFromModule);
    if (gblFromModule == "RegisterUser") {
        frmLoginKA.show();
        frmRegisterUser.destroy();
    } else if (gblFromModule == "UpdateSMSNumber") {
        callMyProfileData();
        gblToOTPFrom = "";
        gblFromModule = "";
    } else if (gblFromModule == "UpdateMobileNumber") {
        frmManageCardsKA.show();
        gblToOTPFrom = "";
        gblFromModule = "";
    } else {
        gblFromModule = "ForgotPassword";
        gblReqField = "Username";
        initialiseAndCallEnroll();
        frmRegisterUser.destroy();
    }
}