function AS_TextField_f5c8e9d52f1e4ef2a79c6b2b8ba4d4ec(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidUsername(frmEnrolluserLandingKA.tbxUsernameKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxGreenLine";
        usernameFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxOrangeLine";
        usernameFlag = false;
    }
    animateLabel("DOWN", "lblUsername", frmEnrolluserLandingKA.tbxUsernameKA.text);
}