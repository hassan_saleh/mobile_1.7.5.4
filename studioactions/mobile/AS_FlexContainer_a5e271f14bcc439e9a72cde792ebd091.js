function AS_FlexContainer_a5e271f14bcc439e9a72cde792ebd091(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseESP, onClickNoBackESP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function userResponseESP() {
        //   removeDatafrmTransfersformEPS();
        ResetFormIPSData();
        popupCommonAlertDimiss();
        //frmPaymentDashboard.show();
        frmIPSManageBene.show();
    }

    function onClickNoBackESP() {
        popupCommonAlertDimiss();
    }
}