function onclickbackconfirmcards(eventobject) {
    return AS_FlexContainer_e6e65f3f3888459f919c4be864a6c858(eventobject);
}

function AS_FlexContainer_e6e65f3f3888459f919c4be864a6c858(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18nkey("i18n.common.sureExit"), popupCommonAlertDimiss, cancelconfirm, geti18nkey("i18n.common.NO"), geti18nkey("i18n.common.YES"));
}