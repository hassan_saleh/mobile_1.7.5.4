function AS_TextField_d9467ca6abe6481f9374e5ad79f41d66(eventobject, changedtext) {
    try {
        if (frmEstatementLandingKA.lblEmail1.top !== "15%") animateLabel("UP", "lblEmail1", frmEstatementLandingKA.txtEmail1.text);
        if (frmEstatementLandingKA.txtEmail1.text !== null && frmEstatementLandingKA.txtEmail1.text !== "" && isValidEmaill(frmEstatementLandingKA.txtEmail1.text)) {
            if (frmEstatementLandingKA.flxSwitchOnTouchLogin.isVisible && frmEstatementLandingKA.lblTermsandConditionsCheckBox.text === "p") frmEstatementLandingKA.lblNext.skin = "sknLblNextEnabled";
            else frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
        } else {
            frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
        }
        frmEstatementLandingKA.lblInvalidEmail1.setVisibility(false);
    } catch (err) {
        kony.print("err:" + err);
    }
}