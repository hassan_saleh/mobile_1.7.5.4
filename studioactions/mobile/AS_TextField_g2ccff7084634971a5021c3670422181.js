function AS_TextField_g2ccff7084634971a5021c3670422181(eventobject, changedtext) {
    if (frmRegisterUser.txtCardNum.text !== null && frmRegisterUser.txtPIN.text !== null && frmRegisterUser.txtMobileNumber.text !== null && frmRegisterUser.txtCountryCode.text !== null) {
        var cardNo = getCardNum(frmRegisterUser.txtCardNum.text);
        if (cardNo.length == 16 && frmRegisterUser.txtPIN.text.length == 4) {
            frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
        }
    } else {
        frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
    }
    if (!isEmpty(frmRegisterUser.txtCountryCode.text)) {
        if (frmRegisterUser.txtCountryCode.text.length == 3) {
            frmRegisterUser.txtMobileNumber.setFocus(true);
        }
    }
}