function AS_Form_editAccountSettingsPreshow(eventobject) {
    return AS_Form_cf00a963559540a1995e20866f36506d(eventobject);
}

function AS_Form_cf00a963559540a1995e20866f36506d(eventobject) {
    if (isEmpty(frmEditAccountSettings.txtAddNickName.text)) animateLabel("DOWN", "lblAddNickName", frmEditAccountSettings.txtAddNickName.text);
    else animateLabel("UP", "lblAddNickName", frmEditAccountSettings.txtAddNickName.text);
}