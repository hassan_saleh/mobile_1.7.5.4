function AS_Label_d4de92a360224d50ac617e7f277db65a(eventobject, x, y) {
    if (isSiriEnabled === "Y") {
        if (!isEmpty(gblDefaultAccTransfer)) {
            frmUserSettingsSIRILIMIT.show();
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.Siri.addaccount"), popupCommonAlertDimiss, "");
        }
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.SIRI.trasnferLimitenable"), popupCommonAlertDimiss, "");
    }
}